#!/usr/bin/python
#
#  REINTERPRETATION OF THE GIST FINDABLE HERE : https://gist.github.com/oseiskar/dbd51a3727fc96dcf5ed189fca491fb3
#    Changes -> Don't read on stdin use ARGV[1] instead
#    Changes -> Don't write on stdout use ARGV[2] instead
#    Changes -> Minify the HTML with htmlmin before writing file
#
#  Copyright 2017 Otto Seiskari
#  Licensed under the Apache License, Version 2.0.
#  See http://www.apache.org/licenses/LICENSE-2.0 for the full text.
#
#  This file is based on
#  https://github.com/swagger-api/swagger-ui/blob/4f1772f6544699bc748299bd65f7ae2112777abc/dist/index.html
#  (Copyright 2017 SmartBear Software, Licensed under Apache 2.0)
#
"""
Usage:
    python swagger-yaml-to-html.py /path/to/api.yaml doc.html
"""
import yaml, json, sys, htmlmin
TEMPLATE = """
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Swagger UI</title>
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700|Source+Code+Pro:300,600|Titillium+Web:400,600,700" rel="stylesheet">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swagger-ui-themes@3.0.1/themes/3.x/theme-muted.css" integrity="sha256-90JWHqS/RMhoNA0NX1XH/P2stL0EOtDJuPYfbogD0+I=" crossorigin="anonymous">
  <style>
    html
    {
      box-sizing: border-box;
      overflow: -moz-scrollbars-vertical;
      overflow-y: scroll;
    }
    *,
    *:before,
    *:after
    {
      box-sizing: inherit;
    }
    body {
      margin:0;
      background: #fafafa;
    }
  </style>
</head>
<body>
<div id="swagger-ui"></div>
<script <script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.24.2/swagger-ui-bundle.js" integrity="sha256-vSnFNvuQhNviHFS0M3EXj1LHCYdscvEdVmLIiDDZVhQ=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/swagger-ui/3.24.2/swagger-ui-standalone-preset.js" integrity="sha256-2NyzxwSleX/JEPRC1RQTlJjlR311bA2GbYYLWpye6Qk=" crossorigin="anonymous"></script>
<script>
window.onload = function() {
  var spec = %s;
  // Build a system
  const ui = SwaggerUIBundle({
    spec: spec,
    dom_id: '#swagger-ui',
    deepLinking: true,
    presets: [
      SwaggerUIBundle.presets.apis,
      SwaggerUIStandalonePreset
    ],
    plugins: [
      SwaggerUIBundle.plugins.DownloadUrl
    ],
    layout: "StandaloneLayout"
  })
  window.ui = ui
}
</script>
</body>
</html>
"""
inputFile = open(sys.argv[1], 'r')
spec = yaml.load(inputFile, Loader=yaml.FullLoader)

outputFile = open(sys.argv[2], 'w')
outputFile.write(htmlmin.minify(TEMPLATE % json.dumps(spec)))
# sys.stdout.write(TEMPLATE % json.dumps(spec))