)[![pipeline status](https://gitlab.com/glo-3112/ugram-api/badges/master/pipeline.svg)](https://gitlab.com/glo-3112/ugram-api/-/commits/master)

[![coverage report](https://gitlab.com/glo-3112/ugram-api/badges/master/coverage.svg)](https://gitlab.com/glo-3112/ugram-api/-/commits/master)

[![api status](http://badges.atelias.ovh/badges/api.svg)](http://badges.atelias.ovh/redirect/api)

[![api version](http://badges.atelias.ovh/badges/api/version.svg)](http://badges.atelias.ovh/redirect/api/version)

[![api reference](http://badges.atelias.ovh/badges/api/reference.svg)](http://badges.atelias.ovh/redirect/api/reference)

[![api reference](http://badges.atelias.ovh/badges/api/documentation.svg)](http://badges.atelias.ovh/redirect/api/documentation)

[![mongo-express](http://badges.atelias.ovh/badges/mongo-express.svg)](http://badges.atelias.ovh/redirect/mongo-express)

# ugram-h2020-team-09 Backend 


## Setup:
Because we are using `.env` files to manage access to mongodb and other services, you need to run `setup.py` before `docker-compose`.
This program will generate automatically all the `.env` files needed by the stack.
```shell
?> ./setup.py mute
  Mongodb credentials:
  (Unchanged) Username xxxxxxx
  (Unchanged) Password xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
                        
  # use thoose creds to log into mongo-express
  # re running setup.py will not recreate creds if they are already created but will display them
  Mongo-express credentials:
  (Unchanged) Username xxxxx
  (Unchanged) Password xxxxxxxxxxxxxxxxx

?> docker-compose up
```
   If you have trouble launching the API on Linux make sure your Mongo service is stopped localy (will work on most Linux OS)

    sudo systemctl stop mongod 

## Features

### GDPR compliance
According to the canadian recommendations in terms of data privacy, any visitor should have the possibility to accept or deny the usage of cookies. 
In this purpose the any user can retrieve their collected data. For a non-authenticated data we store nothing. 
So theses recommendations only impacts registered and authenticated users. 
Any user at anytime can ask for a packaging of any of his data in a human readable format.
Because of the load of this operation this is done hourly by a Cron task.

Any authenticated and at any time again can ask for a complete deletion of the data concerning him.
Again because of the load of this operation this is again done hourly by a Cron task.

## Documentation:
* [Api Code Documentation](http://localhost:6060)
* [Api Reference](http://localhost:7070)
* [Mongo Credentials](http://localhost:8081)

## Front:
    git clone https://github.com/GLO3112-classrooms/ugram-h2020-team-09.git

## Production Servers : 
* The V1 of the API can be found at : [https://api.atelias.ovh/api/v1](https://api.atelias.ovh/api/v1)
* The API Reference can be found at : [https://ref.atelias.ovh](https://ref.atelias.ovh)
* The API Documentation can be found at : [https://doc.atelias.ovh](https://doc.atelias.ovh)
* The API Mongo-Express image can be found at : [https://express.atelias.ovh](https://express.atelias.ovh)

### Network Architecture 
Architecture chart can be found [here on mairmaid](https://mermaid-js.github.io/mermaid-live-editor/#/view/eyJjb2RlIjoiZ3JhcGggVERcbiAgICBBW0NsaWVudF0gLS0-IEJbYXRlbGlhcy5vdmhdXG4gICAgQiAtLT4gQ3tETlN9XG4gICAgQyAtLT4gfGF0ZWxpYXMub3ZofEF3c0ZcbiAgICBDIC0tPiB8cHJlcHJvZC5hdGVsaWFzLm92aHxBd3NQRlxuICAgIEMgLS0-IHxhcGkuYXRlbGlhcy5vdmh8QXdzQlxuICAgIEMgLS0-IHxyZWYuYXRlbGlhcy5vdmh8QXdzQlxuICAgIEMgLS0-IHxkb2MuYXRlbGlhcy5vdmh8QXdzQlxuICAgIEMgLS0-IHxleHByZXNzLmF0ZWxpYXMub3ZofEF3c0JcbiAgICBDIC0tPiB8YXBpLnByZXByb2QuYXRlbGlhcy5vdmh8QXdzUEJcbiAgICBDIC0tPiB8cmVmLnByZXByb2QuYXRlbGlhcy5vdmh8QXdzUEJcbiAgICBDIC0tPiB8ZG9jLnByZXByb2QuYXRlbGlhcy5vdmh8QXdzUEJcbiAgICBDIC0tPiB8ZXhwcmVzcy5wcmVwcm9kLmF0ZWxpYXMub3ZofEF3c1BCXG5cbiAgICBBd3NGW0F3cyBGcm9udCBQcm9kXSAtLT4gQXdzRlByeGlcbiAgICBBd3NQRltQcmVwcm9kIEZyb250XSAtLT4gQXdzRlByeGlcbiAgICBBd3NGUHJ4aXtOZ2lueH0gLS0-IEF3c0ZEa3JcbiAgICBBd3NGRGtye0RvY2tlcn0gLS0-IEF3c0ZyXG4gICAgQXdzRkRrcntEb2NrZXJ9IC0tPiBBd3NGUHJlXG4gICAgQXdzRlByZVtQcmVwcm9kIEZyb250XVxuICAgIEF3c0ZyW0Zyb250XVxuXG4gICAgQXdzQltBd3MgQmFjayBwcm9kXSAtLT4gfGh0dHB8QXdzUHJ4aVxuICAgIEF3c0IgLS0-wqB8aHR0cHN8QXdzUHJ4aVxuICAgIEF3c1ByeGl7Tmdpbnh9IC0tPiB8OjYwNjB8QXdzRGtyXG4gICAgQXdzUHJ4aXtOZ2lueH0gLS0-IHw6NzA3MHxBd3NEa3JcbiAgICBBd3NQcnhpe05naW54fSAtLT4gfDo4MDgwfEF3c0RrclxuICAgIEF3c1ByeGl7Tmdpbnh9IC0tPiB8OjgwODF8QXdzRGtyXG4gICAgQXdzRGtye0RvY2tlcn0gLS0-IHw6NzA3MHxBd3NSZWZcbiAgICBBd3NEa3J7RG9ja2VyfSAtLT4gfDo2MDYwfEF3c0RvY1xuICAgIEF3c0RrcntEb2NrZXJ9IC0tPiB8OjgwODB8QXdzQXBpXG4gICAgQXdzRGtye0RvY2tlcn0gLS0-IHw6ODA4MXxBd3NFeHBcbiAgICBBd3NEa3J7RG9ja2VyfSAtLS0gQXdzTWdvXG4gICAgQXdzQXBpW0dvbGFuZyBBUEldIC0tPiBBd3NNZ29cbiAgICBBd3NFeHAgLS0-IEF3c01nb1xuICAgIEF3c0FwaVtHb2xhbmcgQVBJXVxuICAgIEF3c1JlZltBcGkgUmVmZXJlbmNlXVxuICAgIEF3c0RvY1tEb2N1bWVudGF0aW9uXVxuICAgIEF3c0V4cFtNb25nbyBFeHByZXNzXVxuXG4gICAgQXdzUEJbQXdzIEJhY2sgcHJlcHJvZF0gLS0-IHxodHRwfEF3c1BQcnhpXG4gICAgQXdzUEIgLS0-wqB8aHR0cHN8QXdzUFByeGlcbiAgICBBd3NQUHJ4aXtOZ2lueH0gLS0-IHw6NjA2MHxBd3NQRGtyXG4gICAgQXdzUFByeGl7Tmdpbnh9IC0tPiB8OjcwNzB8QXdzUERrclxuICAgIEF3c1BQcnhpe05naW54fSAtLT4gfDo4MDgwfEF3c1BEa3JcbiAgICBBd3NQUHJ4aXtOZ2lueH0gLS0-IHw6ODA4MXxBd3NQRGtyXG4gICAgQXdzUERrcntEb2NrZXJ9IC0tPiB8OjcwNzB8QXdzUFJlZlxuICAgIEF3c1BEa3J7RG9ja2VyfSAtLT4gfDo2MDYwfEF3c1BEb2NcbiAgICBBd3NQRGtye0RvY2tlcn0gLS0-IHw6ODA4MHxBd3NQQXBpXG4gICAgQXdzUERrcntEb2NrZXJ9IC0tPiB8OjgwODF8QXdzUEV4cFxuICAgIEF3c1BEa3J7RG9ja2VyfSAtLS0gQXdzUE1nb1xuICAgIEF3c1BBcGlbR29sYW5nIEFQSV0gLS0-IEF3c1BNZ29cbiAgICBBd3NQRXhwIC0tPiBBd3NQTWdvXG4gICAgQXdzUEFwaVtHb2xhbmcgQVBJXVxuICAgIEF3c1BSZWZbQXBpIFJlZmVyZW5jZV1cbiAgICBBd3NQRG9jW0RvY3VtZW50YXRpb25dXG4gICAgQXdzUEV4cFtNb25nbyBFeHByZXNzXVxuXG4iLCJtZXJtYWlkIjp7ImFycm93TWFya2VyQWJzb2x1dGUiOnRydWUsImZsb3djaGFydCI6eyJ1c2VNYXhXaWR0aCI6ZmFsc2UsImh0bWxMYWJlbHMiOnRydWUsImN1cnZlIjoiY2FyZGluYWwifX19)

We have `atelias.ovh` as domain. We have configured this domain to redirect to our AWS services stack.
You can found the used `dns.zone` in `config/dns/dns.zone`. This is the file that is loaded by OVH's DNS.

Because our CI test our api endpoints with `Newman` (see [github.com/postmanlabs/newman](https://github.com/postmanlabs/newman)) we need to have a running server to do the tests. So we're using an AWS t2.micro instance as preproduction.
If newman determine the API is ok, we deploy to our production server. 

We use nginx to :
- Force using https
- Reverse-proxy on domains
- Handle both IPv4 and IPv6 (Client -> IPv6 -> Nginx -> IPv4 (127.0.0.1) -> Docker)

We also have integrated a `badge-manager`. It's a micro-api written in Go too that create and manage all the badges displayed previously.
You can found it in `./config/badge-manager`. The badge manager is not present and deployed in the docker-compose routine because it should run independently and be deployed independently too.

### Deployment stack
Actually we use AWS ec2 instances as server only (without Beanstalk or S3) for many reasons :
1. The first is that this allow us to deploy a really custom stack. We have many dockers that run on the server, some are here only for monitoring purposes (downtime ...)
1. The second is that this allow us to link in a really simple way an external domain. We're not obliged to set our DNS to AWS Route 53.
1. A third reason can be the fact we're using CloudFlare as anti-DDOS service. That's the main reason that we can't set the DNSs of `atelias.ovh` to Route 53. That's because we have set them to CloudFlare.
1. A reason why we actually store our images to the API instance directly is because of two factors : 
    1. First : the API serve all images following a specific policy. A user can set his account to `private`. In this specific case an user can access the user publication only if it is a friend of the private account. This is not easily possible with S3. With S3 we should use a Lambda to do the checks. But the lambda should use the API database because some token information are stored in mongo (such as his validity or if it is revoked or not...). We're actually not using any AWS database service. So we can't easily connect the lambda to the database.
    1. Second : the API can store the images into S3 and serve them on his behalf. But this will add some useless complexity and there is no performance gains because of the network limitations of the t2.micro instances. We actually use a second disk storage mounted on `/static`. This way we can extend our storage space.

### Monitoring:
You can actually find server execution time on every response's body in `executionTime` field.

## Additionnal features of the UGRAM-API

### From where am I authenticated ?
The backend implements a way for a user to know from which device he is logged in. This is done via `geoip` and Nginx. When `nginx` reverse-proxy the api, it adds some headers that are used when a JWT token is created. This feature allow any user to know from which device he loggs-in an from which zone (geographically). 

More pecisely nginx adds : 

```
X-Host 							// The 'real' host. Not 127.0.0.1 (aka Nginx)
X-Real-IP          	// The 'real' remote ip address
X-Forwarded-Host    // The 'real' host, from where the request come
X-Location-Country  // The geoip country name
X-Location-City     // The geoip city name
```

A user can manage his tokens from theses routes : 

1 - Retieve his tokens :

```http request
GET /v1/users/me/security/authentication/tokens
Host: https://api.atelias.ovh
Accept: application/json
```

Will return something like this : 

```json
{
    "code": 200,
    "message": "OK",
    "data": [
         {
            "id": "5e9cc1b511bb41000106c2e5",
            "issuedAt": "2020-04-19T21:25:09Z",
            "remoteAddr": "172.18.0.1:59522",
          	"issuedFor": "Remote 77.205.153.49 at France, Bordeaux"
            "userAgent": {
                "name": "Safari",
                "version": "13.0.5",
                "device": "Desktop",
                "os": "macOS"
            }
        }
    ]
}
```

2 - Revoking a token :

```http request
GET /v1/users/me/security/authentication/tokens/revoke?token=tokenId
Host: https://api.atelias.ovh
```

```http request
DELETE /v1/users/me/security/authentication/tokens/revoke?token=tokenId
Host: https://api.atelias.ovh
```

The next time the token will be used, it will get a `401 Forbidden`. Revoking a token makes the token deleted from mongodb. 

When doing some benckmarks, this adds about 12ms of lattency.

### I have signed-in via the 'old fashined way' (login-password) but i want to use an Oauth provider or vice-versa

It is possible at any time to add / remove an OAuth provider. If you have signed-in via the ancient way and you want now to use Github or Gitlab as auth provider without delleting your account a user can use theses routes to do so.

#### 1 - Add a provider 

```http request
GET /v1/auth/oauth/:provider?redirect=/profile
Host: https://api.atelias.ovh
```

`provider` is the expected auth provider. Actually you can sign with `Gitlab` and `Github`. Redirect is where the API must redirect after the authentication process. This is `/` by default. And there is no way to specify the redirection host. This is always redirecting to `atelias.ovh`.

This route is like a swiss knives: 

1. If you're already authenticated : This route will add the provider to your OAuth authentication providers. That way you can temporarily sign with Github and Gitlab. This manipulation allows you to remove later one of them.
2. If you're not authenticated and not registered : This route will create your account with your 'email', 'name' ... found in the providers.
3. If you're not authenticated but registered with this provider : this route will sign-in with this provider.

#### 2 - Remove a provider

Like exposed before you can remove a provider from your providers list. This is possible at only one condition. (Two in real conditions explained later). You can delete a provider only if you have a fully complleted account (username, mail, password) or you have an other authentication provider set. (Because we don't want to lose access to our account)

```http request
GET /v1/users/me/security/authentication/providers/:name/unlink
Host: https://api.atelias.ovh
```

```http request
DELETE /v1/users/me/security/authentication/providers/:name/unlink
Host: https://api.atelias.ovh
```

`name` is the provider you want to remove.

In real condition and for security purposes, this feature must be joined with a 2FA protocol. Because we initially want to allow the user to be "provider free", we can't use a Google Authenticator or other branded solution. A really simple way to do this (for the user) is to make the frontend displaying a QR code and use a 'ugram' mobile application to scan it. The application check the QR code with the API and by this way we are theorically 100% sure that the person that is changing his settings is the real account owner. In a certain way it's like Steam 2FA but with a QR code instead of 5 chars.

#### 3 - Listing my providers

You can list alll your providers (in order to unlink them) at this route : 

``` http request
GET /v1/users/me/security/authentication/providers
Host: https://api.atelias.ovh
```

This route will return something like this : 

```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "github": {
            "foreignId": "xxxxxxxx",
            "foreignMail": "thomas.lombard@epitech.eu",
            "foreignName": "lombar_e",
            "linkedAt": "2020-04-19T22:42:14.003Z"
        }
    },
    "executionTime": "2.742038ms"
}
```



### I'm an European user and I want to know how my datas are used and what datas Ugram have of me

Following the European RGPD (Section 2 Art. 13), any user can ask all the data we know about him, in a easily readable format. 

#### 1 - Creating a bundle

A user can ask for a bundling of all his datas at this route : 

```http request
GET /v1/users/me/privacy/packaging/create
Host: https://api.atelias.ovh
```

Because this is a heavy load, the bundling is not done instantaneously. This is queued for an x CRON task.

The user will receive that kind of response : 

```json
{
    "code": 200,
    "message": "The packaging of your data is scheduled for Sunday, 19-Apr-20 22:10:00 UTC . Notice that the exact time may vary of few minutes",
    "data": "2020-04-19T22:10:00Z",
    "executionTime": "18.373936ms"
}
```

#### 2 - Getting the bundle list

A user can list all his previously created bundles at this route. Of course because that kind of archive can be heavy, thoses have an expiration delay of 1 month. 

Hitting this route : 

```http request
GET /v1/users/me/privacy/packaging
Host: https://api.atelias.ovh
```

Will return something like this : 

```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "id": "5e9ccba88c2d370001af001e",
            "requestedAt": "2020-04-19T22:07:36.912Z",
            "createdAt": "2020-04-19T22:10:00.492Z",
            "file": "74af649f-5578-4521-b877-9af819592087",
            "expiry": "2020-05-19T22:10:00.492Z",
            "done": true
        }
    ],
    "executionTime": "2.740954ms"
}
```

#### 3 - Downloading an archive

Once the packaging is done, a user can download his archive at this route : 

```http request
GET /v1/users/me/privacy/packaging/file/:taskId
Host: https://api.atelias.ovh
```

`taskId` is the id of the task. 

The user willl obtain a .zip file. This zip contains : 

```
anyname.zip
 |-- connections.csv
 |-- publications.csv
 |-- user.csv
 \-- images/
       |-- medium/
       |-- original/
       \-- small/
```

`connections.csv` contain all the tokens history. (without any security sensible informations)

`publications.csv` contain all the user publications history.

`user.csv` contain all the user informations.

For example publications is composed as follow :

```csv
Id,Descriptions,Tags,Mentioned users,Images,Created At,Update At
5e8dead8fb47f00001e08827,66113 heures 🤔,,,5e8dead8fb47f00001e08828.png,"Wednesday, 08-Apr-20 15:16:40 UTC","Wednesday, 08-Apr-20 15:16:41 UTC"
5e6fc94d91218f000179c97d,Updated,test,,5e6fc94d91218f000179c97e.png,"Monday, 16-Mar-20 18:45:33 UTC","Monday, 23-Mar-20 02:07:14 UTC"
,,dev,,,,
,,act,,,,
```

Images is the images that the API know in all sizes. You can found them at `archive/images/:size/:image.png`

### Anti scrapping protection

We have implemented a ratelimit protection. It's working is simple, at each request we increment a number, that resets to 0 every 15 minutes. If you're signed-in you can reach the API about 2000 times, and if you're a guest, you can reach the API 1000 times every 15 minutes. This mechanism adds 3 headers at each api response :  

| Header                  | Description                                                  |
| ----------------------- | ------------------------------------------------------------ |
| `X-Ratelimit`           | This is the number of request you can do before getting blocked |
| `X-Ratelimit-Remaining` | This is the number of remaining requests before getting blocked |
| `X-Ratelimit-Reset`     | This is the timestamp of when the limit resets               |

 
