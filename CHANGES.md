# Version 1.7.0
* Features : 
    * Added a way to pool notifications
    * Added a way to mark a notification as read
    * Added a way to "flush" all the notifications

### Added a way to pool notifications
It is now possible to get a user notifications via pooling.
Simply hit this route :
```http request
GET /v1/users/me/notifications 
Accept: application/json
```
This route will return something like this
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 20,
        "next": null,
        "prev": null,
        "total": 2,
        "items": [
            {
                "id": "5e4b7b89a54d751bd13ca4b4",
                "type": "like",
                "target": "comment",
                "targetId": "5e56f25ca54d75896c60bb8b",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": null,
                "read": false,
                "date": "2020-03-03T04:31:38.931Z"
            },
            {
                "id": "5e4b7b89a54d751bd13ca4b4",
                "type": "reply",
                "target": "comment",
                "targetId": "5e56f25ca54d75896c60bb3a",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": "Ah cool !",
                "read": false,
                "date": "2020-03-03T04:31:38.931Z"
             }
        ]
    },
    "executionTime": "38.695262ms"
}
```

### Added a way to mark a publication as read
Simply iht this route : 
```http request
GET /v1/users/me/notifications/:notificationId/read
Accept: application/json
```
You will obtain something like this
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e4b7b89a54d751bd13ca4b4",
        "type": "reply",
        "target": "comment",
        "targetId": "5e56f25ca54d75896c60bb3a",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Ah cool !",
        "read": true,
        "date": "2020-03-03T04:31:38.931Z"
    },
    "executionTime": "38.695262ms"
}
```

### Added a way to "flush" all the notifications
Simply iht this route :
```http request
GET /v1/users/me/notifications/flush
Accept: application/json
```
You will obtain something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 20,
        "next": null,
        "prev": null,
        "total": 0,
        "items": []
    },
    "executionTime": "38.695262ms"
}
```


## Version 1.6.0
* Features : 
    * Added a way to comment a publication
    * Added a way to reply to a publication comment
    * Added a way to like / dislike a publication
    * Added a way to like / dislike a comment
    * Added a way to like / dislike a reply
    
### Added a way to comment a publication (Delivery 3)
It is now possible to comment a publication.
Simply use this route : 
```http request
POST /v1/publications/:publicationid/comments
Content-Type: application/json

{
    "text": "comment here"
}
```
You will obtain something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e9230982fa5dbc2c96c208e",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-11T23:03:20.447735+02:00",
        "updatedAt": null,
        "replies": [],
        "nbReplies": 0,
        "likes": [],
        "nbLikes": 0
    },
    "executionTime": "57.357363ms"
}
```

### Added a way to reply to a publication comment
It is now possible to reply to a comment
Simply hit this route :
```http request
POST /v1/publications/:publicationid/comments/:commentId/replies
Content-Type: application/json

{
    "text": "reply here"
}
```
You will obtain something like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e9236e02fa5dbcd0b2d8692",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-11T23:30:08.089502+02:00",
        "updatedAt": null,
        "likes": [],
        "nbLikes": 0
    },
    "executionTime": "51.253534ms"
}
```

### Added a way to like / dislike a publication
It is now possible to like / dislike a publication
##### If publication is liked
```http request
GET /v1/publications/:publicationid/liked
```
This route will return something like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": false,
    "executionTime": "47.543574ms"
}
```
##### Like a publication 
```http request
POST /v1/publications/:publicationid/liked
```
This route will return something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e5e8f00a54d752f51fcfe49",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "firstName": "Jean",
            "lastName": "Paul",
            "friendsId": [],
            "publicationsId": [
                "5e7668cba54d75dd78baf7e2"
            ],
            "profilePicture": "000000000000000000000001",
            "registrationDate": "2020-03-03T04:31:38.931Z"
        },
        "description": "Une seule image",
        "tags": [
            "test",
            "dev"
        ],
        "mentions": [],
        "images": [],
        "comments": null,
        "liked": true,
        "nbLikes": 1,
        "createdAt": "2020-03-03T17:08:16.301Z",
        "updatedAt": "2020-04-11T23:45:32.766943+02:00"
    },
    "executionTime": "48.411097ms"
}
```
##### Dislike a publication 
```http request
DELETE /v1/publications/:publicationid/liked
```
This route will return something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e5e8f00a54d752f51fcfe49",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "firstName": "Jean",
            "lastName": "Paul",
            "friendsId": [],
            "publicationsId": [
                "5e7668cba54d75dd78baf7e2"
            ],
            "profilePicture": "000000000000000000000001",
            "registrationDate": "2020-03-03T04:31:38.931Z"
        },
        "description": "Une seule image",
        "tags": [
            "test",
            "dev"
        ],
        "mentions": [],
        "images": [],
        "comments": null,
        "liked": false,
        "nbLikes": 1,
        "createdAt": "2020-03-03T17:08:16.301Z",
        "updatedAt": "2020-04-11T23:45:32.766943+02:00"
    },
    "executionTime": "48.411097ms"
}
```

### Added a way to like / dislike a comment
##### If comment is liked
```http request
GET /v1/publications/:publicationId/comments/:commentId/liked
```
This route will return something like this 
```json
{
    "code": 200,
    "message": "OK",
    "data": false,
    "executionTime": "24.76871ms"
}
```
##### Like a comment
```http request
POST /v1/publications/:publicationId/comments/:commentId/liked
```
This route will return something like this 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e8e3b7d2fa5dbc178727188",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-08T21:00:45.785Z",
        "updatedAt": null,
        "replies": [
            {
                "id": "5e8e3cea2fa5dbc178727189",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": "Commentaire",
                "createdAt": "2020-04-08T21:06:50.892Z",
                "updatedAt": null,
                "likes": [
                    {
                        "id": "5e4cb7f119bab3000132b0e6",
                        "username": "test",
                        "profilePicture": null
                    },
                    {
                        "id": "5e4d7762a1fabc0001c95273",
                        "username": "res",
                        "profilePicture": null
                    }
                ],
                "nbLikes": 11
            },
            {
                "id": "5e9236e02fa5dbcd0b2d8692",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": "Commentaire",
                "createdAt": "2020-04-11T21:30:08.089Z",
                "updatedAt": null,
                "likes": [],
                "nbLikes": 0
            }
        ],
        "nbReplies": 2,
        "likes": [
            {
                "id": "5e4afddf8764bf00015d1bca",
                "username": "vale",
                "profilePicture": null
            },
            {
                "id": "5e4cb7f119bab3000132b0e6",
                "username": "test",
                "profilePicture": null
            }
        ],
        "nbLikes": 11
    },
    "executionTime": "34.344046ms"
}
```
##### Dislike a comment
```http request
DELETE /v1/publications/:publicationId/comments/:commentId/liked
```
This route will return something like this 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e8e3b7d2fa5dbc178727188",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-08T21:00:45.785Z",
        "updatedAt": null,
        "replies": [
            {
                "id": "5e8e3cea2fa5dbc178727189",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": "Commentaire",
                "createdAt": "2020-04-08T21:06:50.892Z",
                "updatedAt": null,
                "likes": [
                    {
                        "id": "5e4cb7f119bab3000132b0e6",
                        "username": "test",
                        "profilePicture": null
                    },
                    {
                        "id": "5e4d7762a1fabc0001c95273",
                        "username": "res",
                        "profilePicture": null
                    }
                ],
                "nbLikes": 11
            },
            {
                "id": "5e9236e02fa5dbcd0b2d8692",
                "author": {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "profilePicture": "000000000000000000000001"
                },
                "text": "Commentaire",
                "createdAt": "2020-04-11T21:30:08.089Z",
                "updatedAt": null,
                "likes": [],
                "nbLikes": 0
            }
        ],
        "nbReplies": 2,
        "likes": [
            {
                "id": "5e4afddf8764bf00015d1bca",
                "username": "vale",
                "profilePicture": null
            },
            {
                "id": "5e4cb7f119bab3000132b0e6",
                "username": "test",
                "profilePicture": null
            }
        ],
        "nbLikes": 11
    },
    "executionTime": "34.344046ms"
}
```


### Added a way to like / dislike a reply
##### If comment is liked
```http request
GET /v1/publications/:publicationid/comments/:commentId/replies/:replyId/liked
```
This route will return something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": false,
    "executionTime": "25.655649ms"
}
```

##### Like a comment
```http request
POST /v1/publications/:publicationid/comments/:commentId/replies/:replyId/liked
```
This route will return something like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e8e3cea2fa5dbc178727189",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-08T21:06:50.892Z",
        "updatedAt": null,
        "likes": [
            {
                "id": "5e4afddf8764bf00015d1bca",
                "username": "vale",
                "profilePicture": null
            },
            {
                "id": "5e4cb7f119bab3000132b0e6",
                "username": "test",
                "profilePicture": null
            }
        ],
        "nbLikes": 11
    },
    "executionTime": "69.029367ms"
}
```

##### Dislike a comment
```http request
DELETE /v1/publications/:publicationid/comments/:commentId/replies/:replyId/liked
```
This route will return something like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e8e3cea2fa5dbc178727189",
        "author": {
            "id": "5e5dddaaa54d759da7eb3691",
            "username": "username",
            "profilePicture": "000000000000000000000001"
        },
        "text": "Commentaire",
        "createdAt": "2020-04-08T21:06:50.892Z",
        "updatedAt": null,
        "likes": [
            {
                "id": "5e4afddf8764bf00015d1bca",
                "username": "vale",
                "profilePicture": null
            },
            {
                "id": "5e4cb7f119bab3000132b0e6",
                "username": "test",
                "profilePicture": null
            }
        ],
        "nbLikes": 10
    },
    "executionTime": "66.874561ms"
}
```

## Version 1.5.0
* Refactoring research routes
    * Added - /search
    * Added - /search/tags
    * Added - /search/users

### Refactoring research routes
#### Added - /search
It is now possible to make a global research.
Simply reach this route : 
```http request
GET /v1/search?q=search+string&page=1&size=20
```
##### Note that `page` and `size` are not required arguments as they are respectively defaulted at 1 and 20
You should obtain something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "users": {
            "page": 1,
            "size": 5,
            "next": null,
            "prev": null,
            "total": 3,
            "items": [
                {
                    "id": "5e5dddaaa54d759da7eb3691",
                    "username": "username",
                    "firstName": "Jean",
                    "lastName": "Paul",
                    "friendsId": [],
                    "publicationsId": [
                        "5e5e8f00a54d752f51fcfe49",
                        "5e5e8f02a54d752f51fcfe4b",
                        "5e73039fa54d759c5b519e8a",
                        "5e758532a54d75cc009b9951",
                        "5e7668cba54d75dd78baf7e2"
                    ],
                    "profilePicture": "000000000000000000000001",
                    "registrationDate": "0001-01-01T00:00:00Z"
                },
                ...
            ]
        },
        "tags": {
            "page": 1,
            "size": 5,
            "next": null,
            "prev": null,
            "total": 8,
            "items": [
                {
                    "id": "5e7668cba54d75dd78baf7e3",
                    "name": "user",
                    "score": 1
                },
                {
                    "id": "5e7668cba54d75dd78baf7e4",
                    "name": "usage",
                    "score": 1
                },
                ...
            ]
        },
        "publications": {
            "page": 1,
            "size": 5,
            "next": null,
            "prev": null,
            "total": 1,
            "items": [
                {
                    "id": "5e4dcdc3a1fabc0001c952a1",
                    "author": {
                        "id": "5e4cd64819bab3000132b0f4",
                        "username": "zacklbd",
                        "profilePicture": null
                    },
                    "description": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et [...]",
                    "match": {
                        "match": "us",
                        "index": 39,
                        "score": 0.0017341040462427746,
                        "pretty": "... iste nat<q>us</q> error sit voluptatem accusant ..."
                    }
                }
            ]
        }
    },
    "executionTime": "60.770539ms"
}
```
### Added - /search/tags
You can now search tags from this route :
```http request
GET /v1/search/tags?q=tes
```
You will obtain something like this:
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 5,
        "next": null,
        "prev": null,
        "total": 1,
        "items": [
            {
                "id": "5e758532a54d75cc009b9952",
                "name": "test",
                "score": 1
            }
        ]
    },
    "executionTime": "12.889875ms"
}
```
### Added - /search/users
You can now search users from this route : 
```http request
GET /v1/search/users?q=use
```
You will obtain something like this:
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 5,
        "next": null,
        "prev": null,
        "total": 2,
        "items": [
            {
                "id": "5e5af23ea54d7524fdc08e12",
                "username": "usernedame",
                "firstName": "Jean",
                "lastName": "Paul",
                "friendsId": [],
                "publicationsId": [],
                "profilePicture": "000000000000000000000001",
                "registrationDate": "0001-01-01T00:00:00Z"
            },
            {
                "id": "5e5dddaaa54d759da7eb3691",
                "username": "username",
                "firstName": "Jean",
                "lastName": "Paul",
                "friendsId": [],
                "publicationsId": [
                    "5e5e8f00a54d752f51fcfe49",
                    "5e5e8f02a54d752f51fcfe4b",
                    "5e73039fa54d759c5b519e8a",
                    "5e758532a54d75cc009b9951",
                    "5e7668cba54d75dd78baf7e2"
                ],
                "profilePicture": "000000000000000000000001",
                "registrationDate": "0001-01-01T00:00:00Z"
            }
        ]
    },
    "executionTime": "14.974483ms"
}
```

## Version 1.4.0
* User privacy recommendations
    * GDPR compliance
    * Improvements with global internet recommendations

### GDPR compliance
#### Ask for all registered datas
Any authenticated user can now ask for a packaging of any of his data here :
```http request
GET /v1/user/me/privacy/packaging/create
```
He will obtain a response like this :
```json
{
    "code": 200,
    "message": "The packaging of your data is scheduled for Thursday, 19-Mar-20 18:45:00 EDT . Notice that the exact time may vary of few minutes",
    "data": "2020-03-19T18:45:00-04:00",
    "executionTime": "66.226095ms"
}
```
#### Retrieve all the bundles
At any time the user can retrieve all his packages information here :
```http request
GET /v1/user/me/privacy/packaging
```
He will obtain a response like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "id": "5e72cbe9a54d7504e0c35a89",
            "requestedAt": "2020-03-18T21:33:29.775-04:00",
            "createdAt": "2020-03-18T21:40:31.47-04:00",
            "file": null,
            "expiry": "2020-02-17T20:40:31.47-05:00",
            "done": true
        },
        {
            "id": "5e72cce0a54d75052ae7ed2e",
            "requestedAt": "2020-03-18T21:35:46.137-04:00",
            "createdAt": "2020-03-18T21:40:36.62-04:00",
            "file": "ccc14d46-a51e-4e8a-9190-88290474a4b5",
            "expiry": "2020-04-17T21:40:36.62-04:00",
            "done": true
        },
        {
            "id": "5e73f5b4a54d75338dc8e7f1",
            "requestedAt": "2020-03-19T18:44:04.534-04:00",
            "createdAt": null,
            "file": null,
            "expiry": null,
            "done": false
        }
    ],
    "executionTime": "27.430053ms"
}
```
#### Download a specific bundle
At anytime a user can download the bundled datas concerning him here
```http request
GET /v1/users/me/privacy/packaging/file/5e72cce0a54d75052ae7ed2e
```
Will respond a zip file.

## Version 1.3.0
## Tags Update
* Api
    * Tags
        - Get Tag
        - Get Tag by Id
        - Post Tag
        - Get Tag from substring

    * Publications
        - Get Publication from tags

### Tags
#### Get Tag / Get Tag by Id
You can get all tag using
```http request
    GET /v1/tags
```
response:
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 20,
        "next": null,
        "prev": null,
        "total": 5,
        "items": [
            {
                "id": "5e6fed29ab71f57d56566479",
                "name": "Animaux",
                "score": 2
            },
            {
                "id": "5e6fed82ab71f50e2851034d",
                "name": "Langue",
                "score": 1
            }
        ]
    },
    "executionTime": "21.599431ms"
}
```

#### Post tag
You can add Tag to the database using:
```http request
    POST /v1/tags
```
request body must be:
```json
{
    "name": "tagName"
}
```

#### Get Tags from substring
You can get tag using partial string
exemple: if your looking for "Rododindron" using "dodin" while work

```http request
    GET /v1/tags/partials?q=dodin
```

response body:
```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "id": "5e6fed17ab71f57d56566478",
            "name": "Rododindron",
            "score": 1
        }
    ],
    "executionTime": "2.320495ms"
}
```

### Publications
#### Get Publication from tags
You can get publication containing tags using:
```http request
    GET /v1/publications/fromTags?q=Chat,Chien
```
response body:
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 20,
        "next": null,
        "prev": null,
        "total": 11,
        "items": [
            {
                "id": "5e6fed82ab71f50e2851034b",
                "author": {
                    "id": "5e6fe3f4ab71f531affbc5fe",
                    "username": "tatatata",
                    "firstName": "toto",
                    "lastName": "toto",
                    "friendsId": [],
                    "publicationsId": [
                        "5e6fe4e7ab71f531affbc600",
                        "5e6fe541ab71f531affbc601",
                        "5e6fe5ceab71f5567ec02dfe",
                        "5e6fe5f3ab71f5567ec02dff",
                        "5e6fe628ab71f5567ec02e00",
                        "5e6fe6a2ab71f55c35f65ee9",
                        "5e6fe6e7ab71f55e5fe8f17f",
                        "5e6feb96ab71f57d56566473",
                        "5e6fececab71f57d56566477",
                        "5e6fed42ab71f50e28510348",
                        "5e6fed82ab71f50e2851034b"
                    ],
                    "profilePicture": "000000000000000000000001",
                    "registrationDate": "2020-03-16T21:39:16.99+01:00"
                },
                "description": "Publication sur les chiens",
                "tags": [
                    "Chien",
                    "Langue",
                    "Animaux",
                    "quatres pattes"
                ],
                "mentions": [],
                "images": null,
                "createdAt": "2020-03-16T22:20:02.478+01:00",
                "updatedAt": "2020-03-16T22:20:02.686+01:00",
                "score": 0,
                "desc_reg": null
            }
        ]
    },
    "executionTime": "22.349412ms"
}
```

## Version 1.2.3
* Api Security
    * OAuth integration (Delivery 2)
        * Improved redirections on /auth/oauth/{provider}
        
#### OAuth Integration - Redirections 

Because in an HTML `<a>` tag it is not possible to set an header. You can now specify where the api should redirect by the query param.

The old way : 
```http request
GET /v1/auth/oauth/gitlab?redirect=/profile

X-Dev-front-origin='http://localhost:3000'
```
Becomes :
```http request
GET /v1/auth/oauth/gitlab?redirect=localhost:3000/profile
```

The goal of the change is to allow the dev front-end to specify on which port the api should redirect the user
By example if your dev environment is on localhost:3000 you can tel the api to redirect on localhost:3000.

In development mode, any host are allowed.

In production, only `atelias.ovh`

If the host is not allowed you will obtain a response like this :
```http request
GET /v1/auth/oauth/gitlab?redirect=unallowed.host:3000/profile
```
```json
{
  "code": 403,
  "message": "Unallowed redirection to unallowed.host",
  "data":null,
  "executionTime": "9.201306ms"
}
```

## Version 1.2.2
* Api Security
    * OAuth integration (Delivery 2)
        * GitLab
            - Added SignIn
            - Added SignUp
            - Added Link / Unlink
        * Redirect after authenticated
    * Fixed authentication bug
    * Rate limiter
* Api routes
    * Removed /api prefix
    * Added /publications/search
* Refacto responses helpers
    * Added all responses helpers in `req.Writter`
    * Deprecated all the helpers in `api/generic`


### Api Security
#### OAuth integration - GitLab
You can now use GitLab as a authentication provider. Behaviours are exactly the sames as GitHub previous implementation.

The routes are almost the sames as GitHub's except the fact that the `providerName` parameter is `gitlab`. Example :
```http request
GET /v1/auth/oauth/gitlab?redirect=/profile
```
___

#### Redirect after authentication
When you hit the route `/oauth/:providerName`, it is recommended to add a `redirect` parameter to specify where the API should redirect after authentication success / failure. 
If `redirect` is not specified, then the api will redirect to the default endpoint `/`.
The API will by default ever redirect to a production server (aka `atelias.ovh`).
To redirect to an other host, you must set the `X-Dev-front-origin` header. 

By example : 
```http request
GET /v1/auth/oauth/gitlab?redirect=localhost:3000/profile

X-Dev-front-origin='http://localhost:3000'
```
Will redirect by example to : `http://localhost:3000/profile?message=Success&status=ok`

The redirected route will have two parameters, `status` and `message`.
* Status can be either `ok` or `ko`
* Message is a printable describing the status. It is always set when an error occurred. 
```http request
GET http://atelias.ovh/profile?status=ok&message=Success
```
___
#### Rate limiter
In order to control the ressources and control any scrapping tentatives, the API is now equipped with a Rate Limiter.

By example a logged user can call 2000 times the api per 15 minutes. If the user exceeds this limit, he will be blocked until the end of the 15 minutes.
This limit is set at 1000 calls for an anonymous user.

The rate limiter provides 3 headers :
```http request
GET http://api.atelias.ovh/any/route

X-Ratelimit=1000
X-Ratelimit-Remaining=999
X-Ratelimit-Reset=1583211128
```
* `X-Ratelimit` is the limit the user can call the API
* `X-Ratelimit-Remaining` is the remaining calls the user have
* `X-Ratelimit-Reset` is the timestamp when the limit returns to `X-Ratelimit`

If the user reach the calls limit it will get that type of response :
```json
{
    "code": 403,
    "message": "You've reach the API rate limit. Try again later",
    "data": null,
    "executionTime": "5.931791ms"
}
```

##### Note theses are stupid limits. This is just for the project purposes and to demonstrate the good working of the functionality
___
#### Fixed authentication bug
Since the Version 1.2.1, the /sign-in, /sign-up routes were not setting the authentication cookies. This is now fixed.


### Api Routes
#### Removed /api prefix
Because we're now using `api.atelias.ovh` as api server, the route prefix `/api` is now redundant.
Api routes shall now start with `/vX` where `X` is the api version.
___

#### Added /publications/search (Delivery 2)
As expected in the Delivery 2, it is now possible to retrieve some publications by a querystring. 
This route is obviously paged and provide some parameters to customize the request.
This route follows the privacy policy too. If a user is not public, you can't see his publications except if the authenticated user is friend with the private account.
The research algorithm is implemented as follow : 
- The string correspondence case insensitive as a regex (example: 'lorem ipsum' will be evaluated as the following regex: 'lorem\\s+ipsum'). Actually about 75%
- The user friendship with the author. Actually about 25%

We will further add the popularity of the publication (comments, likes ...)
For example with this request : 
```http request
GET /v1/publications/search?q=Lorem
```
You will obtain something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "page": 1,
        "size": 20,
        "next": null,
        "prev": null,
        "total": 1,
        "items": [
            {
                "id": "5e4dcdc3a1fabc0001c952a1",
                "author": {
                    "id": "5e4cd64819bab3000132b0f4",
                    "username": "zacklbd",
                    "profilePicture": null
                },
                "description": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ",
                "match": {
                    "match": "lorem",
                    "index": 77,
                    "score": 0.004335260115606936,
                    "pretty": "... um do<q>lorem</q>que laudantium, totam rem aper ..."
                }
            }
        ]
    },
    "executionTime": "4.811428723s"
}
```
More information about this route [here](https://ref.atelias.ovh/#/publications/get_publications_search)

## Version 1.2.1
* OAuth integration
    * Added a way to sign-up with OAuth
    * Added a way to unlink an OAuth provider without loosing account
    * Added a way to link further an account with OAuth
* Improved authentication
    * Added a way to get information about all tokens issued
    * Added a way to revoke one of these tokens
* Added User account deletion
   
### OAuth integration
#### Signing up with OAuth (Delivery 2)
You can sign-up with an OAuth provider. (Only GitHub at this time more will comming).
OAuth will not automatically provide all the necessary informations to create an account.
So if you sign-up with OAuth you must complete your profile before continuing. Otherwise all
the API routes - except GET & PATCH /users/me - will return a 403 error.  

```http request
GET /api/v1/auth/oauth/:providerName?redirect=localhost:3000/completeProfile
```
The parameter `redirect` is optional. By default the API will redirect to `atelias.ovh`. 
This parameter will prevent this behavior.

___ 
#### Signing in with OAuth (Delivery 2)
You can sign-in with an OAuth provider. (Only GitHub at this time)
To sign-in this is the same route with the same behavior except the `redirect` parameter. 
You can specify one that redirect to the user profile instead of his edition page.
___
#### Retreive the current linked OAuth providers (Bonus)
You can now retrieve your auth providers via this route.
```
Restrictions :
    - User itself only
```
```http request
GET /api/v1/users/me/security/authentication/providers
```
This route will return something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "github": {
            "foreignId": "01234567",
            "foreignMail": "adresse@mail.fr",
            "foreignName": "username",
            "linkedAt": "2020-02-26T17:33:26.654-05:00"
        }
    },
    "executionTime": "6.989938ms"
}
```
----
#### Remove an OAuth provider (Bonus RGPD compliant)
You can also "unlink" an OAuth provider. 
This behavior is for if you want to stop logging with an auth provider, then you can.
```
Restrictions :
    - User itself only
    - Have a Username and a Mail and a Password set
```
```http request
GET /api/v1/users/me/security/authentication/providers/:providerName/unlink
```
Or:
```http request
DELETE /api/v1/users/me/security/authentication/providers/:providerName/unlink
```
---
#### Link a new OAuth provider (Bonus)
You can link a new provider. Simply use this route when you're logged in. 
This will automatically link your account with the provider
```http request
GET /api/v1/auth/oauth/:providerName?redirect=localhost:3000/completeProfile
```

### Authentication improvements
#### Retrieve the authenticated devices (Bonus)
You can now retrieve your authenticated devices. This will allow you to revoke your account access for an authenticated device.
```
Restrictions : 
    - User itself only
``` 
```http request
GET /api/v1/users/me/security/authentication/tokens
```
This route will return something like this :
```json
{
    "code": 200,
    "message": "OK",
    "data": [
        {
            "id": "5e56f236a54d75896c60bb8a",
            "issuedAt": "2020-02-26T17:33:26-05:00",
            "remoteAddr": "127.0.0.1:53170",
            "userAgent": {
                "name": "Safari",
                "version": "13.0.5",
                "device": "Desktop",
                "os": "macOS"
            }
        },
        {
            "id": "5e56f25ca54d75896c60bb8b",
            "issuedAt": "2020-02-26T17:34:04-05:00",
            "remoteAddr": "127.0.0.1:53145",
            "userAgent": {
                "name": "PostmanRuntime",
                "version": "7.22.0",
                "device": "",
                "os": ""
            }
        }
    ],
    "executionTime": "7.339606ms"
}
```
---
#### Revoke a device access : (Bonus)
You can at any time revoke a device authentication by calling the following route.
```
Restrictions : 
    - User itself only
```
```http request
GET /api/v1/users/me/security/authentication/tokens/revoke?token=5e56f25ca54d75896c60bb8b
```
Or
```http request
DELETE /api/v1/users/me/security/authentication/tokens/revoke?token=5e56f25ca54d75896c60bb8b
```

### User improvements
#### User account deletion (Delivery 2)
You can now delete a user. The deletion will not take instantly effect but it will be done hourly. 
```
Restrictions :
    - User itself only
```
```http request
DELETE /api/v1/users/me
```
You should obtain something like this. This action can't be canceled and it's definitive. This action will remove all the user publications, images, comments and likes.
```json
{
    "code": 200,
    "message": "Deletion take in count. All access tokens has been revoked. Deletion will take fully effect around Feb 27 00:00:00 +/- 20 min",
    "data": "2020-02-27T00:00:00-05:00",
    "executionTime": "38.654961ms"
}
```
___
___

## Version 1.1.6

* Added profile pictures management
* Refactored publications responses 

### Profile picture management

Added a way to add a profile picture.

#### Post on `/auth/sign-up` has changed. Added an optional parameter `profilePicture` in the request body.

Request body can now be :
```json
{
  "username": "string",
  "password": "string",
  "mail": "string",
  "firstname": "string",
  "lastname": "string",
  "phoneNumber": {
    "isoCode": "string",
    "number": "string"
  },
  "profilePicture": "optional base64 URI"
}
```

#### Patch on `/users/me` has changed. Added an optional parameter `profilePicture` in the request body.

Request body can now be : 
```json
{
  "mail": "string",
  "firstname": "string",
  "lastname": "string",
  "phoneNumber": {
    "isoCode": "string",
    "number": "string"
  },
  "profilePicture": "optional base64 URI"
}
```

#### Added a way to get the user's profile picture. 
The route is : `/:handle/profilepicture/:imageformat`
The parameters are : 

| `handle`      | The user's id                                    
|---------------|--------------------------------------------------
| `imageformat` | One in following : `small`, `medium`, `original`

This route return an image (mime type : `image/png`).

#### Changed "publications" responses
The responses of 
* POST `/publications`
* GET `/publications/:id`
* PATCH `/publications/:id`
* POST `/publications/:id/images`

were actually something like this : 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e4b7b33a54d751bd13ca4b1",
        "author": "5e4b1618a54d7507f6161689",
        "description": "Bonjour",
        "tags": [
            "test",
            "dev"
        ],
        "images": [
            "5e4b7b33a54d751bd13ca4b2",
            "5e4b7b33a54d751bd13ca4b3",
            "5e4b7b89a54d751bd13ca4b4",
            "5e4b7b8aa54d751bd13ca4b5"
        ],
        "createdAt": "2020-02-18T00:50:43.074-05:00",
        "updatedAt": "2020-02-18T00:52:10.141433-05:00"
    },
    "executionTime": "27.0896ms"
}
```
it is now : 
```json
{
    "code": 200,
    "message": "OK",
    "data": {
        "id": "5e4b7b33a54d751bd13ca4b1",
        "author": {
            "id": "5e4b1618a54d7507f6161689",
            "username": "username",
            "firstName": "Jean",
            "lastName": "Paul",
            "publicationsId": [
                "5e4b7b33a54d751bd13ca4b1"
            ],
            "profilePicture": null,
            "registrationDate": "2020-02-17T17:39:20.289-05:00"
        },
        "description": "Bonjour",
        "tags": [
            "test",
            "dev"
        ],
        "images": [
            {
                "id": "5e4b7b33a54d751bd13ca4b2",
                "medium": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/medium/5e4b7b33a54d751bd13ca4b2",
                "small": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/small/5e4b7b33a54d751bd13ca4b2",
                "original": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/original/5e4b7b33a54d751bd13ca4b2"
            },
            {
                "id": "5e4b7b33a54d751bd13ca4b3",
                "medium": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/medium/5e4b7b33a54d751bd13ca4b3",
                "small": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/small/5e4b7b33a54d751bd13ca4b3",
                "original": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/original/5e4b7b33a54d751bd13ca4b3"
            },
            {
                "id": "5e4b7b89a54d751bd13ca4b4",
                "medium": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/medium/5e4b7b89a54d751bd13ca4b4",
                "small": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/small/5e4b7b89a54d751bd13ca4b4",
                "original": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/original/5e4b7b89a54d751bd13ca4b4"
            },
            {
                "id": "5e4b7b8aa54d751bd13ca4b5",
                "medium": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/medium/5e4b7b8aa54d751bd13ca4b5",
                "small": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/small/5e4b7b8aa54d751bd13ca4b5",
                "original": "/api/v1/publications/5e4b7b33a54d751bd13ca4b1/images/original/5e4b7b8aa54d751bd13ca4b5"
            }
        ],
        "createdAt": "2020-02-18T00:50:43.074-05:00",
        "updatedAt": "2020-02-18T00:52:10.141433-05:00"
    },
    "executionTime": "27.0896ms"
}
```

The author is no more an ID and the image are now no more an array of id too. 
