package crontasks

import (
	"api/repository"
)

func doRevokeExpiredTokens() {
	_ = repository.TokensRepository.RevokeOldTokens()
}