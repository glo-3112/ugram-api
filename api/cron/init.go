package crontasks

import (
	"log"

	"github.com/robfig/cron/v3"
)

type cronTaskManager struct {
	c            *cron.Cron
	deleteUser   cron.EntryID
	revokeTokens cron.EntryID
	packUserData cron.EntryID
}

var CronTaskManager *cronTaskManager

func init() {
	CronTaskManager = &cronTaskManager{
		c: cron.New(),
	}

	var err error
	CronTaskManager.deleteUser, err = CronTaskManager.c.AddFunc("*/5 * * * *", deleteUsers)
	CronTaskManager.revokeTokens, err = CronTaskManager.c.AddFunc("*/5 * * * *", revokeOldTokens)
	CronTaskManager.packUserData, err = CronTaskManager.c.AddFunc("*/5 * * * *", doBundleUserData)
	CronTaskManager.packUserData, err = CronTaskManager.c.AddFunc("*/5 * * * *", doRemoveBundlesUsersData)
	CronTaskManager.packUserData, err = CronTaskManager.c.AddFunc("*/5 * * * *", doRevokeExpiredTokens)
	if err != nil {
		log.Fatal(err)
	}
	go CronTaskManager.c.Run()
}
