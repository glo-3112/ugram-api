package crontasks

import (
	"fmt"

	"api/repository"
)

func revokeOldTokens() {
	err := repository.TokensRepository.RevokeOldTokens()
	if err != nil {
		fmt.Println(err)
	}
}
