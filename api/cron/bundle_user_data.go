package crontasks

import (
	"time"

	"github.com/globalsign/mgo/bson"

	"api/models"
	"api/repository"
)

func (c *cronTaskManager) AddPackUserDataCronJob(user *models.User) (*time.Time, error) {
	request := &models.BundleUserData{
		Id:          bson.NewObjectId(),
		RequestedAt: time.Now(),
		CreatedAt:   nil,
		UserId:      user.Id,
	}

	err := repository.PendingRequestUserDataRepository.Add(request)
	if err != nil {
		return nil, err
	}

	user.RequestedUserData = append(user.RequestedUserData, request)
	_ = repository.UsersRepository.Persist(user)

	prevision := c.c.Entry(c.revokeTokens).Next
	return &prevision, nil
}
