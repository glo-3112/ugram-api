package crontasks

import (
	"time"

	"github.com/globalsign/mgo/bson"

	"api/constants"
	"api/models"
	"api/repository"
	"api/tools/filemanager"
)

func deleteUsers() {
	nbDelete := repository.PendingUserDeleteRepository.Count()

	for i := 0; i < nbDelete; i++ {
		user := repository.PendingUserDeleteRepository.GetOne()

		// Deleting publications
		for _, pubId := range user.PublicationIds {
			// Retrieving the publication
			pub, _ := repository.PublicationsRepository.FindByObjectId(pubId)

			// Deleting the publication in db
			_ = repository.PublicationsRepository.DeleteByObjectId(pubId)

			// Deleting the publication images
			_ = filemanager.DeletePicturesByObjectId(pub.Images...)
		}

		if user.ProfilePicture != constants.DefaultProfilePicture {
			_ = filemanager.DeletePicturesByObjectId(user.ProfilePicture)
		}

		// Removing all user's mentions
		_ = repository.PublicationsRepository.UnmentionUser(user.Id)
	}
}

func (c *cronTaskManager) AddDeleteUserIdCronJob(uid bson.ObjectId) *time.Time {
	user, _ := repository.UsersRepository.FindByObjectId(uid)

	return c.AddDeleteUserCronJob(user)
}

func (c *cronTaskManager) AddDeleteUserCronJob(user *models.User) *time.Time {
	_ = repository.TokensRepository.RevokeUser(user.Id)

	_ = repository.UsersRepository.Remove(user.Id)

	if user == nil {
		return nil
	}
	repository.PendingUserDeleteRepository.Add(user)

	deletion := c.c.Entry(c.deleteUser).Next
	return &deletion
}
