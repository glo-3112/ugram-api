package crontasks

import (
	"archive/zip"
	"encoding/csv"
	"fmt"
	"io"
	"path"
	"strconv"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/sirupsen/logrus"
	"github.com/globalsign/mgo/bson"

	"api/models"
	"api/repository"
	"api/tools/filemanager"
)

func doPackUser(files map[string][][]string, images *[]bson.ObjectId, user *models.User) {
	files["user.csv"] = [][]string{
		{"Id", "Roles", "Username", "First Name", "Last Name", "Mail", "Phone number", "Auth Provider", "Auth Provider Id", "Auth Provider Name", "Auth Provider Mail", "Account activated", "Account is public", "Friends names", "Profile picture", "Registration date", "Last sign in"},
		{user.Id.Hex(), "", user.Name, user.Firstname, user.Lastname, user.Mail, user.PhoneNumber.International, "", "", "", "", strconv.FormatBool(user.Activated), strconv.FormatBool(user.Public), "", user.ProfilePicture.Hex() + ".png", user.RegistrationDate.Format(time.RFC850), user.LastSignIn.Format(time.RFC850)},
	}
	if user.Roles != nil {
		for i, role := range user.Roles {
			if i < len(files["user.csv"]) {
				files["user.csv"] = append(files["user.csv"], make([]string, len(files["user.csv"])))
			}
			files["user.csv"][i+2][1] = role
		}
	}
	i := 0
	if user.AuthProviders != nil {
		for name, v := range user.AuthProviders {
			if i < len(files["user.csv"]) {
				files["user.csv"] = append(files["user.csv"], make([]string, len(files["user.csv"])))
			}
			files["user.csv"][i+2][6] = name
			files["user.csv"][i+2][7] = v.ForeignId
			files["user.csv"][i+2][8] = v.ForeignName
			files["user.csv"][i+2][9] = v.ForeignMail
		}
	}
	if user.FriendsIds != nil {
		for i, friend := range user.Friends {
			if i < len(files["user.csv"]) {
				files["user.csv"] = append(files["user.csv"], make([]string, len(files["user.csv"])))
			}
			files["user.csv"][i+2][12] = friend.Name
		}
	}
	*images = append(*images, user.ProfilePicture)
}

func doPackPublications(files map[string][][]string, images *[]bson.ObjectId, publications []models.Publication) {
	files["publications.csv"] = [][]string{
		{"Id", "Descriptions", "Tags", "Mentioned users", "Images", "Created At", "Update At"},
	}

	for _, p := range publications {
		line := make([][]string, 1)
		line[0] = []string{
			p.Id.Hex(),
			p.Description,
			"",
			"",
			"",
			p.CreatedAt.Format(time.RFC850),
			p.UpdatedAt.Format(time.RFC850),
		}

		if p.Tags != nil {
			for i, t := range p.Tags {
				if len(line) <= i {
					line = append(line, make([]string, len(line[0])))
				}
				line[i][2] = t
			}
		}

		mentions, _ := repository.UsersRepository.FindByObjectIds(p.MentionsId)
		if mentions != nil {
			for i, m := range mentions {
				if len(line) <= i {
					line = append(line, make([]string, len(line[0])))
				}
				line[i][3] = m.Name
			}
		}
		if p.Images != nil {
			for i, img := range p.Images {
				if len(line) <= i {
					line = append(line, make([]string, len(line[0])))
				}
				line[i][4] = img.Hex() + ".png"
			}
		}
		*images = append(*images, p.Images...)

		files["publications.csv"] = append(files["publications.csv"], line...)
	}
}

func doPackTokens(files map[string][][]string, tokens []*models.TokenInfo) {
	files["connections.csv"] = [][]string{
		{"Address", "Device", "Connection date", "Connection expiry"},
	}

	for _, t := range tokens {
		line := make([][]string, 1)
		line[0] = []string{
			t.RemoteAddr,
			fmt.Sprint(t.UserAgent.Name, t.UserAgent.Version, t.UserAgent.Device, t.UserAgent.Os),
			t.IssuedAt.Format(time.RFC850),
			t.Expiry.Format(time.RFC850),
		}

		files["connections.csv"] = append(files["connections.csv"], line...)
	}
}

func doPack(task *models.BundleUserData, user *models.User, tokens []*models.TokenInfo) {
	files := map[string][][]string{}
	images := make([]bson.ObjectId, 0)

	doPackUser(files, &images, user)
	doPackPublications(files, &images, user.Publications)
	doPackTokens(files, tokens)

	zipName := uuid.NewV4()
	archivePath, err := filemanager.CreateZipFileArchive(user.Name, zipName.String())
	if err != nil {
		fmt.Println(err)
		return
	}
	archive := zip.NewWriter(archivePath)
	defer archive.Close()

	for file, values := range files {
		output, err := archive.Create(file)
		if err != nil {
			fmt.Println(err)
			continue
		}
		writer := csv.NewWriter(output)
		err = writer.WriteAll(values)
		if err != nil {
			fmt.Println(err)
			continue
		}
		writer.Flush()
	}

	for _, image := range images {
		imgHandlers, err := filemanager.GetImageFilesHandlers(image)
		if err != nil {
			fmt.Println(err)
			continue
		}
		for img, reader := range imgHandlers {
			writer, err := archive.Create(path.Join("images/", img))
			if err != nil {
				_ = reader.Close()
				fmt.Println(err)
				continue
			}
			_, err = io.Copy(writer, reader)
			if err != nil {
				_ = reader.Close()
				fmt.Println(err)
				continue
			}
			_ = reader.Close()
		}
	}

	filename := zipName.String()
	createdTime := time.Now()
	expiry := time.Now().Add(time.Hour * 24 * 30)
	task.File = &filename
	task.Expiry = &expiry
	task.CreatedAt = &createdTime
	task.Done = true
}

func doBundleUserData() {
	for task, user, err := repository.PendingRequestUserDataRepository.PopOne(); task != nil; task, user, err = repository.PendingRequestUserDataRepository.PopOne() {
		if err != nil {
			logrus.Error(err)
			continue
		}
		if user == nil {
			continue
		}

		user.Friends, err = repository.UsersRepository.FindByObjectIds(user.FriendsIds)
		if err != nil {
			fmt.Println(err)
		}

		user.Publications, err = repository.PublicationsRepository.FindUserPublications(user.Id)
		if err != nil {
			fmt.Println(err)
		}

		tokens, err := repository.TokensRepository.FindByOwner(user.Id)
		if err != nil {
			fmt.Println(err)
		}

		doPack(task, user, tokens)
		_ = repository.UsersRepository.UpdateRequestUserData(user, task)
	}
}

func doRemoveBundlesUsersData() {
	users, bundles, _ := repository.UsersRepository.FindExpiredBundleUsersData()

	if users == nil || bundles == nil {
		return
	}

	for i, user := range users {
		for idx , bundle := range bundles[i] {
			if bundle.File != nil {
				_ = filemanager.RemoveZipFileArchive(user.Name, *bundle.File)
			}
			user.RequestedUserData[idx].File = nil
		}
		_ = repository.UsersRepository.Persist(user)
	}
}