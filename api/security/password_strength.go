package security

import (
	"errors"
	"unicode"
)

var unicodeCharset = map[string][]*unicode.RangeTable{
	"UpperCase": {unicode.Upper, unicode.Title},
	"LowerCase": {unicode.Lower},
	"Numeric":   {unicode.Number, unicode.Digit},
	"Special":   {unicode.Space, unicode.Symbol, unicode.Punct, unicode.Mark},
}

func PasswordStrength(password string) error {
	fail := ""
	switch {
	case len(password) < 8:
		return errors.New("Password not long enough")
	}
	for _, r := range password {
		for key, classes := range unicodeCharset {
			fail = key
			if unicode.IsOneOf(classes, r) {
				fail = ""
				break
			}
		}
		if fail != "" {
			return errors.New("Password should contain at least one special character")
		}
	}
	return nil
}
