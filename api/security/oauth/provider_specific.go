package oauth

import (
	constants "api/constants/security"
	"api/models"
	"api/req"
)

func getGitHubUserInfos(w *req.Writer, r *req.Reader) (*models.OauthUser, error) {
	user := models.GitHubUser{}
	tkn, err := doOAuthCallback(constants.GetGitHubConfig(), r.Query().Get("code"), constants.GithubUserUri, &user)
	ret := &models.OauthUser{
		Id:        user.Id,
		UserName:  user.UserName,
		Name:      user.Name,
		Mail:      user.Mail,
		AvatarUrl: user.AvatarUrl,
		Token:     tkn,
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}

func getGitLabUserInfos(w *req.Writer, r *req.Reader) (*models.OauthUser, error) {
	user := models.GitLabUser{}
	tkn, err := doOAuthCallback(constants.GetGitLabConfig(), r.Query().Get("code"), constants.GitlabUserUri, &user)
	ret := &models.OauthUser{
		Id:        user.Id,
		UserName:  user.UserName,
		Name:      user.Name,
		Mail:      user.Mail,
		AvatarUrl: user.AvatarUrl,
		Token:     tkn,
	}
	if err != nil {
		return nil, err
	}
	return ret, nil
}
