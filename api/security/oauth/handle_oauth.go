package oauth

import (
	"fmt"
	"net/url"
	"os"
	"strings"

	"golang.org/x/oauth2"

	constants "api/constants/security"
	"api/models"
	"api/req"
)

func HandleOAuthRedirect(w *req.Writer, r *req.Reader, providerName string) {
	var providerConfig oauth2.Config
	switch strings.ToLower(providerName) {
	case "github":
		if constants.GetGitHubConfig().ClientID == "" || constants.GetGitHubConfig().ClientSecret == "" {
			w.SendNotImplemented(fmt.Errorf("Github not allowed"), "The current server configuration does not allow GitHub as OAuth provider")
			return
		}
		providerConfig = constants.GetGitHubConfig()
	case "gitlab":
		if constants.GetGitLabConfig().ClientID == "" || constants.GetGitLabConfig().ClientSecret == "" {
			w.SendNotImplemented(fmt.Errorf("Gitlab not allowed"), "The current server configuration does not allow GitLab as OAuth provider")
			return
		}
		providerConfig = constants.GetGitLabConfig()
	}
	doOAuthRedirect(w, r, providerConfig)
}

func HandleOAuthCallback(w *req.Writer, r *req.Reader, redirect *url.URL, providerName string) *models.OauthUser {
	ret := &models.OauthUser{}
	var err error

	expectedState, err := r.Cookie(constants.OAuthStateCookieName)
	if err != nil {
		w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "An error occurred while authenticating the token")
		return nil
	}

	if expectedState.Value != r.Query().Get("state") {
		w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "An error occurred while authenticating the token")
	}

	switch strings.ToLower(providerName) {
	case "github":
		if os.Getenv(constants.GithubClientId) == "" || os.Getenv(constants.GithubClientSecret) == "" {
			w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
			w.RedirectQueryTemporary(redirect, "status", "ko", "message", "The current server configuration does not allow GihHub as OAuth provider")
			return nil
		}
		ret, err = getGitHubUserInfos(w, r)
		break
	case "gitlab":
		if os.Getenv(constants.GitlabClientId) == "" || os.Getenv(constants.GitlabClientSecret) == "" {
			w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
			w.RedirectQueryTemporary(redirect, "status", "ko", "message", "The current server configuration does not allow GitLab as OAuth provider")
			return nil
		}
		ret, err = getGitLabUserInfos(w, r)
		break
	default:
		w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "The current server configuration does not allow"+providerName+"as OAuth provider")
		return nil
	}
	if err != nil {
		w.UnsetCookies(constants.OAuthStateCookieName, constants.OAuthRedirectCookieName)
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "Can't retrieve the desired user:"+err.Error())
		return nil
	}
	return ret
}
