package oauth

import (
	"context"
	"crypto/rand"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"time"

	"github.com/sirupsen/logrus"
	"golang.org/x/oauth2"

	constants "api/constants/security"
	"api/req"
)

func doOAuthRedirect(w *req.Writer, r *req.Reader, config oauth2.Config) {
	uri, err := url.Parse(config.Endpoint.AuthURL)
	if err != nil {
		w.SendInternalError(err, "An unknown error occured")
		return
	}

	b := make([]byte, constants.OAuthStateStringLength)
	_, _ = rand.Read(b)
	state := base64.URLEncoding.EncodeToString(b)
	http.SetCookie(w, &http.Cookie{
		Name:     constants.OAuthStateCookieName,
		Value:    state,
		HttpOnly: constants.OAuthStateCookieHttpOnly,
		Secure:   os.Getenv(constants.CanSecureCookies) == constants.OAuthStateCookieSecure,
		Expires:  time.Now().Add(constants.OAuthStateCookieExpiry),
		Path:     constants.OAuthStateCookiePath,
	})

	q := uri.Query()
	q.Set("client_id", config.ClientID)
	q.Set("redirect_uri", config.RedirectURL)
	q.Set("response_type", "code")
	q.Set("state", state)

	uri.RawQuery = q.Encode()
	w.Header().Add("Cache-Control", "no-store")

	http.Redirect(w, r.Req, uri.String(), http.StatusTemporaryRedirect)
}

func doOAuthCallback(providerConfig oauth2.Config, code string, getUserUri string, reading interface{}) (*oauth2.Token, error) {
	// Retrieving the authentication token
	token, err := providerConfig.Exchange(context.Background(), code)
	if err != nil {
		return nil, fmt.Errorf("code exchange wrong: %s", err.Error())
	}

	// Checking if the request throws any error
	response, err := http.Get(getUserUri + "?access_token=" + token.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("failed getting user info: %s", err.Error())
	}
	defer response.Body.Close()

	// Reading the user information request response
	contents, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, fmt.Errorf("failed read response: %s", err.Error())
	}

	// Checking if the request is good
	if response.StatusCode < 200 || response.StatusCode >= 400 {
		logrus.Println("Getting the user infos returns : ", string(contents))
		return nil, fmt.Errorf("failed getting user infos: %s", response.Status)
	}

	err = json.Unmarshal(contents, reading)
	if err != nil {
		return nil, fmt.Errorf("failed parse response: %s", err.Error())
	}

	return token, nil
}
