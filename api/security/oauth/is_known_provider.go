package oauth

import (
	"strings"

	constants "api/constants/security"
)

func IsKnownProvider(provider string) bool {
	for _, v := range constants.GetSupportedProviders() {
		if strings.ToLower(v) == strings.ToLower(provider) {
			return true
		}
	}
	return false
}
