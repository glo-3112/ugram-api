package security

import (
	"fmt"

	"github.com/globalsign/mgo/bson"
	"gopkg.in/square/go-jose.v2"
	"gopkg.in/square/go-jose.v2/jwt"

	securityconst "api/constants/security"
	"api/models"
	"api/repository"
	"api/req"
)

var signingKey = jose.SigningKey{Algorithm: jose.HS512, Key: []byte(securityconst.JWTSigningKey)}

func CreateJwt(s *req.SessionInfo, user models.User, userAgent string, remote string) string {
	signer, err := jose.NewSigner(signingKey, (&jose.SignerOptions{}).WithType("JWT"))
	if err != nil {
		panic(err)
	}

	claims := CreateClaims(user)
	token, err := jwt.Signed(signer).Claims(claims).CompactSerialize()
	if err != nil {
		panic(err)
	}

	infos := &models.TokenInfo{
		Id:         bson.NewObjectId(),
		Uuid:       claims.ID,
		Expiry:     claims.Expiry.Time(),
		IssuedAt:   claims.IssuedAt.Time(),
		UserId:     user.Id,
		UserAgent:  models.CreateUserAgentFromString(userAgent),
		IssuedFor:  fmt.Sprint("Remote ", s.Ip, " at ", s.Country + ", ", s.City),
		RemoteAddr: remote,
	}

	_, err = repository.TokensRepository.Insert(infos)
	if err != nil {
		panic(err)
	}

	return token
}
