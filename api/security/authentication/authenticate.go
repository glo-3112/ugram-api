package authentication

import (
	"net/http"
	"os"
	"strings"
	"time"

	constants "api/constants/security"
	"api/models"
	"api/req"
	security "api/security/jwt"
)

func AuthenticateUser(s *req.Session, w *req.Writer, r *req.Reader, user *models.User) {
	// Making the token and creating associated cookies
	jwt := security.CreateJwt(s.Infos, *user, r.Req.UserAgent(), r.Req.RemoteAddr)
	domain := s.Infos.Host
	if strings.HasSuffix(domain, "atelias.ovh") {
		domain = "atelias.ovh"
	} else {
		domain = "localhost"
	}

	token := &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    jwt,
		HttpOnly: constants.TokenCookieHttpOnly,
		Secure:   os.Getenv(constants.CanSecureCookies) == constants.TokenCookieSecure,
		// MaxAge:   constants.TokenCookieMaxAge,
		Expires: time.Now().Add(constants.TokenCookieExpiry),
		Path:    constants.TokenCookiePath,
	}
	auth := &http.Cookie{
		Name:     constants.AuthenticationCookieName,
		Value:    "true",
		HttpOnly: constants.AuthenticationCookieHttpOnly,
		Secure:   os.Getenv(constants.CanSecureCookies) == constants.AuthenticationCookieSecure,
		// MaxAge:   constants.AuthenticationMaxAge,
		Expires: time.Now().Add(constants.AuthenticationCookieExpiry),
		Path:    constants.AuthenticationCookiePath,
		// Domain:  domain,
	}

	// Setting cookies
	http.SetCookie(w, token)
	http.SetCookie(w, auth)
}
