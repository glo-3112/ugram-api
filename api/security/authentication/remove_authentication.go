package authentication

import (
	"net/http"

	constants "api/constants/security"
	"api/req"
)

func RemoveAuthentication(w *req.Writer) {
	// Setting age to 1 (has the effect of deleting the cookie)
	token := &http.Cookie{
		Name:     constants.TokenCookieName,
		Value:    "",
		HttpOnly: constants.TokenCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.TokenCookiePath,
	}
	auth := &http.Cookie{
		Name:     constants.AuthenticationCookieName,
		Value:    "false",
		HttpOnly: constants.AuthenticationCookieHttpOnly,
		MaxAge:   1,
		Path:     constants.AuthenticationCookiePath,
	}

	// Setting cookies update
	http.SetCookie(w, token)
	http.SetCookie(w, auth)
}
