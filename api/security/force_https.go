package security

import (
	"net/url"
	"strings"
)

func ForceHttps(uri string) (*url.URL, error) {
	if !strings.HasPrefix(uri, "http") {
		uri = "http://" + uri
	}

	ret, err := url.Parse(uri)
	if err != nil {
		return nil, err
	}

	if ret.Hostname() != "localhost" {
		ret.Scheme = "https"
	} else {
		ret.Scheme = "http"
	}
	return ret, nil
}
