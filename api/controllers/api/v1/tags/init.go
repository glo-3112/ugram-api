package tags

import (
	. "api/middlewares"
	"api/router"
	"net/http"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/tags")

	Router.Register("/", MultiMethod(Methods{
		http.MethodGet: {GetTagsController},
		http.MethodPost: {PostTagController},
	}))

	Router.Register("/partials", MultiMethod(Methods{
		http.MethodGet: {GetTagContainingSubStr},
	}))

	Router.Register("/{tagId}", MultiMethod(Methods{
		http.MethodGet: {GetSingleTagController},
	}))
}
