package tags

import (
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

func GetTagsController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 20)

	tags, err := repository.TagsRepository.GetAllTags(page, size)

	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(r.MakePagedResponse(page, size, tags))
}

func PostTagController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	tag := &models.AddTag{}

	if err := r.UnmarshalJSON(&tag); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	newTag, err := repository.TagsRepository.CreateTag(tag.Name)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(newTag)
}

func GetTagContainingSubStr(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	partialTag := r.Req.URL.Query().Get("q")
	tags, err := repository.TagsRepository.GetContainingTags(partialTag)

	if tags == nil {
		w.SendNotFound(nil)
		return
	}
	if err != nil {
		w.SendInternalError(err, "An unknown error occured")
		return
	}

	w.SendOk(tags)
}
