package auth

import (
	form "api/form/api/v1"
	"api/repository"
	"api/req"
	"api/security/authentication"
	. "api/tools"
)

// Description :
//		Method POST
//		This controllers register a user using the given body
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PostSignInController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	// Parsing json body
	formData := form.SignIn{}
	if err := r.UnmarshalJSON(&formData); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	// Looking if the user is existing or not
	user, err := repository.UsersRepository.FindByLoginAndPassword(formData.Login, formData.Password)
	if err != nil || user == nil {
		w.SendForbidden(err, "No combination login password found")
		return
	}

	err = repository.UsersRepository.UpdateLastSignIn(user)
	if err != nil {
		w.SendForbidden(err, "An error occured while authenticating")
		return
	}

	authentication.AuthenticateUser(s, w, r, user)

	// Respond to client
	w.SendOk(user)
}
