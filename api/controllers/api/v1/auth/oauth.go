package auth

import (
	"fmt"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"

	"github.com/sirupsen/logrus"

	constants2 "api/constants"
	constants "api/constants/security"
	"api/models"
	"api/repository"
	"api/req"
	"api/security"
	"api/security/authentication"
	"api/security/oauth"
	"api/tools"
)

// Description :
//		Method GET
//		This controller redirect the user to the desired OAuth provider
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetOAuthController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	providerName := r.Params["providerName"]
	if providerName == "" {
		w.SendBadRequest(nil, "You must specify a valid oauth server")
		return
	}

	if !oauth.IsKnownProvider(providerName) {
		w.SendBadRequest(nil, providerName, "is not a known oauth provider")
		return
	}

	redirectUri := r.Query().Get("redirect")
	if strings.HasPrefix("http://", redirectUri) {
		redirectUri = "https://" + strings.TrimPrefix("http://", redirectUri)
	} else if !strings.HasPrefix("https://", redirectUri) {
		redirectUri = "https://" + redirectUri
	}

	redirect, err := url.Parse(redirectUri)
	if err != nil {
		panic(err)
	}

	if os.Getenv(constants2.DevEnv) != "" {
		switch strings.ToLower(redirect.Hostname()) {
		case "localhost":
			redirect.Host = "localhost:" + redirect.Port()
			redirect.Scheme = "http"
		case "atelias.ovh":
			redirect.Host = "atelias.ovh"
			redirect.Scheme = "https"
		default:
			w.SendForbidden(nil, "Unallowed redirection to", redirect.Hostname())
			return
		}
	}

	http.SetCookie(w, &http.Cookie{
		Name:     constants.OAuthRedirectCookieName,
		Value:    redirect.String(),
		HttpOnly: constants.OAuthRedirectCookieHttpOnly,
		Secure:   os.Getenv(constants.CanSecureCookies) == constants.OAuthRedirectCookieSecure,
		Expires:  time.Now().Add(constants.OAuthRedirectCookieExpiry),
		Path:     constants.OAuthRedirectCookiePath,
	})

	oauth.HandleOAuthRedirect(w, r, providerName)
}

// Description :
//		Method GET
//		This controllers is the endpoint where the provider should redirect.
//	    This controller is the endpoint to :
//	        - authenticate the user if the user is known and linked to the given provider
//	        - link the provider to the user if the user is known but not linked to the provider
//	        - register the user otherwise
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetOAuthRedirectController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	status := ""

	w.UnsetCookies(constants.OAuthRedirectCookieName, constants.OAuthStateCookieName)
	expectedRedirection, err := r.Cookie(constants.OAuthRedirectCookieName)
	if err != nil {
		w.SendBadRequest(err, "An error occurred while determining where the user should be redirected")
		return
	}
	redirect, err := security.ForceHttps(expectedRedirection.Value)
	if err != nil {
		w.SendBadRequest(err, "An error occurred while determining where the user should be redirected")
		return
	}

	// Checking if the provider is known and valid
	providerName := r.Params["providerName"]
	if providerName == "" {
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "You must specify a valid oauth server")
		return
	}

	if !oauth.IsKnownProvider(providerName) {
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", providerName+"is not a known oauth provider")
		return
	}

	// Handle the callback
	oauthUser := oauth.HandleOAuthCallback(w, r, redirect, providerName)

	if oauthUser == nil {
		return
	}

	user := s.User

	// If the user is authenticated then we link the provider to the user
	if s.User != nil {
		if s.User.AuthProviders == nil {
			s.User.AuthProviders = map[string]models.AuthProviderInformations{}
		}
		if _, ok := s.User.AuthProviders[providerName]; !ok {
			s.User.AuthProviders[providerName] = models.AuthProviderInformations{
				ForeignId:    fmt.Sprint(oauthUser.Id),
				ForeignMail:  oauthUser.Mail,
				ForeignName:  oauthUser.Name,
				LinkedAt:     time.Now(),
				ForeignToken: oauthUser.Token,
			}
			err := repository.UsersRepository.Persist(s.User)
			if err != nil {
				w.RedirectQueryTemporary(redirect, "status", "ko", "message", "An error occurred while linking "+providerName+" auth provider")
				return
			}
			status = "linked"
		} else {
			// The user is authenticated and the provider is already known. Do nothing
			w.RedirectTemporary(redirect)
			return
		}
		goto end
	}

	// If the user is not authenticated then it's possibly a known user. So check if the user is known.
	user, err = repository.UsersRepository.GetUserByOauthProvider(providerName, oauthUser)
	if err != nil {
		logrus.Error(err)
		w.RedirectQueryTemporary(redirect, "status", "ko", "message", "An error occurred while retrieving the user")
		return
	}

	// If the user is not known, then it's a new user. So we create him.
	if user == nil {
		user, err = repository.UsersRepository.CreateUserByOauthProvider(providerName, oauthUser)
		if err != nil {
			logrus.Error(err)
			w.RedirectQueryTemporary(redirect, "status", "ko", "message", "An error occurred while creating the user")
			return
		}
		status = "registered"
	} else {
		status = "authenticated"
	}

end:
	switch status {
	case "linked":
		w.RedirectQueryTemporary(redirect, "status", "ok", "message", "Successfully linked "+providerName)
		break
	case "registered":
		authentication.AuthenticateUser(s, w, r, user)
		w.RedirectQueryTemporary(redirect, "status", "ok", "message", "Successfully signed in")
		break
	case "authenticated":
		authentication.AuthenticateUser(s, w, r, user)
		w.RedirectTemporary(redirect)
		break
	}
}
