package auth

import (
	"api/repository"
	"api/req"
	"api/security/authentication"
	"api/tools"
)

// Description :
//		Method POST
//		This controllers sign out a user
// Parameters :
//      s *req.Session :
//          The current user session (Can't be equal to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func SignOutController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	authentication.RemoveAuthentication(w)

	err := repository.TokensRepository.DeleteById(s.Token.Id)
	if err != nil {
		w.SendInternalError(err)
	}

	// Respond to client
	w.SendOk(nil, "Successfully disconnected")
}
