package auth

import (
	"github.com/globalsign/mgo/bson"

	form "api/form/api/v1"
	"api/repository"
	"api/req"
	"api/security"
	"api/tools"
	"api/tools/filemanager"
	"api/tools/validators"
)

// Description :
//		Method POST
//		This controllers sign in a user using the given body
// Parameters :
//      s *req.Session :
//          The current user session (Can't be equal to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PostSignUpController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	// Getting request body

	// Parsing json body
	form := form.SignUp{}
	if err := r.UnmarshalJSON(&form); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if err := security.PasswordStrength(form.Password); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if err := validators.IsMailValid(form.Mail); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	// Verifying the phone number
	if err := validators.IsPhoneValid(form.PhoneNumber); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	// Looking if the user is existing or not
	status, err := repository.UsersRepository.IsUsernameOrMailAvailable(form.Username, form.Mail)
	if err != nil {
		w.SendInternalError(err, "An unknown error occured")
		return
	}

	if status == false {
		w.SendConflict(nil, "Username or mail address already taken")
		return
	}

	user, err := repository.UsersRepository.CreateUserNoHashPassword(form)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	if form.ProfilePicture != "" {
		profilePicture := bson.NewObjectId()
		err = filemanager.SaveBase64Picture(form.ProfilePicture, profilePicture.Hex())
		if err != nil {
			w.SendInternalError(err, "An unknown error occured")
			return
		}

		user.ProfilePicture = profilePicture
		err = repository.UsersRepository.Persist(user)
		if err != nil {
			w.SendInternalError(err, "An unknown error occured")
			return
		}
	}

	// Respond to client
	w.SendOk(nil)
	return
}
