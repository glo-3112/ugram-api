package auth

import (
	"net/http"

	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/auth")

	Router.Register("/sign-in", CheckMethod(http.MethodPost), PostSignInController)
	Router.Register("/sign-up", CheckMethod(http.MethodPost), PostSignUpController)
	Router.Register("/sign-out", CheckMethod(http.MethodPost, http.MethodGet, http.MethodDelete), MustBeAuthenticated, SignOutController)

	Router.Register("/oauth/{providerName}", CheckMethod(http.MethodGet), GetOAuthController)
	Router.Register("/oauth/{providerName}/redirect", CheckMethod(http.MethodGet), ResolveAuthentication, GetOAuthRedirectController)
}
