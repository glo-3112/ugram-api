package publications

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

// Description :
//		Method GET
//		This controller Get the [(page - 1) * size : page * size] first comments of a publication
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the {size} first comments. The first comment is the (size * (page - 1))th comment.
func GetPublicationCommentController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 30)
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if comments, err := repository.PublicationsRepository.GetExternalComments(s.User, publication.Id, page, size); err != nil {
			w.SendInternalError(err, "An unknown error occurred")
		} else if comments == nil {
			w.SendNotFound(nil)
		} else {
			w.SendOk(r.MakePagedResponse(page, size, comments))
		}
		return
	}
	w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
}

// Description :
//		Method POST
//		This controller add a comment to a publication
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the created comment
func PostPublicationCommentController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		comment := &models.CreateComment{}

		if err := r.UnmarshalJSON(comment); err != nil {
			w.SendBadRequest(nil, err.Error())
			return
		}

		if comment.Text == "" {
			w.SendBadRequest(nil, "You must provide a comment")
			return
		}

		if comm, err := repository.PublicationsRepository.AddComment(s.User, comment, publication); err != nil {
			w.SendInternalError(err, "An unknown error occurred")
		} else {
			repository.NotificationRepository.NotifyPublicationComment(publication.Author, publication.Id, s.User, comment.Text)
			w.SendOk(comm)
		}
	}
	w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
}

// Description :
//		Method GET
//		This controller Get the [(page - 1) * size : page * size] first replies of a publication comment
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the {size} last replies. The first reply is the (size * (page - 1))th comment reply.
func GetPublicationCommentReplyController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 30)
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			replies, err := repository.PublicationsRepository.GetExternalCommentReplies(s.User, publication.Id, bson.ObjectIdHex(r.Params["commentId"]), page, size)
			if err != nil {
				w.SendInternalError(err, "An unknown occurred")
				return
			} else {
				w.SendOk(r.MakePagedResponse(page, size, replies), "Ok")
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}

}

// Description :
//		Method POST
//		This controller add a reply to a comment
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the created reply
func PostPublicationCommentReplyController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {

			reply := &models.CreateReply{}

			if err := r.UnmarshalJSON(reply); err != nil {
				w.SendBadRequest(nil, err.Error())
				return
			}

			if reply.Text == "" {
				w.SendBadRequest(nil, "You must provide a comment")
				return
			}

			if ret, err := repository.PublicationsRepository.AddReply(s.User, bson.ObjectIdHex(r.Params["commentId"]), reply); err != nil {
				w.SendInternalError(err, "An unknown error occurred")
			} else {
				repository.NotificationRepository.NotifyPublicationCommentReply(publication.Author, publication.Id, s.User, reply.Text)
				w.SendOk(ret)
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method GET
//		This controller returns if the comment is liked or not
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns a boolean. True if the comment is liked, false otherwise
func GetPublicationCommentLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			if liked, err := repository.PublicationsRepository.IsCommentLiked(s.User, publication.Id, bson.ObjectIdHex(r.Params["commentId"])); err != nil && err != mgo.ErrNotFound {
				w.SendInternalError(err, "An unknown error occurred")
				return
			} else if err == mgo.ErrNotFound {
				w.SendNotFound(nil)
			} else {
				w.SendOk(liked)
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method POST
//		This controller likes a publication comment. If the comment is already liked, it does nothing
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the liked comment
func PostPublicationCommentLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			if err := repository.PublicationsRepository.LikeComment(s.User, publication.Id, bson.ObjectIdHex(r.Params["commentId"])); err != nil {
				w.SendInternalError(err, "An unknown error occurred")
			} else if err == mgo.ErrNotFound {
				w.SendNotFound(nil)
			} else if ret, err := repository.PublicationsRepository.GetExternalComment(s.User, bson.ObjectIdHex(r.Params["commentId"])); err != nil {
				w.SendInternalError(err, "An unknown error occurred")
			} else if ret == nil {
				w.SendNotFound(nil)
			} else {
				repository.NotificationRepository.NotifyPublicationCommentLike(publication.Author, publication.Id, s.User)
				w.SendOk(ret)
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method DELETE
//		This controller dislike a comment. If the comment is not liked, it does nothing.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the disliked comment.
func DeletePublicationCommentLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			if err := repository.PublicationsRepository.UnlikeComment(s.User, publication.Id, bson.ObjectIdHex(r.Params["commentId"])); err != nil && err != mgo.ErrNotFound {
				w.SendInternalError(err, "An unknown error occurred")
			} else if err == mgo.ErrNotFound {
				w.SendNotFound(nil)
			} else if ret, err := repository.PublicationsRepository.GetExternalComment(s.User, bson.ObjectIdHex(r.Params["commentId"])); err != nil {
				w.SendInternalError(err, "An unknown error occurred")
			} else if ret == nil {
				w.SendNotFound(nil)
			} else {
				w.SendOk(ret)
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method GET
//		This controller returns if the publication is liked or not
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns a boolean. True if the comment is liked, false otherwise
func GetPublicationCommentReplyLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			if bson.IsObjectIdHex(r.Params["replyId"]) {
				if liked, err := repository.PublicationsRepository.IsReplyLiked(s.User, publication.Id, bson.ObjectIdHex(r.Params["commentId"]), bson.ObjectIdHex(r.Params["replyId"])); err != nil && err != mgo.ErrNotFound {
					w.SendInternalError(err, "An unknown error occurred")
					return
				} else if err == mgo.ErrNotFound {
					w.SendNotFound(nil)
				} else {
					w.SendOk(liked)
				}
			} else {
				w.SendBadRequest(nil, "Wrong reply id")
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method POST
//		This controller likes a reply. If the reply is already liked, it does nothing
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the liked reply
func PostPublicationCommentReplyLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			cId := bson.ObjectIdHex(r.Params["commentId"])
			if bson.IsObjectIdHex(r.Params["replyId"]) {
				rId := bson.ObjectIdHex(r.Params["replyId"])
				if err := repository.PublicationsRepository.LikeReply(s.User, publication.Id, cId, rId); err != nil && err != mgo.ErrNotFound {
					w.SendInternalError(err, "An unknown error occurred")
				} else if err == mgo.ErrNotFound {
					w.SendNotFound(nil)
				} else if ret, err := repository.PublicationsRepository.GetExternalCommentReply(s.User, publication.Id, cId, rId); err != nil {
					w.SendInternalError(err, "An unknown error occurred")
				} else if ret == nil {
					w.SendNotFound(nil)
				} else {
					repository.NotificationRepository.NotifyPublicationCommentReplyLike(publication.Author, publication.Id, s.User)
					w.SendOk(ret)
				}
			} else {
				w.SendBadRequest(nil, "Wrong reply id")
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}

// Description :
//		Method DELETE
//		This controller dislike a reply. If the reply is not liked, it does nothing.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the disliked reply.
func DeletePublicationCommentReplyLikeController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); ok {
		if bson.IsObjectIdHex(r.Params["commentId"]) {
			cId := bson.ObjectIdHex(r.Params["commentId"])
			if bson.IsObjectIdHex(r.Params["replyId"]) {
				rId := bson.ObjectIdHex(r.Params["replyId"])
				if err := repository.PublicationsRepository.UnlikeReply(s.User, publication.Id, cId, rId); err != nil && err != mgo.ErrNotFound {
					w.SendInternalError(err, "An unknown error occurred")
				} else if err == mgo.ErrNotFound {
					w.SendNotFound(nil)
				} else if ret, err := repository.PublicationsRepository.GetExternalCommentReply(s.User, publication.Id, cId, rId); err != nil {
					w.SendInternalError(err, "An unknown error occurred")
				} else if ret == nil {
					w.SendNotFound(nil)
				} else {
					w.SendOk(ret)
				}
			} else {
				w.SendBadRequest(nil, "Wrong reply id")
			}
		} else {
			w.SendBadRequest(nil, "Wrong comment id")
		}
	} else {
		w.SendInternalError(fmt.Errorf("Publication is not set"), "An unknown error occurred")
	}
}
