package publications

import (
	"fmt"
	"net/http"
	"path"

	"github.com/globalsign/mgo/bson"

	"api/constants"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/filemanager"
)

// Description :
//		Method GET
//		This controllers returns all images of a publication
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the images ids of a publication
func GetPublicationImagesController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	w.SendOk(publication.Images)
}

// Description :
//		Method POST
//		This controllers Adds images to a publication by it's id.
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the edited publication.
func PostPublicationImagesController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	images := []string{}
	err := r.UnmarshalJSON(&images)
	if err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	for _, file := range images {
		fileid := bson.NewObjectId()
		err := filemanager.SaveBase64Picture(file, fileid.Hex())
		if err != nil {
			break
		}
		publication.Images = append(publication.Images, fileid)
	}

	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
	}

	full, err := repository.PublicationsRepository.ToFullPublication(s.User, publication)
	if err != nil || full == nil {
		w.SendInternalError(err, "An unknown error occurred")
	}

	w.SendOk(full)
}

// Description :
//		Method DELETE
//		This controller remove an image from a publication by their ids.
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the edited publication.
func DeletePublicationImagesController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	imagesToDelete := []bson.ObjectId{}

	err := r.UnmarshalJSON(&imagesToDelete)
	if err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	publication := r.SolvedParams["publication"].(*models.Publication)
	files := []string{}

removing:
	for i, v := range imagesToDelete {
		for index, image := range publication.Images {
			if image == v {
				files = append(files, image.Hex())

				// Removing id found
				publication.Images = append(publication.Images[0:index], publication.Images[index+1:]...)
				imagesToDelete = append(imagesToDelete[0:i], imagesToDelete[i+1:]...)
				goto removing
			}
		}
	}

	_ = filemanager.DeletePicturesByObjectId(imagesToDelete...)

	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	_ = filemanager.DeletePicture(files...)

	w.SendOk(publication)
}

// Description :
//		Method GET
//		This controllers serve an image. (Sends a image/png)
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the expected image.
func ServePublicationPictureController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	if r.Params["imageId"] == "" {
		w.Static().SendNotFound(fmt.Errorf("Image id empty"))
		return
	}

	imageId := bson.ObjectIdHex(r.Params["imageId"])
	index := -1
	for i, v := range publication.Images {
		if v == imageId {
			index = i
			break
		}
	}

	if index < 0 {
		w.Static().SendNotFound(fmt.Errorf("imageId %s not found in publication %s", imageId, publication.Id.Hex()))
		return
	}

	imageFormat := r.Params["imageFormat"]

	dir := http.Dir(constants.GetStaticDir())

	file, err := dir.Open(path.Join(imageFormat, imageId.Hex()+".png"))
	if err != nil {
		w.Static().SendNotFound(err)
		return
	}

	infos, err := file.Stat()
	if err != nil {
		w.Static().SendNotFound(err)
		return
	}

	if !infos.Mode().IsRegular() {
		w.Static().SendNotFound(fmt.Errorf("The file %s is not a regular file", path.Join(imageFormat, imageId.Hex()+".png")))
		return
	}

	w.Static().SendOk(file)
}


