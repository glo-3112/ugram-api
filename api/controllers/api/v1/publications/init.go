package publications

import (
	"net/http"

	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/publications")

	Router.Register("/", MultiMethod(Methods{
		http.MethodGet:  {GetLastPublicationsController},
		http.MethodPost: {MustBeAuthenticated, PostPublicationController},
	}))

	Router.Register("/search", MultiMethod(Methods{
		http.MethodGet: {GetPublicationsSearchController},
	}))
	Router.Register("/fromTags", MultiMethod(Methods{
		http.MethodGet: {GetPublicationFromTagsController},
	}))

	publicationId := router.NewRouter("/{publicationId}", ResolvePublication)
	publicationId.Register("/", MultiMethod(Methods{
		http.MethodGet:    {GetSinglePublicationController},
		http.MethodPatch:  {MustBeAuthenticated, ShouldOwnPublication, PatchSinglePublicationController},
		http.MethodDelete: {MustBeAuthenticated, ShouldOwnPublication, DeleteSinglePublicationController},
	}))

	publicationId.Register("/liked", MustBeAuthenticated, MultiMethod(Methods{
		http.MethodGet: {IsPublicationLikedController},
		http.MethodPost: {LikePublicationController},
		http.MethodDelete: {UnlikePublicationController},
	}))

	publicationId.Register("/images", MultiMethod(Methods{
		http.MethodGet:    {GetPublicationImagesController},
		http.MethodPost:   {MustBeAuthenticated, ShouldOwnPublication, PostPublicationImagesController},
		http.MethodDelete: {MustBeAuthenticated, ShouldOwnPublication, DeletePublicationImagesController},
	}))

	publicationId.Register("/images/{imageFormat:(?:original|medium|small)}/{imageId:.*}", MultiMethod(Methods{
		http.MethodGet: {ServePublicationPictureController},
	}))

	comments := router.NewRouter("/comments")

	comments.Register("/", MultiMethod(Methods{
		http.MethodGet: {GetPublicationCommentController},
		http.MethodPost: {MustBeAuthenticated, PostPublicationCommentController},
	}))

	comments.Register("/{commentId}/liked", MultiMethod(Methods{
		http.MethodGet: {GetPublicationCommentLikeController},
		http.MethodPost: {PostPublicationCommentLikeController},
		http.MethodDelete: {DeletePublicationCommentLikeController},
	}))

	comments.Register("/{commentId}/replies", MultiMethod(Methods{
		http.MethodGet: {GetPublicationCommentReplyController},
		http.MethodPost: {MustBeAuthenticated, PostPublicationCommentReplyController},
	}))

	comments.Register("/{commentId}/replies/{replyId}/liked", MultiMethod(Methods{
		http.MethodGet: {GetPublicationCommentReplyLikeController},
		http.MethodPost: {MustBeAuthenticated, PostPublicationCommentReplyLikeController},
		http.MethodDelete: {MustBeAuthenticated, DeletePublicationCommentReplyLikeController},
	}))

	//
	publicationId.UseRouter(comments)
	Router.UseRouter(publicationId)
}
