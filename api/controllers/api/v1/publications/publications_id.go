package publications

import (
	"fmt"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/filemanager"
)

// Description :
//		Method GET
//		This controllers Get a publication by it's id.
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the expected publication.
func GetSinglePublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	publication, err := repository.PublicationsRepository.ToFullPublication(s.User, r.SolvedParams["publication"].(*models.Publication))
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	} else if publication == nil {
		w.SendNotFound(nil, "Publication not found")
		return
	}

	w.SendOk(publication)
}

// Description :
//		Method PATCH
//		This controllers edit a publication by it's id.
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the edited publication.
func PatchSinglePublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	publication := r.SolvedParams["publication"].(*models.Publication)

	patch := models.Publication{}
	err := r.UnmarshalJSON(&patch)
	if err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if patch.MentionsId != nil {
		count, err := repository.UsersRepository.CountUserIds(patch.MentionsId)
		if err != nil && err != mgo.ErrNotFound {
			w.SendInternalError(err, "An unknown error occurred")
			return
		}
		if count != len(patch.MentionsId) {
			w.SendNotFound(nil, "Not all users found")
			return
		}
		publication.MentionsId = patch.MentionsId
	}
	if patch.Description != "" {
		publication.Description = patch.Description
	}
	if patch.Tags != nil {
		publication.Tags = patch.Tags
	}
	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	full, err := repository.PublicationsRepository.ToFullPublication(s.User, publication)
	if err != nil || full == nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(full)
}

// Description :
//		Method DELETE
//		This controllers Delete a publication by it's id.
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func DeleteSinglePublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	publication := r.SolvedParams["publication"].(*models.Publication)

	_ = filemanager.DeletePicturesByObjectId(publication.Images...)

	// Removing publication id in user
	publicationIds := s.User.PublicationIds
	s.User.PublicationIds = []bson.ObjectId{}
	for _, v := range publicationIds {
		if v != publication.Id {
			s.User.PublicationIds = append(s.User.PublicationIds, v)
		}
	}

	// Saving user modification
	err := repository.UsersRepository.Persist(s.User)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	// Deleting the publication
	err = repository.PublicationsRepository.DeleteByObjectId(publication.Id)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendNoContent()
}

// Description :
//		Method GET
//		This controller dislike a comment. If the comment is not liked, it does nothing.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns a boolean. True if the publication is liked, false otherwise.
func IsPublicationLikedController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); !ok {
		w.SendInternalError(fmt.Errorf("Publication not ok"), "An unknown error occurred")
		return
	} else {
		w.SendOk(publication.HasLikeFrom(s.User))
	}
}

// Description :
//		Method POST
//		This controller likes a publication. If the publication is liked, it does nothing.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the liked publication.
func LikePublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); !ok {
		w.SendInternalError(fmt.Errorf("Publication not ok"), "An unknown error occurred")
	} else {
		publication.LikeFrom(s.User)
		publication, err := repository.PublicationsRepository.Persist(publication)
		if err != nil || publication == nil {
			w.SendInternalError(err, "An unknown error occurred")
			return
		}
		if pub, err := repository.PublicationsRepository.ToFullPublication(s.User, publication); err != nil {
			w.SendInternalError(err)
		} else {
			repository.NotificationRepository.NotifyPublicationLike(publication.Author, publication.Id, s.User)
			w.SendOk(pub)
		}
	}
}

// Description :
//		Method DELETE
//		This controller dislikes a publication. If the publication is not liked, it does nothing.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the disliked publication.
func UnlikePublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	if publication, ok := r.SolvedParams["publication"].(*models.Publication); !ok {
		w.SendInternalError(fmt.Errorf("Publication not ok"), "An unknown error occurred")
		return
	} else {
		publication.UnlikeFrom(s.User)
		publication, err := repository.PublicationsRepository.Persist(publication)
		if err != nil || publication == nil {
			w.SendInternalError(err, "An unknown error occurred")
			return
		}
		if pub, err := repository.PublicationsRepository.ToFullPublication(s.User, publication); err != nil {
			w.SendInternalError(err)
		} else {
			w.SendOk(pub)
		}
	}
}
