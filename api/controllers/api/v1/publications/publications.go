package publications

import (
	"strings"
	"time"

	"github.com/globalsign/mgo/bson"

	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
	"api/tools/filemanager"
)

// Description :
//		Method GET
//		This controllers Get the x last publications for a user taking in count the privacy policy of the user.
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the {size} last publication. The first publication is the (size * (page - 1))th publication.
func GetLastPublicationsController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 20)

	publications, err := repository.PublicationsRepository.FindLastPublicationsPaged(s.User, page, size)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(r.MakePagedResponse(page, size, publications))
}

// Description :
//		Method POST
//		This controllers creates a publication for a user
//      Given parameters :
//          body : -> the publication to create
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the {size} last publication. The first publication is the (size * (page - 1))th publication.
func PostPublicationController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	pub := models.PublicationCreate{}

	err := r.UnmarshalJSON(&pub)
	if err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	publication, err := repository.PublicationsRepository.CreatePublication(s.User, pub)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	loadedImages := []bson.ObjectId{}

	for _, file := range pub.Images {
		fileid := bson.NewObjectId()
		err := filemanager.SaveBase64Picture(file, fileid.Hex())
		if err != nil {
			break
		}
		loadedImages = append(loadedImages, fileid)
	}
	publication.Images = loadedImages
	publication, err = repository.PublicationsRepository.Persist(publication)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	full, err := repository.PublicationsRepository.ToFullPublication(s.User, publication)
	if err != nil || full == nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendCreated(full)
}

// Description :
//		Method GET
//		This controller do a research in all publications by a querystring.
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
//          q: string -> The string to search in the publications
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the {size} best matched publications. The first publication is the (size * (page - 1))th publication.
func GetPublicationsSearchController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	query := &repository.ComplexPublicationQuery{
		Page:      0,
		Size:      0,
		Order:     -1,
		MaxImages: -1,
		MinImages: -1,
	}
	page, size := r.Paged(1, 20)
	query.Page = page
	query.Size = size

	if q, ok := r.Query()["q"]; ok {
		query.Desc = &q[0]
	} else {
		w.SendBadRequest(nil, "you must provide a query")
		return
	}
	if username, ok := r.Query()["uname"]; ok {
		query.UserName = &username[0]
	}
	if userId, ok := r.Query()["uid"]; ok {
		if !bson.IsObjectIdHex(userId[0]) {
			w.SendBadRequest(nil, "wrong user id format")
			return
		}
		uid := bson.ObjectIdHex(userId[0])
		query.UserId = &uid
	}
	if start, ok := r.Query()["s"]; ok {
		t, err := time.Parse(time.RFC3339, start[0])
		if err != nil {
			w.SendBadRequest(err, "wrong start time format")
			return
		}
		query.Start = &t
	}
	if ends, ok := r.Query()["e"]; ok {
		t, err := time.Parse(time.RFC3339, ends[0])
		if err != nil {
			w.SendBadRequest(err, "wrong start time format")
			return
		}
		query.Ends = &t
	}

	publications, err := repository.PublicationsRepository.SearchPublication(s.User, query)
	if err != nil {
		w.SendInternalError(err, "An error occurred while searching the publications")
		return
	} else if publications == nil {
		// It's ok to make an empty int array, it will be marshaled as "[]"
		w.SendOk(r.MakePagedResponse(page, size, []int{}))
		return
	}
	w.SendOk(r.MakePagedResponse(page, size, publications))
}

// Description :
//		Method GET
//		This controller search publications with tags.
//      Given parameters :
//          none
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the publications found
func GetPublicationFromTagsController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	tags := r.Req.URL.Query().Get("q")

	tagsArr := strings.Split(tags, ",")
	query := &repository.ComplexPublicationQuery{
		Order: -1,
		Page:  0,
		Size:  0,
		Tags:  tagsArr,
	}

	page, size := r.Paged(1, 20)
	query.Page = page
	query.Size = size

	pubs, err := repository.PublicationsRepository.FindByTags(s.User, query)

	if len(pubs) == 0 {
		w.SendNotFound(nil, "No publications found matching thoose parameters")
		return
	}
	if err != nil {
		w.SendInternalError(nil, "An error occurred while searching the publications")
		return
	} else if pubs == nil {
		w.SendOk(r.MakePagedResponse(page, size, []int{}))
		return
	}
	w.SendOk(r.MakePagedResponse(page, size, pubs))
}
