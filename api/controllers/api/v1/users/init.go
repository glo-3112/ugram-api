package users

import (
	"net/http"

	. "api/middlewares"
	"api/router"
)

var Router *router.Router

func init() {
	Router = router.NewRouter("/users", ResolveAuthentication)

	Router.Register("/", CheckMethod(http.MethodGet), GetAllUsersController)
	Router.Register("/search", CheckMethod(http.MethodGet), SearchUserController)

	Router.Register("/{userId}", MultiMethod(Methods{
		http.MethodGet:    {ResolveMe, ResolveUserParam, GetUserController},
		http.MethodPatch:  {MustBeAuthenticated, ShouldBeMe, PatchMe},
		http.MethodDelete: {MustBeAuthenticated, ShouldBeMe, DeleteMe},
	}))

	// Router.Register("/{userId}/mydatas", MustBeAuthenticated, ShouldBeMe, CheckMethod(http.MethodGet), GetCreateUserDataController)

	Router.Register("/{userId}/publications", MultiMethod(Methods{
		http.MethodGet: {ResolveAuthentication, ResolveMe, ResolveUserParam, GetUserPublicationsController},
	}))

	meRouter := router.NewRouter("/{userId}", MustBeAuthenticated, ShouldBeMe)

	meRouter.Register("/firstname", CheckMethod(http.MethodPatch), PatchMeFirstnameController)

	meRouter.Register("/lastname", CheckMethod(http.MethodPatch), PatchMeLastnameController)

	meRouter.Register("/mail", CheckMethod(http.MethodPatch), PatchMeMailController)

	meRouter.Register("/phone", CheckMethod(http.MethodPatch), PatchMePhoneController)

	meRouter.Register("/isActivated", CheckMethod(http.MethodGet), GetIfProfileIsActivatedController)

	meRouter.Register("/notifications", CheckMethod(http.MethodGet), GetProfileNotificationsController)
	meRouter.Register("/notifications/flush", CheckMethod(http.MethodGet), GetProfileNotificationsFlushController)
	meRouter.Register("/notifications/{notificationId}/read", CheckMethod(http.MethodGet), GetProfileNotificationsReadController)

	Router.Register("/{userId}/profilepicture/{imageFormat:(?:original|medium|small)}", MultiMethod(Methods{
		http.MethodGet: {ResolveAuthentication, ResolveUserParam, GetUserProfilePictureController},
	}))

	meRouter.Register("/security/authentication/tokens", MultiMethod(Methods{
		http.MethodGet: {GetMeSecurityAuthenticationTokens},
	}))

	meRouter.Register("/security/authentication/tokens/revoke",
		CheckMethod(http.MethodGet, http.MethodDelete), DeleteMeSecurityAuthenticationTokens)

	meRouter.Register("/security/authentication/providers", MultiMethod(Methods{
		http.MethodGet: {GetMeSecurityAuthenticationProviders},
	}))

	meRouter.Register("/security/authentication/providers/{providerName}/unlink",
		CheckMethod(http.MethodGet, http.MethodDelete), DeleteMeSecurityAuthenticationProvidersUnlink)

	meRouter.Register("/privacy/packaging/", CheckMethod(http.MethodGet), GetMePrivacyPackagedData)
	meRouter.Register("/privacy/packaging/create", CheckMethod(http.MethodGet), GetMePrivacyPackagedDataCreate)
	meRouter.Register("/privacy/packaging/file/{packagingId}", CheckMethod(http.MethodGet), GetMePrivacyPackagedDataFile)

	Router.UseRouter(meRouter)
}
