package users

import (
	"fmt"
	"net/http"
	"path"

	"api/constants"
	"api/generic/static"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

// Description :
//		Method GET
//		This controllers return all the registered users
// Parameters :
//      _ *req.Session :
//          The current user session (Can't be equal to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetAllUsersController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	page, size := r.Paged(1, 20)

	users, _ := repository.UsersRepository.PublicFindPaged(page, size)

	w.SendOk(r.MakePagedResponse(page, size, users))
}

func SearchUserController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	q := r.Req.URL.Query().Get("q")

	page, size := r.Paged(1, 10)

	users, err := repository.UsersRepository.PublicUserSearchPaged(q, page, size)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}
	w.SendOk(r.MakePagedResponse(page, size, users))
}

func GetUserController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {

	if r.SolvedParams["target_user"] == nil {
		w.SendNotFound(nil, "User", r.Params["userId"], "can't be found")
		return
	}
	targetUser := r.SolvedParams["target_user"].(*models.User)

	if s.User != nil {
		if s.User.Id == targetUser.Id {
			w.SendOk(targetUser)
			return
		}
	}
	if targetUser.Public {
		public := repository.UsersRepository.GetPublicData(targetUser)
		w.SendOk(public)
		return
	}
	public := repository.UsersRepository.GetPrivacyPublicData(targetUser)
	w.SendOk(public)
}

func GetUserProfilePictureController(_ *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	user := r.SolvedParams["user"].(*models.User)

	imageFormat := r.Params["imageFormat"]

	dir := http.Dir(constants.GetStaticDir())

	file, err := dir.Open(path.Join(imageFormat, user.ProfilePicture.Hex()+".png"))
	if err != nil {
		w.Static().SendNotFound(err)
		return
	}

	infos, err := file.Stat()
	if err != nil {
		w.Static().SendNotFound(err)
		return
	}

	if !infos.Mode().IsRegular() {
		err := fmt.Errorf("The file %s %s", path.Join(imageFormat, user.ProfilePicture.Hex()+".png"), "is not a regular file")
		w.Static().SendNotFound(err)
		return
	}

	static.Respond(w, file, http.StatusOK)
}
