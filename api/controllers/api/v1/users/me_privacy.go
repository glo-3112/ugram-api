package users

import (
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"

	crontasks "api/cron"
	"api/models"
	"api/req"
	. "api/tools"
	"api/tools/filemanager"
)

// Description :
//		Method GET
//		This controller return all the current and past tasks
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetMePrivacyPackagedData(s *req.Session, w *req.Writer, _ *req.Reader, _ CtrlFunc) {
	w.SendOk(s.User.RequestedUserData)
}

// Description :
//		Method GET
//		This controller create a task to package the user data
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetMePrivacyPackagedDataCreate(s *req.Session, w *req.Writer, _ *req.Reader, _ CtrlFunc) {
	nb := 0
	for _, v := range s.User.RequestedUserData {
		if ! v.Done {
			nb++
		}
	}
	if nb > 5 {
		w.SendForbidden(nil, "You can't request more than 5 data pack at a time. Please try again in few minutes")
		return
	}

	t, err := crontasks.CronTaskManager.AddPackUserDataCronJob(s.User)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(t, "The packaging of your data is scheduled for", t.Format(time.RFC850), ". Notice that the exact time may vary of few minutes")
}

// Description :
//		Method GET
//		This controller send a data packaged file
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetMePrivacyPackagedDataFile(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	static := w.Static()
	if packageId, ok := r.Params["packagingId"]; !ok || !bson.IsObjectIdHex(packageId) {
		static.SendNotFound(nil)
	} else {
		var targetPackage *models.BundleUserData = nil

		for _, r := range s.User.RequestedUserData {
			if r.Id == bson.ObjectIdHex(packageId) {
				targetPackage = r
			}
		}

		if targetPackage == nil || targetPackage.Expiry == nil || targetPackage.File == nil || targetPackage.Expiry.Unix() < time.Now().Unix() {
			static.SendNotFound(nil)
			return
		}

		file, err := filemanager.OpenZipFileArchive(s.User.Name, *targetPackage.File)
		if err != nil {
			static.SendInternalError(err)
			return
		}
		static := w.Static()
		static.SetFile(file, &req.FileInfos{Name: s.User.Name + " - " + targetPackage.RequestedAt.Format(time.RFC3339) + ".zip"})
		static.SendResponse(http.StatusOK, "Ok")
	}
}
