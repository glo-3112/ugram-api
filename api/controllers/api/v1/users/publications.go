package users

import (
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

func GetUserPublicationsController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 20)

	author := r.SolvedParams["user"].(*models.User)

	publications, err := repository.PublicationsRepository.FindUserPublicationsPaged(author.Id, page, size)
	if err != nil {
		w.SendInternalError(err, "Unknown error occured")
		return
	}

	fullPublications, err := repository.PublicationsRepository.ToFullPublications(s.User, publications)
	if err != nil {
		w.SendInternalError(err, "Unknown error occured")
		return
	}

	w.SendOk(r.MakePagedResponse(page, size, fullPublications))
}
