package users

import (
	"net/http"
	"time"

	"github.com/globalsign/mgo/bson"

	"api/constants/errors"
	crontasks "api/cron"
	"api/models"
	"api/repository"
	"api/req"
	. "api/tools"
	"api/tools/validators"
)

// Description :
//		Method PATCH
//		This controller updates the user information such as his profile picture, his name, mail ...
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMe(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	form := &models.PartialUser{}

	if err := r.UnmarshalJSON(form); err != nil {
		w.SendBadRequest(err, err.Error())
		return
	}

	me := s.User
	if err, message := validators.IsMeFormValid(me, form); err != nil || message != "" {
		w.SendBadRequest(err, message)
		return
	}

	if !me.Activated {
		me.Activated = me.Firstname != "" && me.Lastname != "" && me.PhoneNumber != nil
	}

	err := repository.UsersRepository.Persist(me)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(me)
}

// Description :
//		Method Delete
//		Delete definitively the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func DeleteMe(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	effective := crontasks.CronTaskManager.AddDeleteUserCronJob(s.User)

	w.SendOk(effective, "Deletion take in count. All access tokens has been revoked.",
		"Deletion will take fully effect around", effective.Format(time.Stamp), "+/- 5 min")
}

// Description :
//		Method Patch
//		Update the firstname of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeFirstnameController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {

	form := &models.UserFirstname{}

	if err := r.UnmarshalJSON(form); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if form.Firstname == "" {
		w.SendResponse(http.StatusBadRequest, errors.ErrFirstnameEmpty, nil)
		return
	}

	user := s.User
	user.Firstname = form.Firstname

	if !user.Activated {
		user.Activated = user.Firstname != "" && user.Lastname != "" && user.PhoneNumber != nil
	}

	if err := repository.UsersRepository.Persist(user); err != nil {
		w.SendResponse(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
		return
	}

	w.SendOk(user)
}

// Description :
//		Method Patch
//		Update the lastname of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeLastnameController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.UserLastname{}

	if err := r.UnmarshalJSON(&form); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if form.Lastname == "" {
		w.SendBadRequest(nil, errors.ErrLastnameEmpty)
		return
	}

	user := s.User
	user.Lastname = form.Lastname

	if !user.Activated {
		user.Activated = user.Firstname != "" && user.Lastname != "" && user.PhoneNumber != nil
	}

	if err := repository.UsersRepository.Persist(user); err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(user)
}

//		Method Patch
//		Update the email of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMeMailController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.UserMail{}

	if err := r.UnmarshalJSON(&form); err != nil {
		w.SendBadRequest(nil, err.Error())
		return
	}

	if form.Mail != s.User.Mail {
		if form.Mail == "" {
			w.SendBadRequest(nil, errors.ErrMailEmpty)
			return
		}

		if err := validators.IsMailValid(form.Mail); err != nil {
			w.SendBadRequest(nil, err.Error())
			return
		}

		if status, err := repository.UsersRepository.IsMailAvailable(form.Mail); err != nil {
			w.SendInternalError(err)
			return
		} else if !status {
			w.SendConflict(nil, errors.ErrMailAlreadyUsed)
			return
		}

		user := s.User
		user.Mail = form.Mail

		if !user.Activated {
			user.Activated = user.Firstname != "" && user.Lastname != "" && user.PhoneNumber != nil
		}

		if err := repository.UsersRepository.Persist(user); err != nil {
			w.SendInternalError(err)
			return
		}
	}

	w.SendOk(s.User)
}

// Description :
//		Method Patch
//		Update the user phone number of the authenticated user
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func PatchMePhoneController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {

	form := models.PhoneNumber{}
	if err := r.UnmarshalJSON(&form); err != nil {
		w.SendBadRequest(err, "Can't parse JSON body")
		return
	}

	user := s.User

	if form.Number != user.PhoneNumber.Number || form.ISOCode != user.PhoneNumber.ISOCode {
		if err := validators.IsPhoneValid(&form); err != nil {
			w.SendBadRequest(err, err.Error())
			return
		}

		if status, err := repository.UsersRepository.IsPhoneAvailable(form); err != nil {
			w.SendInternalError(err)
			return
		} else if status == false {
			w.SendResponse(http.StatusConflict, "This phone number is already taken", nil)
			return
		}

		user.PhoneNumber = &form

		if !user.Activated {
			user.Activated = user.Firstname != "" && user.Lastname != "" && user.PhoneNumber != nil
		}

		if err := repository.UsersRepository.Persist(user); err != nil {
			w.SendInternalError(err)
			return
		}
	}
	w.SendOk(s.User)
}

func GetIfProfileIsActivatedController(s *req.Session, w *req.Writer, _ *req.Reader, _ CtrlFunc) {
	w.SendOk(!s.User.Activated)
}

// Description :
//		Method Get
//		Returns all the user notifications paged
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetProfileNotificationsController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	page, size := r.Paged(1, 20)
	notifs, err := repository.NotificationRepository.GetNotificationsPaged(s.User.Id, page, size)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
	} else {
		if notifs == nil {
			w.SendOk([]interface{}{}, "Ok")
		} else {
			w.SendOk(r.MakePagedResponse(page, size, notifs))
		}
	}
}

// Description :
//		Method Get
//		Mark a notification as read
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetProfileNotificationsReadController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	if !bson.IsObjectIdHex(r.Params["notificationId"]) {
		w.SendBadRequest(nil, r.Params["notificationId"], "is not a valid id")
	} else {
		notif, err := repository.NotificationRepository.ReadNotification(s.User.Id, bson.ObjectIdHex(r.Params["notificationId"]))
		if err != nil {
			w.SendInternalError(err, "An unknown error occurred")
		} else if notif == nil {
			w.SendNotFound(nil)
		} else {
			w.SendOk(notif)
		}
	}
}


// Description :
//		Method Get
//      Remove all the notifications
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetProfileNotificationsFlushController(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	err := repository.NotificationRepository.FlushNotifications(s.User.Id)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
	} else {
		w.SendOk(r.MakePagedResponse(0, 0, []interface{}{}))
	}
}


