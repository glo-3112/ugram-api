package users

import (
	"api/repository"
	"api/req"
	. "api/tools"
)

// Description :
//		Method GET
//		This controller return's the user's active authentication tokens
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetMeSecurityAuthenticationTokens(s *req.Session, w *req.Writer, _ *req.Reader, _ CtrlFunc) {
	tokens, err := repository.TokensRepository.FindPublicByOwner(s.User.Id)

	if err != nil {
		w.SendInternalError(err, "An unknown error occurred while retrieving the tokens")
		return
	} else if tokens == nil {
		w.SendNoContent()
		return
	}

	w.SendOk(tokens)
}

// Description :
//		Method DELETE
//		This controller revoke the given user's active authentication tokens
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func DeleteMeSecurityAuthenticationTokens(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	tokens, _ := r.Query()["token"]

	if len(tokens) == 0 {
		err := r.UnmarshalJSON(&tokens)
		if err != nil {
			w.SendBadRequest(err, "Wrong body format")
			return
		}
	}

	err := repository.TokensRepository.DeleteByOwnerAndIds(s.User.Id, tokens)
	if err != nil {
		w.SendInternalError(err, "An error occurred while revoking the tokens")
		return
	}

	w.SendOk(nil)
}

// Description :
//		Method GET
//		This controller return's the user's active authentication providers
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func GetMeSecurityAuthenticationProviders(s *req.Session, w *req.Writer, _ *req.Reader, _ CtrlFunc) {
	providers := s.User.AuthProviders

	if len(providers) == 0 {
		w.SendNoContent()
	} else {
		w.SendOk(providers)
	}
}

// Description :
//		Method DELETE
//		This controller revoke the desired authentication provider only if there is an alternative
// Parameters :
//      _ *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
func DeleteMeSecurityAuthenticationProvidersUnlink(s *req.Session, w *req.Writer, r *req.Reader, _ CtrlFunc) {
	providerName := r.Params["providerName"]
	if providerName == "" {
		w.SendBadRequest(nil, "You must provide a provider to unlink")
		return
	}

	user := s.User

	if _, ok := user.AuthProviders[providerName]; !ok {
		w.SendNotFound(nil, "You're not linked to this provider")
		return
	}

	if len(user.AuthProviders) > 1 {
		delete(user.AuthProviders, providerName)
	} else {
		if user.Mail == "" || user.Name == "" || user.Hash == "" {
			w.SendNotAcceptable(nil, "You must have a sign-in alternative to unlink your last provider")
			return
		} else {
			delete(user.AuthProviders, providerName)
		}
	}

	_ = repository.UsersRepository.Persist(user)

	w.SendOk(nil)
}
