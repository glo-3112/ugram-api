package search

import (
	"api/router"
)

var Search *router.Router

func init() {
	Search = router.NewRouter("/search")

	Search.Register("/", GetSearchBulkController)

	Search.Register("/publications", GetSearchPublicationsController)

	Search.Register("/tags", GetSearchTagsController)

	Search.Register("/users", GetSearchUsersController)
}