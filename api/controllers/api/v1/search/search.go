package search

import (
	"net/url"
	"strings"

	"api/repository"
	"api/req"
	"api/tools"
)

type searchBulk struct {
	Users        req.Paged `json:"users"`
	Tags         req.Paged `json:"tags"`
	Publications req.Paged `json:"publications"`
}

// Description :
//		Method GET
//		This controllers search for a match of a query in users, publications and tags
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
//          q: string -> The search string
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the a bunch of results according to the {q} parameter
func GetSearchBulkController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 5)
	q := r.Query().Get("q")
	if q == "" {
		w.SendNotFound(nil, "You must provide a query to search")
		return
	}

	ret := &searchBulk{}

	users, err := repository.UsersRepository.PublicUserSearchPaged(q, page, size)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	userRoute, _ := url.Parse("/v1/search/users?q=" + q)
	ret.Users.From(page, size, len(users), userRoute)

	tags, err := repository.TagsRepository.GetContainingTags(strings.Join(strings.Split(q, " "), ""))
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	tagsRoute, _ := url.Parse("/v1/search/tags?q=" + q)
	ret.Tags.From(page, size, len(tags), tagsRoute)

	complexQuery := &repository.ComplexPublicationQuery{
		Desc:      &q,
		Page:      page,
		Size:      size,
	}
	publications, err := repository.PublicationsRepository.SearchPublication(s.User, complexQuery)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	publicationsRoute, _ := url.Parse("/v1/search/publications?q=" + q)
	ret.Publications.From(page, size, len(publications), publicationsRoute)

	ret.Users.Items = users
	ret.Tags.Items = tags
	ret.Publications.Items = publications

	w.SendOk(ret)
	return
}
