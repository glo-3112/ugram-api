package search

import (
	"strings"

	"api/repository"
	"api/req"
	"api/tools"
)

// Description :
//		Method GET
//		This controllers search for a match of a query in users, publications and tags
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
//          q: string -> The search string
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the a bunch of results according to the {q} parameter
func GetSearchTagsController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 5)
	q := r.Query().Get("q")
	if q == "" {
		w.SendNotFound(nil, "You must provide a query to search")
		return
	}

	tags, err := repository.TagsRepository.GetContainingTags(strings.Join(strings.Split(q, " "), ""))
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(r.MakePagedResponse(page, size, tags))
	return
}
