package search

import (
	"strings"

	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

// Description :
//		Method GET
//		This controllers search for a match of a query in users
//      Given parameters :
//          page: integer -> The number of the desired page
//          size: integer -> The size of the desired page
//          q: string -> The search string
// Parameters :
//      s *req.Session :
//          The current user session (Possibly set to nil)
//		w *req.Writer :
//			Interface used by the HTTP handler to construct the HTTP response
//		r *req.Reader :
//			HTTP request received by the API from the client
// Response :
//      returns the a bunch of results according to the {q} parameter
func GetSearchPublicationsController(s *req.Session, w *req.Writer, r *req.Reader, _ tools.CtrlFunc) {
	page, size := r.Paged(1, 5)
	q := r.Query().Get("q")
	t := r.Query().Get("t")
	if t == "" && q == "" {
		w.SendBadRequest(nil, "You must precise what to search")
		return
	}

	var ret []*models.FullPublication
	var err error

	complexQuery := &repository.ComplexPublicationQuery{
		Page: page,
		Size: size,
		Desc: &q,
		Tags: strings.Split(t, ","),
	}
	if q != "" {
		complexQuery.Tags = nil
		ret, err = repository.PublicationsRepository.SearchPublication(s.User, complexQuery)
	} else {
		complexQuery.Desc = nil
		ret, err = repository.PublicationsRepository.FindByTags(s.User, complexQuery)
	}
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	w.SendOk(r.MakePagedResponse(page, size, ret))
	return
}
