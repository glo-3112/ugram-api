package api

import (
	"api/controllers/api/v1/auth"
	"api/controllers/api/v1/publications"
	"api/controllers/api/v1/search"
	"api/controllers/api/v1/tags"
	"api/controllers/api/v1/users"
	"api/middlewares"
	"api/router"
)

var ApiRouter *router.Router

func init() {
	ApiRouter = router.NewRouter("/v1")

	ApiRouter.Use(middlewares.ResolveAuthentication, middlewares.RateLimiterMiddleware)

	ApiRouter.UseRouter(auth.Router)
	ApiRouter.UseRouter(publications.Router)
	ApiRouter.UseRouter(search.Search)
	ApiRouter.UseRouter(tags.Router)
	ApiRouter.UseRouter(users.Router)
}
