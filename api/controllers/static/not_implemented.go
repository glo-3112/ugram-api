package static

import (
	"api/req"
	. "api/tools"
)

func NotImplementedController(s *req.Session, w *req.Writer, r *req.Reader, next CtrlFunc) {
	w.SendNotImplemented(nil, "Not implemented yet")
}
