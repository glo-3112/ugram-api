package router

import (
	"net/http"
	"strings"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"api/req"
	"api/tools"
)

type route struct {
	path  string
	chain []tools.MdFunc
}

type Router struct {
	Basepath        string
	BaseMiddlewares []tools.MdFunc
	routes          []route
}

// Create a router with basepath as starting point
func NewRouter(basepath string, middleware ...tools.MdFunc) *Router {
	ret := &Router{Basepath: basepath, BaseMiddlewares: middleware}
	ret.routes = []route{}
	return ret
}

// Register the given middleware to a route
func (r *Router) Register(path string, middleware ...tools.MdFunc) {
	path = strings.TrimRight(path, "/")
	route := route{path: r.Basepath + path, chain: append(r.BaseMiddlewares, middleware...)}
	r.routes = append(r.routes, route)
}

func (r *Router) Use(middlewares ...tools.MdFunc) {
	r.BaseMiddlewares = append(r.BaseMiddlewares, middlewares...)
}

// Include the given router routes into this router
func (r *Router) UseRouter(router *Router) {
	for _, route := range router.routes {
		route.path = r.Basepath + route.path
		route.chain = append(r.BaseMiddlewares, route.chain...)
		r.routes = append(r.routes, route)
	}
}

// Apply all the routes to the go router.
func (r *Router) Apply(router *mux.Router) {
	for _, route := range r.routes {
		logrus.Println("applying route", route.path)

		handler := tools.MiddlewareResolver(route.chain)

		httpHandler := func(w http.ResponseWriter, r *http.Request) {
			wr := req.NewWriter(w, r)
			logrus.Infoln(r.Header)
			rd := &req.Reader{Req: r, SolvedParams: map[string]interface{}{}, Params: mux.Vars(r)}
			handler(req.NewSession(r), wr, rd)
		}

		router.HandleFunc(route.path, httpHandler)
	}
}
