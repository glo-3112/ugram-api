package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/sirupsen/logrus"

	"api/constants"
	"api/controllers/api"
	"api/controllers/static"
	"api/middlewares"
	"api/req"
	"api/router"
)

func main() {
	ugramServer := mux.NewRouter()
	logrus.StandardLogger().SetLevel(logrus.DebugLevel)

	mainRouter := router.NewRouter("")
	mainRouter.UseRouter(api.ApiRouter)
	mainRouter.UseRouter(static.StaticRouter)
	mainRouter.Apply(ugramServer)

	cors := middlewares.SetupCors()

	if ugramServer == nil {
		log.Fatal("Error while creating the server")
	}
	ugramServer.StrictSlash(false)
	ugramServer.NotFoundHandler = notFoundHandler{}

	server := cors.Handler(ugramServer)

	fmt.Println("Starting server on", os.Getenv(constants.ListenPort))

	if err := http.ListenAndServe(":"+os.Getenv(constants.ListenPort), server); err != nil {
		log.Fatal(err)
	}
}

type notFoundHandler struct {
}

func (h notFoundHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	writter := req.NewWriter(w, r)
	logrus.Debug("Not found:", r.URL)
	writter.SendNotFound(nil)
}
