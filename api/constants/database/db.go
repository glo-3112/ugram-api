package constants

const (
	Timeout       = 10
	MongoAddress  = "MONGODB_URI"
	MongoDatabase = "MONGODB_DATABASE"
	MongoUsername = "MONGODB_USERNAME"
	MongoPassword = "MONGODB_PASSWORD"

	TokensCollection       = "tokens"
	UsersCollection        = "users"
	PublicationsCollection = "publications"
	TagsCollection         = "tags"
	MigrationCollection    = "migration"

	PendingUserDeletion    = "pending_users_delete"
	PendingRequestUserData = "pending_users_data"
)
