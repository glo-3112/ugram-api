package constants

import (
	"os"

	"golang.org/x/oauth2"
	"golang.org/x/oauth2/gitlab"

	"api/constants"
)

const (
	GithubClientId     = "GITHUB_CLIENT_ID"
	GithubClientSecret = "GITHUB_CLIENT_SECRET"
	GithubRedirectURI  = "/v1/auth/oauth/github/redirect"
	GithubUserUri      = "https://api.github.com/user"

	GitlabClientId     = "GITLAB_CLIENT_ID"
	GitlabClientSecret = "GITLAB_CLIENT_SECRET"
	GitlabRedirectURI  = "/v1/auth/oauth/gitlab/redirect"
	GitlabUserUri      = "https://gitlab.com/api/v4/user"
)

func GetSupportedProviders() []string {
	return []string{
		"github",
		"gitlab",
	}
}

func GetGitHubConfig() oauth2.Config {
	return oauth2.Config{
		ClientID:     os.Getenv(GithubClientId),
		ClientSecret: os.Getenv(GithubClientSecret),
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://github.com/login/oauth/authorize",
			TokenURL:  "https://github.com/login/oauth/access_token",
			AuthStyle: oauth2.AuthStyleInHeader,
		},
		RedirectURL: os.Getenv(constants.ServerHost) + GithubRedirectURI,
		Scopes:      []string{"user"},
	}
}

func GetGitLabConfig() oauth2.Config {
	return oauth2.Config{
		ClientID:     os.Getenv(GitlabClientId),
		ClientSecret: os.Getenv(GitlabClientSecret),
		Endpoint: oauth2.Endpoint{
			AuthURL:  gitlab.Endpoint.AuthURL,
			TokenURL: gitlab.Endpoint.TokenURL,
		},
		RedirectURL: os.Getenv(constants.ServerHost) + GitlabRedirectURI,
		Scopes:      []string{"api", "read_user"},
	}
}
