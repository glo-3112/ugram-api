package constants

import (
	"time"
)

const (
	RateLimitAnonymousLimit = 1000
	RateLimitAuthedLimit    = 2000
	RateLimitDelay          = time.Minute * 15
)
