package constants

import (
	"time"
)

const (
	CanSecureCookies = "SECURE_COOKIES"

	// Expiration in days
	TokenExpiration     = 1
	TokenCookieName     = "token"
	TokenCookieSecure   = "yes"
	TokenCookieHttpOnly = true
	TokenCookiePath     = "/"
	TokenCookieExpiry   = time.Hour * 24

	// Expiration in days
	AuthenticationExpiration     = TokenExpiration
	AuthenticationCookieName     = "authenticated"
	AuthenticationCookieSecure   = "no"
	AuthenticationCookieHttpOnly = false
	AuthenticationCookiePath     = TokenCookiePath
	AuthenticationCookieExpiry   = TokenCookieExpiry

	OAuthRedirectCookieName     = "oauthRedirect"
	OAuthRedirectCookieSecure   = "yes"
	OAuthRedirectCookieHttpOnly = true
	OAuthRedirectCookiePath     = "/v1/auth/"
	OAuthRedirectCookieExpiry   = time.Hour * 24

	OAuthStateCookieName     = "oauthState"
	OAuthStateCookieSecure   = "yes"
	OAuthStateCookieHttpOnly = true
	OAuthStateCookiePath     = "/v1/auth/oauth"
	OAuthStateCookieExpiry   = time.Hour * 24
	OAuthStateStringLength   = 32
)
