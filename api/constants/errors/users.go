package errors

const (
	ErrFirstnameEmpty = "ERROR_FIRSTNAME_EMPTY"

	ErrLastnameEmpty = "ERROR_LASTNAME_EMPTY"

	ErrMailEmpty       = "ERROR_MAIL_EMPTY"
	ErrBadMailFormat   = "ERROR_MAIL_FORMAT"
	ErrMailAlreadyUsed = "ERROR_MAIL_ALREADY_TAKEN"

	ErrBadPhoneFormat      = "ERROR_PHONE_FORMAT"
	ErrPhoneTooShort       = "ERROR_PHONE_TOO_SHORT"
	ErrPhoneTooLong        = "ERROR_PHONE_TOO_LONG"
	ErrPhoneBadCountryCode = "ERROR_PHONE_COUNTRY_CODE"
	ErrPhoneInvalidLength  = "ERROR_PHONE_INVALID_LENGTH"
)
