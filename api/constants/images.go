package constants

import (
	"os"
)

type Size struct {
	Width  uint64
	Height uint64
}

var staticDir = ""

const (
	ImageSmallWidth   = 400
	ImageSmallHeight  = 400
	ImageMediumWidth  = 800
	ImageMediumHeight = 800
	ImageLargeWidth   = 1200
	ImageLargeHeight  = 1200

	DefaultProfilePicture = "000000000000000000000001"
)

func GetAllowedImageMimeTypes() []string {
	return []string{
		"image/jpeg",
		"image/png",
		"image/tiff",
		"image/gif",
		"image/bmp",
		"image/webp",
	}
}

func GetStaticDir() string {
	return staticDir
}

func init() {
	staticDir = os.Getenv(StaticDirectory)
}
