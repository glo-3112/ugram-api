package constants

const (
	ListenPort      = "VITURAL_PORT"
	StaticDirectory = "STATIC_DIRECTORY"
	ServerHost      = "SERVER_HOST"
	DevEnv          = "DEV_ENVIRONMENT"
)
