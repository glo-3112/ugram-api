package repository

import (
	"fmt"
	"log"
	"strings"
	"time"

	"golang.org/x/crypto/bcrypt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	"api/constants"
	dbconst "api/constants/database"
	userConst "api/constants/users"
	"api/form/api/v1"
	"api/models"
)

type dbUser struct {
	collection *mgo.Collection
}

var UsersRepository dbUser

func (db *dbUser) connect() {
	db.collection = Db.C(dbconst.UsersCollection)

	unique := mgo.Index{
		Key:    []string{"_id", "name", "mail", "phone_number"},
		Unique: true,
	}

	err := db.collection.EnsureIndex(unique)
	if err != nil {
		log.Fatal(err)
	}
}

func (db *dbUser) Find() ([]*models.User, error) {
	users := []*models.User{}

	err := db.collection.Find(bson.M{}).All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (db *dbUser) FindById(id string) (*models.User, error) {
	return db.FindByObjectId(bson.ObjectIdHex(id))
}
func (db *dbUser) FindByObjectIds(ids []bson.ObjectId) ([]*models.User, error) {
	ret := []*models.User{}

	err := db.collection.Find(bson.M{"_id": bson.M{
		"$in": ids,
	}}).All(&ret)

	if err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return ret, nil
}

func (db *dbUser) FindByObjectId(id bson.ObjectId) (*models.User, error) {
	user := &models.User{}

	err := db.collection.Find(bson.M{"_id": id}).One(user)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) FindPublicById(id bson.ObjectId) (*models.UserPublic, error) {
	user := &models.UserPublic{}

	err := db.collection.Find(bson.M{"_id": id}).One(user)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) FindPublicByIds(ids []bson.ObjectId) ([]*models.UserPublic, error) {
	user := []*models.UserPublic{}

	err := db.collection.Find(bson.M{"_id": bson.M{"$in": ids}}).All(&user)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) FindLightPublicById(id bson.ObjectId) (*models.LightUserPublic, error) {
	ret := &models.LightUserPublic{}

	err := db.collection.Find(bson.M{"_id": id}).One(ret)

	if err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return ret, nil
}

func (db *dbUser) FindLightPublicByIds(ids []bson.ObjectId) ([]*models.LightUserPublic, error) {
	ret := []*models.LightUserPublic{}

	err := db.collection.Find(bson.M{"_id": bson.M{
		"$in": ids,
	}}).All(&ret)

	if err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}
	return ret, nil
}

func (db *dbUser) FindByIdOrUsername(query string) (*models.User, error) {
	user := &models.User{}

	err := db.collection.Find(bson.M{
		"$or": []bson.M{
			{
				"_id": bson.ObjectIdHex(query),
			},
			{
				"name": query,
			},
		},
	}).One(user)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return user, nil
}

func (db *dbUser) FindByName(name string) (*models.User, error) {
	user := &models.User{}

	err := db.collection.Find(bson.M{"name": name}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) FindByMail(mail string) (*models.User, error) {
	user := &models.User{}

	err := db.collection.Find(bson.M{"mail": mail}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) IsMailAvailable(mail string) (bool, error) {
	nb, err := db.collection.Find(bson.M{
		"mail": bson.M{
			"$eq": mail,
		},
	}).Count()

	if err != nil {
		return false, err
	}
	return nb == 0, nil
}

func (db *dbUser) IsPhoneAvailable(phone models.PhoneNumber) (bool, error) {
	nb, err := db.collection.Find(bson.M{
		"$and": []bson.M{
			{
				"phone_number.number": bson.M{
					"$eq": phone.Number,
				},
			},
			{
				"phone_number.iso_code": bson.M{
					"$eq": phone.ISOCode,
				},
			},
		},
	}).Count()

	if err != nil {
		return false, err
	} else {
		return nb == 0, nil
	}
}

func (db *dbUser) IsUsernameOrMailAvailable(username string, mail string) (bool, error) {
	nb, err := db.collection.Find(bson.M{
		"$or": []bson.M{
			{"name": username},
			{"mail": mail},
		},
	}).Count()

	if err != nil {
		return false, err
	}
	if nb == 0 {
		return true, nil
	}
	return false, nil
}

func (db *dbUser) FindByUsernameOrMail(username string, mail string) (*models.User, error) {
	user := &models.User{}

	err := db.collection.Find(bson.M{"$or": []bson.M{{"name": username}, {"mail": mail}}}).One(user)
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) FindByLogin(login string) (*models.User, error) {
	return db.FindByUsernameOrMail(login, login)
}

func (db *dbUser) FindByLoginAndPassword(login string, password string) (*models.User, error) {
	user, err := db.FindByLogin(login)

	if err != nil {
		return nil, err
	}

	err = bcrypt.CompareHashAndPassword([]byte(user.Hash), []byte(password))
	if err != nil {
		return nil, err
	}
	return user, nil
}

func (db *dbUser) CreateUserNoHashPassword(user form.SignUp) (*models.User, error) {

	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.DefaultCost)
	if err != nil {
		return nil, err
	}

	insert := &models.User{
		Id:             bson.NewObjectId(),
		Roles:          []string{userConst.DefaultUserRole},
		Hash:           string(hash),
		Name:           user.Username,
		Firstname:      user.Firstname,
		Lastname:       user.Lastname,
		Mail:           user.Mail,
		PhoneNumber:    user.PhoneNumber,
		ProfilePicture: bson.ObjectIdHex(constants.DefaultProfilePicture),
		AuthProviders:  map[string]models.AuthProviderInformations{},
		Activated:      true,

		Public: true,

		FriendsIds: []bson.ObjectId{},

		PublicationIds: []bson.ObjectId{},

		LastSignIn:       time.Unix(0, 0),
		RegistrationDate: time.Now(),
	}

	err = db.collection.Insert(insert)
	if err != nil {
		return nil, err
	}

	return insert, nil
}

func (db *dbUser) GetUserByOauthProvider(provider string, oauthUser *models.OauthUser) (*models.User, error) {
	ret := &models.User{}

	q := bson.M{
		"auth_providers." + provider + ".foreign_id": bson.M{
			"$eq": fmt.Sprint(oauthUser.Id),
		},
	}
	err := db.collection.Find(q).One(&ret)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *dbUser) CreateUserByOauthProvider(provider string, oauthUser *models.OauthUser) (*models.User, error) {
	names := strings.Split(oauthUser.Name, " ")
	if len(names) == 0 {
		names = make([]string, 2)
	}

	ret := &models.User{
		Id:          bson.NewObjectId(),
		Roles:       []string{userConst.DefaultUserRole},
		Name:        oauthUser.UserName,
		Hash:        "",
		Firstname:   names[0],
		Lastname:    strings.Join(names[1:], " "),
		Mail:        oauthUser.Mail,
		PhoneNumber: &models.PhoneNumber{},
		AuthProviders: map[string]models.AuthProviderInformations{
			provider: {
				ForeignId:    fmt.Sprint(oauthUser.Id),
				ForeignName:  oauthUser.Name,
				ForeignMail:  oauthUser.Mail,
				ForeignToken: oauthUser.Token,
				LinkedAt:     time.Now(),
			},
		},
		Activated:        false,
		Public:           true,
		FriendsIds:       nil,
		Friends:          nil,
		ProfilePicture:   bson.ObjectIdHex(constants.DefaultProfilePicture),
		PublicationIds:   nil,
		Publications:     nil,
		RegistrationDate: time.Now(),
		LastSignIn:       time.Unix(0, 0),
	}

	err := db.collection.Insert(ret)
	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *dbUser) UpdateByID(id bson.ObjectId, user *models.User) error {
	return db.collection.Update(bson.M{"_id": id}, user)
}

func (db *dbUser) UpdateLastSignIn(user *models.User) error {
	user.LastSignIn = time.Now()
	return db.collection.Update(bson.M{"_id": user.Id}, user)
}

func (db *dbUser) Persist(user *models.User) error {
	return db.UpdateByID(user.Id, user)
}

func (db *dbUser) ResolveNested(level int, user *models.User) *models.User {
	return user
}

func (db *dbUser) FindPaged(page int, size int) ([]*models.User, error) {
	users := []*models.User{}

	err := db.collection.Find(bson.M{}).Skip((page - 1) * size).Limit(page).All(users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (db *dbUser) PublicFindPaged(page int, size int) ([]*models.UserPublic, error) {
	users := []*models.UserPublic{}

	err := db.collection.Find(bson.M{
	}).Select(bson.M{
		"profile_picture":   1,
		"name":              1,
		"firstname":         1,
		"lastname":          1,
		"registration_date": 1,
	}).Skip((page - 1) * size).Limit(size).All(&users)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (db *dbUser) PublicUserSearchPaged(query string, page int, size int) ([]*models.UserPublic, error) {

	startBy := "^" + query
	mgoQuery := []bson.M{
		{
			"$match": bson.M{
				"$or": []bson.M{
					{
						"name": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
					{
						"firstname": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
					{
						"lastname": bson.M{
							"$regex":   query,
							"$options": "ix",
						},
					},
				},
			},
		},
		{
			"$set": bson.M{
				"common": bson.M{
					"$cond": []interface{}{
						bson.M{
							"$regexMatch": bson.M{
								"input":   "$name",
								"regex":   "des",
								"options": "ix",
							},
						}, "$name", bson.M{
							"$cond": []interface{}{
								bson.M{
									"$regexMatch": bson.M{
										"input":   "$firstname",
										"regex":   "des",
										"options": "ix",
									},
								}, "$firstname", "$lastname"},
						},
					},
				},
				"matchType": bson.M{
					"$cond": []interface{}{
						bson.M{
							"$regexMatch": bson.M{
								"input":   "$name",
								"regex":   startBy,
								"options": "ix",
							},
						}, 6, bson.M{
							"$cond": []interface{}{
								bson.M{
									"$regexMatch": bson.M{
										"input":   "$firstname",
										"regex":   startBy,
										"options": "ix",
									},
								}, 5, bson.M{
									"$cond": []interface{}{
										bson.M{
											"$regexMatch": bson.M{
												"input":   "$lastname",
												"regex":   startBy,
												"options": "ix",
											},
										}, 4, bson.M{
											"$cond": []interface{}{
												bson.M{
													"$regexMatch": bson.M{
														"input":   "$name",
														"regex":   query,
														"options": "ix",
													},
												}, 3, bson.M{
													"$cond": []interface{}{
														bson.M{
															"$regexMatch": bson.M{
																"input":   "$firstname",
																"regex":   query,
																"options": "ix",
															},
														}, 2, bson.M{
															"$cond": []interface{}{
																bson.M{
																	"$regexMatch": bson.M{
																		"input":   "$lastname",
																		"regex":   query,
																		"options": "ix",
																	},
																}, 1, 0},
														},
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		}, {
			"$set": bson.M{
				"score": bson.M{
					"$subtract": []interface{}{
						100,
						bson.M{
							"$multiply": []interface{}{
								bson.M{
									"$divide": []interface{}{
										bson.M{
											"$subtract": []interface{}{
												bson.M{
													"$strLenCP": "$common",
												},
												len(query),
											},
										},
										bson.M{
											"$strLenCP": "$common",
										},
									},
								},
								100,
							},
						},
					},
				},
			},
		}, {
			"$sort": bson.M{
				"matchType": -1,
				"score":     -1,
			},
		}, {
			"$project": bson.M{
				"matchType":         0,
				"roles":             0,
				"hash":              0,
				"mail":              0,
				"phone_number":      0,
				"last_sign_in":      0,
				"common":            0,
				"registration_date": 0,
			},
		}, {
			"$skip": (page - 1) * size,
		}, {
			"$limit": size,
		},
	}
	users := []*models.UserPublic{}

	err := db.collection.Pipe(mgoQuery).All(&users)

	if err != nil {
		return nil, err
	}
	return users, nil
}

func (db *dbUser) PublicFindIds(ids []bson.ObjectId) ([]*models.UserPublic, error) {
	users := []*models.UserPublic{}

	err := db.collection.Find(bson.M{
		"_id": bson.M{
			"$in": ids,
		},
	}).All(&users)

	if err != nil {
		return nil, err
	}
	return users, nil
}

func (db *dbUser) CountUserIds(ids []bson.ObjectId) (int, error) {

	n, err := db.collection.Find(bson.M{
		"_id": bson.M{
			"$in": ids,
		},
	}).Count()

	if err != nil {
		return 0, err
	}

	return n, nil
}

func (db *dbUser) GetPublicData(user *models.User) *models.UserPublic {
	return &models.UserPublic{
		Id:               user.Id,
		Name:             user.Name,
		Firstname:        user.Firstname,
		Lastname:         user.Lastname,
		ProfilePicture:   &user.ProfilePicture,
		FriendsIds:       user.FriendsIds,
		PublicationIds:   user.PublicationIds,
		RegistrationDate: user.RegistrationDate,
	}
}

func (db *dbUser) GetPrivacyPublicData(user *models.User) *models.UserPublic {
	return &models.UserPublic{
		Id:               user.Id,
		Name:             user.Name,
		Firstname:        user.Firstname,
		Lastname:         user.Lastname,
		ProfilePicture:   &user.ProfilePicture,
		RegistrationDate: user.RegistrationDate,
	}
}

func (db *dbUser) Remove(id bson.ObjectId) error {

	err := db.collection.RemoveId(id)
	if err == mgo.ErrNotFound {
		return nil
	} else if err != nil {
		return err
	}

	return nil
}

func (db *dbUser) UpdateRateLimit(user *models.User) {
	_ = db.collection.Update(bson.M{"_id": user.Id}, bson.M{
		"$set": bson.M{
			"rate_limit": user.RateLimit,
		},
	})
}

func (db *dbUser) UpdateRequestUserData(user *models.User, task *models.BundleUserData) error {
	for i, bundle := range user.RequestedUserData {
		if bundle.Id == task.Id {
			user.RequestedUserData[i] = task
			break
		}
	}

	return db.Persist(user)
}

func (db *dbUser) FindExpiredBundleUsersData() ([]*models.User, []map[int]*models.BundleUserData, error) {
	ret := make([]*models.User, 0)
	err := db.collection.Find(bson.M{
		"bundle_user_data": bson.M{
			"$elemMatch": bson.M{
				"expiry": bson.M{
					"$lte": time.Now(),
				},
			},
		},
	}).All(&ret)
	if err == mgo.ErrNotFound {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}

	bundleList := make([]map[int]*models.BundleUserData, len(ret))
	for i, user := range ret {
		bundles := map[int]*models.BundleUserData{}

		for idx, bundle := range user.RequestedUserData {
			if bundle.Expiry != nil && bundle.Expiry.Unix() <= time.Now().Unix() {
				bundles[idx] = bundle
			}
		}
		bundleList[i] = bundles
	}

	return ret, bundleList, nil
}

func (db *dbUser) FindPendingBundleUsersData() ([]*models.User, [][]*models.BundleUserData, error) {
	ret := make([]*models.User, 0)
	err := db.collection.Find(bson.M{
		"bundle_user_data": bson.M{
			"$elemMatch": bson.M{
				"expiry": nil,
			},
		},
	}).All(&ret)
	if err == mgo.ErrNotFound {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}

	bundleList := make([][]*models.BundleUserData, len(ret))
	for i, user := range ret {
		bundles := make([]*models.BundleUserData, 0)

		for _, bundle := range user.RequestedUserData {
			if bundle.Expiry == nil {
				bundles = append(bundles, bundle)
			}
		}
		bundleList[i] = bundles
	}

	return ret, bundleList, nil
}
