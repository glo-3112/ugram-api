package repository

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	dbconsts "api/constants/database"
	"api/models"
)

type dbRequestUserData struct {
	collection *mgo.Collection
}

var PendingRequestUserDataRepository = dbRequestUserData{}

func (db *dbRequestUserData) connect() {
	db.collection = Db.C(dbconsts.PendingRequestUserData)
}

func (db *dbRequestUserData) Add(task *models.BundleUserData) error {
	return db.collection.Insert(task)
}

func (db *dbRequestUserData) Count() (int, error) {
	c, err := UsersRepository.collection.Find(bson.M{
		"bundle_user_data.done": false,
	}).Count()
	if err == mgo.ErrNotFound {
		return 0, nil
	} else if err != nil {
		return 0, err
	}
	return c, nil
}

func (db *dbRequestUserData) PopOne() (*models.BundleUserData, *models.User, error) {
	ret := &models.BundleUserData{}

	err := db.collection.Find(bson.M{}).One(ret)
	if err == mgo.ErrNotFound {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}
	err = db.collection.RemoveId(ret.Id)
	if err == mgo.ErrNotFound {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}

	user, err := UsersRepository.FindByObjectId(ret.UserId)
	if err == mgo.ErrNotFound {
		return nil, nil, nil
	} else if err != nil {
		return nil, nil, err
	}

	return ret, user, nil
}

func (db *dbRequestUserData) UpdateBundleUserData(user *models.User, task *models.BundleUserData) {
	_ = UsersRepository.collection.Update(bson.M{
		"_id":                  user.Id,
		"bundle_user_data._id": task.Id,
	}, bson.M{
		"$set": bson.M{
			"bundle_user_data.$": task,
		},
	})
}
