package repository

import (
	"encoding/json"
	"fmt"
	"log"
	"regexp"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	dbconsts "api/constants/database"
	"api/models"
)

type dbPublications struct {
	c *mgo.Collection
}

type ComplexPublicationQuery struct {
	Desc      *string
	UserId    *bson.ObjectId
	UserName  *string
	Start     *time.Time
	Ends      *time.Time
	Order     int
	Tags      []string
	MinImages int64
	MaxImages int64

	Page int
	Size int
}

var PublicationsRepository dbPublications

func (db *dbPublications) Connect() {
	db.c = Db.C(dbconsts.PublicationsCollection)

	description := mgo.Index{
		Key: []string{"$text:description"},
		Weights: map[string]int{
			"description": 1,
		},
	}

	err := db.c.EnsureIndex(description)
	if err != nil {
		log.Panic(err)
	}
}

func (db *dbPublications) CreatePublication(user *models.User, form models.PublicationCreate) (*models.Publication, error) {
	insert := &models.Publication{
		Id:          bson.NewObjectId(),
		Author:      user.Id,
		Description: form.Description,
		Tags:        form.Tags,
		MentionsId:  form.MentionsId,
		Images:      []bson.ObjectId{},
		CreatedAt:   time.Now(),
		UpdatedAt:   time.Now(),
	}

	err := db.c.Insert(insert)
	if err != nil {
		return nil, err
	}
	user.PublicationIds = append(user.PublicationIds, insert.Id)

	if err := UsersRepository.Persist(user); err != nil {
		return nil, err
	}

	if err := TagsRepository.UpdateScore(insert.Tags); err != nil {
		return nil, err
	}

	return insert, nil
}

func (db *dbPublications) FindByObjectId(id bson.ObjectId) (*models.Publication, error) {
	publication := &models.Publication{}

	err := db.c.Find(bson.M{
		"_id": id,
	}).One(publication)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return publication, nil
}

func (db *dbPublications) FindById(id string) (*models.Publication, error) {
	if !bson.IsObjectIdHex(id) {
		return nil, fmt.Errorf("bad object id")
	}
	return db.FindByObjectId(bson.ObjectIdHex(id))
}

func (db *dbPublications) FindByAuthorAndId(authorId string, id string) (*models.Publication, error) {
	publication := &models.Publication{}

	err := db.c.Find(bson.M{
		"_id":    bson.ObjectIdHex(id),
		"author": bson.ObjectIdHex(authorId),
	}).One(publication)
	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return publication, nil
}

func (db *dbPublications) FindUserPublications(userId bson.ObjectId) ([]models.Publication, error) {
	publications := []models.Publication{}

	err := db.c.Find(bson.M{
		"author": userId,
	}).Sort("-created_at").All(&publications)
	if err != nil {
		return nil, err
	}
	return publications, nil
}

func (db *dbPublications) FindUserPublicationsPaged(userId bson.ObjectId, page, size int) ([]*models.Publication, error) {
	publications := []*models.Publication{}

	err := db.c.Find(bson.M{
		"author": userId,
	}).Sort("-created_at").Skip((page - 1) * size).Limit(size).All(&publications)
	if err != nil {
		return nil, err
	}

	return publications, nil
}

func (db *dbPublications) Persist(publication *models.Publication) (*models.Publication, error) {

	publication.UpdatedAt = time.Now()
	err := db.c.Update(bson.M{
		"_id": publication.Id,
	}, publication)
	if err != nil {
		return nil, err
	}

	return publication, nil
}

func (db *dbPublications) ToFullPublications(user *models.User, publications []*models.Publication) ([]*models.FullPublication, error) {
	ret := make([]*models.FullPublication, len(publications))

	for i, v := range publications {
		fullPub, err := db.ToFullPublication(user, v)
		if err != nil {
			return nil, err
		}
		ret[i] = fullPub
	}

	return ret, nil
}

func (db *dbPublications) ToFullPublication(user *models.User, publication *models.Publication) (*models.FullPublication, error) {
	publicationAuthor, err := UsersRepository.FindByObjectId(publication.Author)

	if err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	publicationMentions, err := UsersRepository.FindByObjectIds(publication.MentionsId)
	mentions := []*models.UserPublic{}
	for _, pm := range publicationMentions {
		mentions = append(mentions, UsersRepository.GetPublicData(pm))
	}

	comments, _ := db.GetExternalComments(user, publication.Id, 1, 10)

	ret := &models.FullPublication{
		Id:          publication.Id,
		Author:      UsersRepository.GetPublicData(publicationAuthor),
		Description: publication.Description,
		Tags:        publication.Tags,
		Mentions:    mentions,
		NbLikes:     len(publication.LikesId),
		Liked:       publication.HasLikeFrom(user),
		Images:      db.MakePublicationImageLink(publication.Id, publication.Images),
		Comments:    comments,
		CreatedAt:   publication.CreatedAt,
		UpdatedAt:   publication.UpdatedAt,
	}

	return ret, nil
}

func (db *dbPublications) MakePublicationImageLink(publicationId bson.ObjectId, images []bson.ObjectId) []models.PublicImage {
	ret := []models.PublicImage{}

	for _, img := range images {
		ret = append(ret, models.PublicImage{
			Id:      img,
			MdThumb: "/publications/" + publicationId.Hex() + "/images/medium/" + img.Hex(),
			SmThumb: "/publications/" + publicationId.Hex() + "/images/small/" + img.Hex(),
			Normal:  "/publications/" + publicationId.Hex() + "/images/original/" + img.Hex(),
		})
	}

	return ret
}

func (db *dbPublications) FindLastPublicationsPaged(user *models.User, page int, size int) ([]*models.FullPublication, error) {
	ret := make([]*models.FullPublication, 0)

	orMatch := []bson.M{
		{
			"author.public": bson.M{
				"$eq": true,
			},
		},
	}

	addedFields := bson.M{
		"n_images": bson.M{
			"$size": "$images",
		},
		"n_mentions": bson.M{
			"$size": "$mentions_id",
		},
		"nb_likes": bson.M{
			"$size": "$likes",
		},
		"liked": false,
	}

	if user != nil {
		orMatch = append(orMatch, bson.M{
			"author._id": bson.M{
				"$eq": user.Id,
			},
		}, bson.M{
			"author.friends_ids": bson.M{
				"$elemMatch": bson.M{
					"$eq": user.Id,
				},
			},
		})
		addedFields["liked"] = bson.M{
			"likes": bson.M{
				"$cond": bson.M{
					"if": bson.M{
						"$gte": []interface{}{
							bson.M{
								"$indexOfArray": []interface{}{
									"$likes", user.Id,
								},
							},
							0,
						},
					},
					"then": true,
					"else": false,
				},
			},
		}
	}

	err := db.c.Pipe([]bson.M{
		{
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		},
		{
			"$unwind": "$author",
		},
		{
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "mentions_id",
				"foreignField": "_id",
				"as":           "mentions",
			},
		},
		{
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		},
		{
			"$match": bson.M{
				"$or": orMatch,
			},
		},
		{
			"$sort": bson.M{
				"created_at": -1,
			},
		},
		{
			"$skip": (page - 1) * size,
		},
		{
			"$limit": size,
		},
		{
			"$set": addedFields,
		},
	}).All(&ret)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	for _, pub := range ret {
		comments, _ := db.GetExternalComments(user, pub.Id, 1, 10)

		pub.Images = db.MakePublicationImageLink(pub.Id, pub.ImagesId)
		pub.Comments = comments
		pub.Liked = pub.HasLikeFrom(user)
	}

	return ret, nil
}

func (db *dbPublications) SearchPublication(user *models.User, complexQuery *ComplexPublicationQuery) ([]*models.FullPublication, error) {
	ret := make([]*models.FullPublication, 0)

	if complexQuery.Order == 0 {
		complexQuery.Order = -1
	}

	friendIds := make([]bson.ObjectId, 0)
	if user != nil {
		friendIds = user.FriendsIds
	}

	specialMatchRules := make([]bson.M, 0)
	if complexQuery.Start != nil || complexQuery.Ends != nil {
		rule := bson.M{}
		if complexQuery.Start != nil {
			rule["$gt"] = *complexQuery.Start
		}
		if complexQuery.Ends != nil {
			rule["$lt"] = *complexQuery.Ends
		}

		specialMatchRules = append(specialMatchRules, bson.M{
			"created_at": rule,
		})
	}

	if complexQuery.UserId != nil {
		specialMatchRules = append(specialMatchRules, bson.M{
			"author._id": bson.M{
				"$eq": *complexQuery.UserId,
			},
		})
	} else if complexQuery.UserName != nil {
		specialMatchRules = append(specialMatchRules, bson.M{
			"author.name": bson.M{
				"$eq": *complexQuery.UserName,
			},
		})
	}
	addedFields := bson.M{
		"n_images": bson.M{
			"$size": "$images",
		},
		"n_mentions": bson.M{
			"$size": "$mentions_id",
		},
		"nb_likes": bson.M{
			"$size": "$likes",
		},
		"liked": false,
	}

	query := regexp.MustCompile("\\s+").ReplaceAllString(*complexQuery.Desc, "\\s+")

	// A user can see a publication if :
	//    - The publication is public
	//    - He is the author
	//    - He is friend with his author
	orMatch := make([]bson.M, 0)
	if user != nil {
		orMatch = append(orMatch, bson.M{
			"author._id": bson.M{
				"$eq": user.Id,
			},
		},
			bson.M{
				"author.friends_ids": bson.M{
					"$elemMatch": bson.M{
						"$eq": user.Id,
					},
				},
			})
		addedFields["liked"] = bson.M{
			"likes": bson.M{
				"$cond": bson.M{
					"if": bson.M{
						"$gte": []interface{}{
							bson.M{
								"$indexOfArray": []interface{}{
									"$likes", user.Id,
								},
							},
							0,
						},
					},
					"then": true,
					"else": false,
				},
			},
		}
	}

	finalMatch := bson.M{}
	if complexQuery.Desc != nil && *complexQuery.Desc != "" {
		finalMatch["match.score"] = bson.M{
			"$gt": 0,
		}
	}

	if complexQuery.Tags != nil && len(complexQuery.Tags) > 0 {
		finalMatch["tags_found"] = bson.M{
			"$gt": 0,
		}
	}

	var pipe = []bson.M{
		{
			// Replacing the author id by the author entity
			"$lookup": bson.M{
				"as":           "author",
				"foreignField": "_id",
				"from":         "users",
				"localField":   "author",
			},
		},
		{
			// Get the author as an object not as an array (the previous lookup did that)
			"$unwind": "$author",
		},
		{
			// Mentioned users junction
			"$lookup": bson.M{
				"as":           "mentions",
				"foreignField": "_id",
				"from":         "users",
				"localField":   "mentions_id",
			},
		},
		{
			"$addFields": bson.M{
				"likes": bson.M{
					"$ifNull": []interface{}{
						"$likes", []interface{}{},
					},
				},
				"replies": bson.M{
					"$ifNull": []interface{}{
						"$replies", []interface{}{},
					},
				},
			},
		},
		{
			"$match": bson.M{
				"$and": append(specialMatchRules, []bson.M{
					{
						// Dont match the empty descriptions publications
						"description": bson.M{
							"$ne": "",
						},
					},
					{
						"$or": append(orMatch, bson.M{
							"author.public": bson.M{
								"$eq": true,
							},
						}),
					},
				}...),
			},
		},
		{
			// Setting the match regexp
			"$set": bson.M{
				"match": bson.M{
					"$regexFind": bson.M{
						"input":   "$description",
						"options": "is",
						"regex":   query,
					},
				},
				"tags_found": bson.M{
					"$size": bson.M{
						"$setIntersection": []interface{}{
							"$tags",
							complexQuery.Tags,
						},
					},
				},
			},
		},
		{
			// Setting other fields
			"$set": bson.M{
				"match": bson.M{
					"$mergeObjects": []interface{}{
						"$match",
						bson.M{
							"pretty": bson.M{
								"$concat": []string{"<q>", "$match.match", "</q>"},
							},
							"score": bson.M{
								"$add": []bson.M{
									{
										"$multiply": []interface{}{
											0.25,
											bson.M{
												"$cond": []interface{}{
													bson.M{
														"$in": []interface{}{
															"author._id",
															friendIds,
														},
													},
													1,
													0,
												},
											},
										},
									},
									{
										"$multiply": []interface{}{
											0.75,
											bson.M{
												"$divide": []interface{}{
													bson.M{
														"$strLenCP": bson.M{
															"$ifNull": []interface{}{
																"$match.match",
																"",
															},
														},
													},
													bson.M{
														"$strLenCP": "$description",
													},
												},
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
		{
			// Keep only the entries that have a score higher than 0
			"$match": finalMatch,
		},
		{
			// Sorting the entries by score
			"$sort": bson.M{
				"match.score": -1,
				"tags_found":  -1,
				"created_at":  complexQuery.Order,
			},
		},
		{
			// Skipping previous entry pages
			"$skip": (complexQuery.Page - 1) * complexQuery.Size,
		},
		{
			// Get one page
			"$limit": 20,
		},
		{
			"$set": addedFields,
		},
	}
	err := db.c.Pipe(pipe).All(&ret)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	for _, pub := range ret {
		comments, _ := db.GetExternalComments(user, pub.Id, 1, 10)

		pub.Images = db.MakePublicationImageLink(pub.Id, pub.ImagesId)
		pub.Comments = comments
		pub.Liked = pub.HasLikeFrom(user)
	}

	return ret, nil
}

func (db *dbPublications) DeleteByObjectId(oid bson.ObjectId) error {
	return db.c.Remove(bson.M{"_id": oid})
}

func (db *dbPublications) FindByTags(user *models.User, complexQuery *ComplexPublicationQuery) ([]*models.FullPublication, error) {
	pubs := make([]*models.FullPublication, 0)

	addedFields := bson.M{
		"n_images": bson.M{
			"$size": "$images",
		},
		"n_mentions": bson.M{
			"$size": "$mentions_id",
		},
		"nb_likes": bson.M{
			"$size": "$likes",
		},
		"liked": false,
	}

	orMatch := make([]bson.M, 0)
	if user != nil {
		orMatch = append(orMatch, bson.M{
			"author._id": bson.M{
				"$eq": user.Id,
			},
		},
			bson.M{
				"author.friends_ids": bson.M{
					"$elemMatch": bson.M{
						"$eq": user.Id,
					},
				},
			})
		addedFields["liked"] = bson.M{
			"likes": bson.M{
				"$cond": bson.M{
					"if": bson.M{
						"$gte": []interface{}{
							bson.M{
								"$indexOfArray": []interface{}{
									"$likes", user.Id,
								},
							},
							0,
						},
					},
					"then": true,
					"else": false,
				},
			},
		}
	}

	var pipe = []bson.M{
		{
			// Replacing the author id by the author entity
			"$lookup": bson.M{
				"as":           "author",
				"foreignField": "_id",
				"from":         "users",
				"localField":   "author",
			},
		},
		{
			// Get the author as an object not as an array (the previous lookup did that)
			"$unwind": "$author",
		},
		{
			// Mentioned users junction
			"$lookup": bson.M{
				"as":           "mentions",
				"foreignField": "_id",
				"from":         "users",
				"localField":   "mentions_id",
			},
		},
		{
			"$addFields": bson.M{
				"likes": bson.M{
					"$ifNull": []interface{}{
						"$likes", []interface{}{},
					},
				},
				"replies": bson.M{
					"$ifNull": []interface{}{
						"$replies", []interface{}{},
					},
				},
				"tags_found": bson.M{
					"$size": bson.M{
						"$setIntersection": []interface{}{
							"$tags",
							complexQuery.Tags,
						},
					},
				},
			},
		},
		{
			"$match": bson.M{
				"$and": []bson.M{
					{
						"$or": append(orMatch, bson.M{
							"author.public": bson.M{
								"$eq": true,
							},
						}),
					},
					{
						"tags_found": bson.M{
							"$ne": 0,
						},
					},
				},
			},
		},
		{
			// Sorting the entries by score
			"$sort": bson.M{
				"created_at": -1,
			},
		},
		{
			// Skipping previous entry pages
			"$skip": (complexQuery.Page - 1) * complexQuery.Size,
		},
		{
			// Get one page
			"$limit": 20,
		},
		{
			"$set": addedFields,
		},
	}
	err := db.c.Pipe(pipe).All(&pubs)

	data, _ := json.MarshalIndent(pipe, "", "   ")

	fmt.Println(string(data))

	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		fmt.Print(err)
		return nil, err
	}

	for _, pub := range pubs {
		comments, _ := db.GetExternalComments(user, pub.Id, 1, 10)

		pub.Images = db.MakePublicationImageLink(pub.Id, pub.ImagesId)
		pub.Comments = comments
		pub.Liked = pub.HasLikeFrom(user)
	}

	return pubs, nil
}

func (db *dbPublications) UnmentionUser(id bson.ObjectId) error {
	_, err := db.c.UpdateAll(bson.M{
		"mentions_id": bson.M{
			"$elemMatch": bson.M{
				"$eq": id,
			},
		},
	}, bson.M{
		"$pull": bson.M{
			"mentions_id": id,
		},
	})

	if err == mgo.ErrNotFound {
		return nil
	} else if err != nil {
		return err
	}
	return nil
}
