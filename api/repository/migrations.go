package repository

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/sirupsen/logrus"

	dbconsts "api/constants/database"
	"api/models"
)


type dbMigrations struct {
	c *mgo.Collection
}

var migrationRepository dbMigrations

func (db *dbMigrations) Connect() {
	db.c = Db.C(dbconsts.MigrationCollection)
}

func (db *dbMigrations) Migrate(name string, fnc func()) {
	logrus.Print("Analyzing migration ", name)

	nb, err := db.c.Find(bson.M{
		"name": name,
	}).Count()

	if err != nil && err != mgo.ErrNotFound {
		panic(err)
	}
	if nb == 0 {
		logrus.Println("not found executing ...")
		fnc()
		err := db.c.Insert(&models.Migration{
			Id:     bson.NewObjectId(),
			Name:   name,
			DoneAt: time.Now(),
		})

		if err != nil {
			panic(err)
		}
	} else {
		logrus.Println("found, ignoring")
	}
}
