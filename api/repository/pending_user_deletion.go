package repository

import (
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	dbconsts "api/constants/database"
	"api/models"
)

type dbPendingUserDelete struct {
	collection *mgo.Collection
}

var PendingUserDeleteRepository dbPendingUserDelete

func (db *dbPendingUserDelete) Connect() {
	db.collection = Db.C(dbconsts.PendingUserDeletion)
}

func (db *dbPendingUserDelete) Add(user *models.User) {
	_ = db.collection.Insert(user)
}

func (db *dbPendingUserDelete) Remove(user *models.User) {
	_ = db.collection.Remove(bson.M{"_id": user.Id})
}

func (db *dbPendingUserDelete) Count() int {
	ret, _ := db.collection.Find(bson.M{}).Count()

	return ret
}

func (db *dbPendingUserDelete) GetOne() *models.User {
	ret := &models.User{}

	_ = db.collection.Find(bson.M{}).One(ret)
	_ = db.collection.RemoveId(ret.Id)
	return ret
}
