package repository

import (
	"log"
	"reflect"
	"time"

	uuid "github.com/satori/go.uuid"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	dbconsts "api/constants/database"
	"api/models"
)

type dbTkn struct {
	collection *mgo.Collection
}

var TokensRepository dbTkn

func (db *dbTkn) Connect() {
	TokensRepository.collection = Db.C(dbconsts.TokensCollection)
	unique := mgo.Index{
		Key:    []string{"uuid"},
		Unique: true,
	}

	err := db.collection.EnsureIndex(unique)
	if err != nil {
		log.Fatal(err)
	}
}

func (db *dbTkn) Find() ([]models.TokenInfo, error) {
	tokens := []models.TokenInfo{}

	err := db.collection.Find(bson.M{}).All(&tokens)
	if err != nil {
		return nil, err
	}
	return tokens, nil
}

func (db *dbTkn) FindById(id string) (*models.TokenInfo, error) {
	return db.FindByObjectId(bson.ObjectIdHex(id))
}

func (db *dbTkn) FindByObjectId(id bson.ObjectId) (*models.TokenInfo, error) {
	token := &models.TokenInfo{}

	err := db.collection.Find(id).One(token)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (db *dbTkn) FindByUUID(uuid *uuid.UUID) (*models.TokenInfo, error) {
	token := &models.TokenInfo{}

	err := db.collection.Find(bson.M{"uuid": uuid.String()}).One(&token)
	if err != nil {
		return nil, err
	}
	return token, nil
}

func (db *dbTkn) Insert(info *models.TokenInfo) (*models.TokenInfo, error) {
	tkn := *info
	err := db.collection.Insert(tkn)
	if err != nil {
		return nil, err
	}
	return info, nil
}

func (db *dbTkn) DeleteById(id bson.ObjectId) error {
	return db.collection.RemoveId(id)
}

func (db *dbTkn) doFindByOwner(ownerId bson.ObjectId, tknArray interface{}) error {
	arrayValue := reflect.ValueOf(tknArray)

	err := db.collection.Find(bson.M{
		"user_id": ownerId,
	}).All(tknArray)
	if err != nil && err != mgo.ErrNotFound {
		arrayValue.SetPointer(nil)
		return nil
	} else if err != nil {
		return err
	}
	return nil
}

func (db *dbTkn) FindByOwner(ownerId bson.ObjectId) ([]*models.TokenInfo, error) {
	tokens := []*models.TokenInfo{}

	err := db.doFindByOwner(ownerId, &tokens)
	return tokens, err
}

func (db *dbTkn) FindPublicByOwner(ownerId bson.ObjectId) ([]*models.PublicTokenInfo, error) {
	tokens := []*models.PublicTokenInfo{}

	err := db.doFindByOwner(ownerId, &tokens)
	return tokens, err
}

func (db *dbTkn) DeleteByOwnerAndIds(id bson.ObjectId, tokens []string) error {

	ids := make([]bson.ObjectId, len(tokens))
	for i, token := range tokens {
		if bson.IsObjectIdHex(token) {
			ids[i] = bson.ObjectIdHex(token)
		}
	}
	if len(ids) == 0 {
		return nil
	}

	_, err := db.collection.RemoveAll(bson.M{
		"user_id": id,
		"_id": bson.M{
			"$in": ids,
		},
	})

	if err != mgo.ErrNotFound {
		return err
	}
	return nil
}

func (db *dbTkn) RevokeUser(id bson.ObjectId) error {
	err := db.collection.Remove(bson.M{
		"user_id": id,
	})

	if err == mgo.ErrNotFound {
		return nil
	} else if err != nil {
		return err
	}

	return nil
}

func (db *dbTkn) RevokeOldTokens() error {
	_, err := db.collection.RemoveAll(bson.M{
		"expiry": bson.M{
			"$lt": time.Now(),
		},
	})
	return err
}
