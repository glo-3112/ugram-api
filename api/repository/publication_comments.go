package repository

import (
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"

	dbconsts "api/constants/database"
	"api/models"
)

func (db *dbPublications) GetExternalComment(user *models.User, comId bson.ObjectId) (*models.ExternalComment, error) {
	ret := &models.ExternalComment{}

	var liked interface{} = false
	var repliesLiked interface{} = false

	if user != nil {
		liked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$gte": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
		repliesLiked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$gte": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$replies.likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
	}
	err := db.c.Pipe([]bson.M{
		{
			"$match": bson.M{
				"comments._id": comId,
			},
		},
		{
			"$unwind": "$comments",
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$comments",
			},
		},
		{
			"$match": bson.M{
				"_id": comId,
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		},
		{
			"$unwind": "$author",
		},
		{
			"$addFields": bson.M{
				"nb_likes": bson.M{
					"$size": "$likes",
				},
				"nb_replies": bson.M{
					"$size": "$replies",
				},
				"liked": liked,
			},
		},
		{
			"$set": bson.M{
				"replies": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$replies", 0, 10},
						},
						[]interface{}{}},
				},
				"likes": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$likes", 0, 10},
						},
						[]interface{}{},
					},
				},
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$replies",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$set": bson.M{
				"replies.liked": repliesLiked,
				"replies.likes": bson.M{
					"$ifNull": []interface{}{
						"$replies.likes",
						[]interface{}{},
					},
				},
			},
		},
		{
			"$set": bson.M{
				"replies.nb_likes": bson.M{
					"$size": "$replies.likes",
				},
				"replies.likes": bson.M{
					"$slice": []interface{}{"$replies.likes", 0, 5},
				},
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "replies.author",
				"foreignField": "_id",
				"as":           "replies.author",
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "replies.likes",
				"foreignField": "_id",
				"as":           "replies.likes",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$replies.author",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$group": bson.M{
				"_id": "$_id",
				"author": bson.M{
					"$first": "$author",
				},
				"text": bson.M{
					"$first": "$text",
				},
				"created_at": bson.M{
					"$first": "$created_at",
				},
				"updated_at": bson.M{
					"$first": "$updated_at",
				},
				"replies": bson.M{
					"$push": "$replies",
				},
				"nb_replies": bson.M{
					"$first": "$nb_replies",
				},
				"likes": bson.M{
					"$first": "$likes",
				},
				"liked": bson.M{
					"$first": "$liked",
				},
				"nb_likes": bson.M{
					"$first": "$nb_likes",
				},
			},
		},
	}).One(ret)

	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return ret, nil
}

func (db *dbPublications) GetExternalComments(user *models.User, pubId bson.ObjectId, page, size int) ([]*models.ExternalComment, error) {
	ret := []*models.ExternalComment{}

	var liked interface{} = false
	var repliesLiked interface{} = false

	if user != nil {
		liked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$gte": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
		repliesLiked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$gte": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$replies.likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
	}

	pipe := []bson.M{
		{
			"$match": bson.M{
				"_id": pubId,
			},
		},
		{
			"$unwind": "$comments",
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$comments",
			},
		},
		{
			"$sort": bson.M{
				"created_at": -1,
			},
		},
		{
			"$skip": (page - 1) * size,
		},
		{
			"$limit": size,
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		},
		{
			"$unwind": "$author",
		},
		{
			"$addFields": bson.M{
				"nb_likes": bson.M{
					"$size": "$likes",
				},
				"nb_replies": bson.M{
					"$size": "$replies",
				},
				"liked": liked,
			},
		},
		{
			"$set": bson.M{
				"replies": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$replies", 0, 10},
						},
						[]interface{}{}},
				},
				"likes": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$likes", 0, 10},
						},
						[]interface{}{},
					},
				},
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$replies",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$set": bson.M{
				"replies.likes": bson.M{
					"$ifNull": []interface{}{
						"$replies.likes",
						[]interface{}{},
					},
				},
			},
		},
		{
			"$set": bson.M{
				"replies.nb_likes": bson.M{
					"$size": "$replies.likes",
				},
				"replies.likes": bson.M{
					"$slice": []interface{}{"$replies.likes", 0, 5},
				},
				"replies.liked": repliesLiked,
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "replies.author",
				"foreignField": "_id",
				"as":           "replies.author",
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "replies.likes",
				"foreignField": "_id",
				"as":           "replies.likes",
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$replies.author",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$group": bson.M{
				"_id": "$_id",
				"author": bson.M{
					"$first": "$author",
				},
				"text": bson.M{
					"$first": "$text",
				},
				"created_at": bson.M{
					"$first": "$created_at",
				},
				"updated_at": bson.M{
					"$first": "$updated_at",
				},
				"replies": bson.M{
					"$push": "$replies",
				},
				"nb_replies": bson.M{
					"$first": "$nb_replies",
				},
				"likes": bson.M{
					"$first": "$likes",
				},
				"liked": bson.M{
					"$first": "$liked",
				},
				"nb_likes": bson.M{
					"$first": "$nb_likes",
				},
			},
		},
	}

	err := db.c.Pipe(pipe).All(&ret)

	if err != nil {
		return nil, err
	}

	for _, com := range ret {
		// Because of the multiple unwind, lookup put empty replies.likes array. MGO generate an empty ExternalReply object
		if len(com.Replies) == 1 && com.Replies[0].Id == "" {
			com.Replies = []*models.ExternalReply{}
		}
	}

	return ret, nil
}

func (db *dbPublications) GetExternalCommentReply(user *models.User, pId, cId, rId bson.ObjectId) (*models.ExternalReply, error) {
	ret := &models.ExternalReply{}

	var liked interface{} = false

	if user != nil {
		liked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$eq": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
	}

	pipe := []bson.M{
		{
			"$match": bson.M{
				"_id": pId,
			},
		},
		{
			"$unwind": "$comments",
		},
		{
			"$match": bson.M{
				"comments._id": cId,
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$comments.replies",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$match": bson.M{
				"comments.replies._id": rId,
			},
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$comments.replies",
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		},
		{
			"$unwind": "$author",
		},
		{
			"$addFields": bson.M{
				"nb_likes": bson.M{
					"$size": "$likes",
				},
				"liked": liked,
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		},
		{
			"$set": bson.M{
				"likes": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$likes", 0, 10},
						},
						[]interface{}{},
					},
				},
			},
		},
	}

	err := db.c.Pipe(pipe).One(ret)

	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	} else if err == mgo.ErrNotFound {
		return nil, nil
	}

	return ret, nil
}

func (db *dbPublications) GetExternalCommentReplies(user *models.User, pId, cId bson.ObjectId, page, size int) ([]*models.ExternalReply, error) {
	ret := []*models.ExternalReply{}

	var liked interface{} = false

	if user != nil {
		liked = bson.M{
			"$cond": bson.M{
				"if": bson.M{
					"$gte": []interface{}{
						bson.M{
							"$indexOfArray": []interface{}{
								"$likes", user.Id,
							},
						},
						0,
					},
				},
				"then": true,
				"else": false,
			},
		}
	}

	pipe := []bson.M{
		{
			"$match": bson.M{
				"_id": pId,
			},
		},
		{
			"$unwind": "$comments",
		},
		{
			"$match": bson.M{
				"comments._id": cId,
			},
		},
		{
			"$unwind": bson.M{
				"path":                       "$comments.replies",
				"preserveNullAndEmptyArrays": true,
			},
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$comments.replies",
			},
		},
		{
			"$skip": (page - 1) * size,
		},
		{
			"$limit": size,
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		},
		{
			"$unwind": "$author",
		},
		{
			"$set": bson.M{
				"likes": bson.M{
					"$ifNull": []interface{}{
						"likes",
						[]interface{}{},
					},
				},
			},
		},
		{
			"$addFields": bson.M{
				"nb_likes": bson.M{
					"$size": "$likes",
				},
				"liked": liked,
			},
		},
		{
			"$lookup": bson.M{
				"from":         dbconsts.UsersCollection,
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		},
		{
			"$set": bson.M{
				"likes": bson.M{
					"$ifNull": []interface{}{
						bson.M{
							"$slice": []interface{}{"$likes", 0, 10},
						},
						[]interface{}{},
					},
				},
			},
		},
	}

	err := db.c.Pipe(pipe).All(&ret)

	if err != nil {
		return nil, err
	}

	return ret, nil
}

func (db *dbPublications) AddComment(author *models.User, comment *models.CreateComment, pub *models.Publication) (*models.ExternalComment, error) {
	com := models.InternalComment{
		Id:        bson.NewObjectId(),
		Author:    author.Id,
		Text:      comment.Text,
		CreatedAt: time.Now(),
		Replies:   []*models.InternalReply{},
		LikesId:   []bson.ObjectId{},
	}

	err := db.c.Update(bson.M{
		"_id": pub.Id,
	}, bson.M{
		"$addToSet": bson.M{
			"comments": com,
		},
	})

	if err != nil {
		return nil, err
	}

	if author, err := UsersRepository.FindLightPublicById(com.Author); err != nil || author == nil {
		return nil, err
	} else {
		exComment := &models.ExternalComment{
			Id:        com.Id,
			Author:    *author,
			Text:      comment.Text,
			CreatedAt: com.CreatedAt,
			UpdatedAt: com.UpdatedAt,
			Replies:   []*models.ExternalReply{},
			Likes:     []*models.LightUserPublic{},
		}

		return exComment, nil
	}
}

func (db *dbPublications) ReplyToExternal(reply *models.InternalReply) (*models.ExternalReply, error) {
	ret := &models.ExternalReply{
		Id:        reply.Id,
		Text:      reply.Text,
		CreatedAt: reply.CreatedAt,
		UpdatedAt: reply.UpdatedAt,
	}
	if author, err := UsersRepository.FindLightPublicById(reply.Author); err != nil || author == nil {
		return nil, err
	} else {
		ret.Author = *author
		if likes, err := UsersRepository.FindLightPublicByIds(reply.Likes); err != nil {
			return nil, err
		} else {
			ret.Likes = likes
			return ret, nil
		}
	}
}

func (db *dbPublications) RepliesToExternal(replies []*models.InternalReply) ([]*models.ExternalReply, error) {
	ret := []*models.ExternalReply{}

	for _, reply := range replies {
		if author, err := UsersRepository.FindLightPublicById(reply.Author); err != nil || author == nil {
			return nil, err
		} else {
			if likes, err := UsersRepository.FindLightPublicByIds(reply.Likes); err != nil {
				return nil, err
			} else {
				appnd := models.ExternalReply{
					Id:        reply.Id,
					Author:    *author,
					Text:      reply.Text,
					CreatedAt: reply.CreatedAt,
					UpdatedAt: reply.UpdatedAt,
					Likes:     likes,
				}
				ret = append(ret, &appnd)
			}
		}
	}

	return ret, nil
}

func (db *dbPublications) AddReply(author *models.User, id bson.ObjectId, reply *models.CreateReply) (*models.ExternalReply, error) {
	rep := &models.InternalReply{
		Id:        bson.NewObjectId(),
		Author:    author.Id,
		Text:      reply.Text,
		CreatedAt: time.Now(),
		Likes:     []bson.ObjectId{},
	}

	err := db.c.Update(bson.M{
		"comments._id": id,
	}, bson.M{
		"$addToSet": bson.M{
			"comments.$.replies": rep,
		},
	})

	if err != nil {
		return nil, err
	}

	return db.ReplyToExternal(rep)
}

func (db *dbPublications) IsCommentLiked(user *models.User, pId bson.ObjectId, cId bson.ObjectId) (bool, error) {
	nb, err := db.c.Find(bson.M{
		"_id":          pId,
		"comments._id": cId,
		"comments.likes": bson.M{
			"$elemMatch": bson.M{
				"$eq": user.Id,
			},
		},
	}).Count()

	if err != nil {
		return false, err
	} else if nb == 1 {
		return true, nil
	}

	return false, nil
}

func (db *dbPublications) LikeComment(user *models.User, pId bson.ObjectId, cId bson.ObjectId) error {
	err := db.c.Update(bson.M{
		"_id":          pId,
		"comments._id": cId,
	}, bson.M{
		"$addToSet": bson.M{
			"comments.$.likes": user.Id,
		},
	})

	return err
}

func (db *dbPublications) UnlikeComment(user *models.User, pId bson.ObjectId, cId bson.ObjectId) error {
	err := db.c.Update(bson.M{
		"_id":          pId,
		"comments._id": cId,
	}, bson.M{
		"$pull": bson.M{
			"comments.$.likes": user.Id,
		},
	})

	return err
}

func (db *dbPublications) IsReplyLiked(user *models.User, pId bson.ObjectId, cId bson.ObjectId, rId bson.ObjectId) (bool, error) {
	pub := &models.Publication{}

	err := db.c.Find(bson.M{
		"_id":                  pId,
		"comments._id":         cId,
		"comments.replies._id": rId,
	}).One(pub)

	if err != nil {
		return false, err
	}

	for _, comm := range pub.Comments {
		if comm.Id != cId {
			continue
		}
		for _, reply := range comm.Replies {
			if reply.Id != rId {
				continue
			}

			for _, like := range reply.Likes {
				if like == user.Id {
					return true, nil
				}
			}
		}
	}
	return false, nil
}

func (db *dbPublications) LikeReply(user *models.User, pId bson.ObjectId, cId bson.ObjectId, rId bson.ObjectId) error {
	pub := &models.Publication{}

	err := db.c.Find(bson.M{
		"_id":                  pId,
		"comments._id":         cId,
		"comments.replies._id": rId,
	}).One(pub)

	if err != nil {
		return err
	}

	for _, comm := range pub.Comments {
		if comm.Id != cId {
			continue
		}
		for _, reply := range comm.Replies {
			if reply.Id != rId {
				continue
			}

			for _, like := range reply.Likes {
				if like == user.Id {
					goto ret
				}
			}
			reply.Likes = append(reply.Likes, user.Id)
			_, err = db.Persist(pub)
		}
	}

ret:
	return err
}

func (db *dbPublications) UnlikeReply(user *models.User, pId bson.ObjectId, cId bson.ObjectId, rId bson.ObjectId) error {
	pub := &models.Publication{}

	err := db.c.Find(bson.M{
		"_id":                  pId,
		"comments._id":         cId,
		"comments.replies._id": rId,
	}).One(pub)

	if err != nil {
		return err
	}

	for _, comm := range pub.Comments {
		if comm.Id != cId {
			continue
		}
		for _, reply := range comm.Replies {
			if reply.Id != rId {
				continue
			}

			for i, like := range reply.Likes {
				if like == user.Id {
					copy(reply.Likes[i:], reply.Likes[i+1:])
					reply.Likes[len(reply.Likes)-1] = ""
					reply.Likes = reply.Likes[:len(reply.Likes)-1]
					_, err = db.Persist(pub)
					goto ret
				}
			}
		}
	}

ret:
	return err
}
