package repository

import (
	"fmt"
	"runtime/debug"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/sirupsen/logrus"

	"api/models"
)

type dbNotifications struct {
}

var NotificationRepository = dbNotifications{}

func (n *dbNotifications) NotifyPublicationComment(publicationAuthor, publication bson.ObjectId, author *models.User, text string) {
	if publicationAuthor == author.Id {
		return
	}

	n.notify(publicationAuthor, &models.Notification{
		Id:       bson.NewObjectId(),
		Type:     "comment",
		Target:   "publication",
		TargetId: publication,
		Author: models.LightUserPublic{
			Id:             author.Id,
			Name:           author.Name,
			ProfilePicture: &author.ProfilePicture,
		},
		Text: &text,
	})
}

func (n *dbNotifications) NotifyPublicationLike(publicationAuthor, publication bson.ObjectId, author *models.User) {
	if publicationAuthor == author.Id {
		return
	}

	n.notify(publicationAuthor, &models.Notification{
		Id:       bson.NewObjectId(),
		Type:     "like",
		Target:   "publication",
		TargetId: publication,
		Author: models.LightUserPublic{
			Id:             author.Id,
			Name:           author.Name,
			ProfilePicture: &author.ProfilePicture,
		},
		Text: nil,
	})
}

func (n *dbNotifications) NotifyPublicationCommentReply(publicationAuthor, comment bson.ObjectId, author *models.User, text string) {
	if publicationAuthor == author.Id {
		return
	}

	n.notify(publicationAuthor, &models.Notification{
		Id:       bson.NewObjectId(),
		Type:     "reply",
		Target:   "comment",
		TargetId: comment,
		Author: models.LightUserPublic{
			Id:             author.Id,
			Name:           author.Name,
			ProfilePicture: &author.ProfilePicture,
		},
		Text: &text,
	})
}

func (n *dbNotifications) NotifyPublicationCommentLike(publicationAuthor, comment bson.ObjectId, author *models.User) {
	// if publicationAuthor == author.Id {
	// 	return
	// }

	user, err := UsersRepository.FindByObjectId(publicationAuthor)

	if user != nil && err != nil {
		fmt.Println(user)
	}
	n.notify(publicationAuthor, &models.Notification{
		Id:       bson.NewObjectId(),
		Type:     "like",
		Target:   "comment",
		TargetId: comment,
		Author: models.LightUserPublic{
			Id:             author.Id,
			Name:           author.Name,
			ProfilePicture: &author.ProfilePicture,
		},
		Text: nil,
	})
}

func (n *dbNotifications) NotifyPublicationCommentReplyLike(publicationAuthor, reply bson.ObjectId, author *models.User) {
	if publicationAuthor == author.Id {
		return
	}

	n.notify(publicationAuthor, &models.Notification{
		Id:       bson.NewObjectId(),
		Type:     "like",
		Target:   "reply",
		TargetId: reply,
		Author: models.LightUserPublic{
			Id:             author.Id,
			Name:           author.Name,
			ProfilePicture: &author.ProfilePicture,
		},
		Text: nil,
	})
}

func (n *dbNotifications) notify(userId bson.ObjectId, notification *models.Notification) {
	notification.Read = false
	notification.Date = time.Now()

	_, err := UsersRepository.collection.UpdateAll(bson.M{
		"_id": userId,
	}, bson.M{
		"$addToSet": bson.M{
			"notifications": notification,
		},
	})

	if err != nil {
		debug.PrintStack()
		logrus.Errorln(err)
	}
}

func (n *dbNotifications) ReadNotification(userId, notificationId bson.ObjectId) (*models.Notification, error) {
	_, err := UsersRepository.collection.UpdateAll(bson.M{
		"_id": userId,
		"notifications._id": notificationId,
	}, bson.M{
		"$set": bson.M{
			"notifications.$.read": true,
		},
	})

	if err != nil {
		return nil, err
	}

	ret := &models.Notification{}

	err = UsersRepository.collection.Pipe([]bson.M{
		{
			"$match": bson.M{
				"_id": userId,
			},
		},
		{
			"$unwind": "$notifications",
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$notifications",
			},
		},
		{
			"$match": bson.M{
				"_id": notificationId,
			},
		},
	}).One(ret)

	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	}

	return ret, nil
}

func (n *dbNotifications) GetNotificationsPaged(userId bson.ObjectId, page, size int) ([]*models.Notification, error) {
	ret := []*models.Notification{}

	err := UsersRepository.collection.Pipe([]bson.M{
		{
			"$match": bson.M{
				"_id": userId,
			},
		},
		{
			"$unwind": "$notifications",
		},
		{
			"$replaceRoot": bson.M{
				"newRoot": "$notifications",
			},
		},
		{
			"$skip": (page - 1) * size,
		},
		{
			"$limit": size,
		},
	}).All(&ret)

	if err != nil && err != mgo.ErrNotFound {
		return nil, err
	}

	return ret, nil
}

func (n *dbNotifications) FlushNotifications(userId bson.ObjectId) error {
	err := UsersRepository.collection.Update(bson.M{
		"_id": userId,
	}, bson.M{
		"$set": bson.M{
			"notifications": []interface{}{},
		},
	})

	if err != mgo.ErrNotFound {
		return err
	}

	return nil
}
