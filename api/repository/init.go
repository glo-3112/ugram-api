package repository

import (
	"log"
	"os"
	"time"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/sirupsen/logrus"

	dbconsts "api/constants/database"
)

var Db *mgo.Database

func init() {
	mgoTimeout := dbconsts.Timeout
	mgoDialInfo := &mgo.DialInfo{
		Addrs:    []string{os.Getenv(dbconsts.MongoAddress)},
		Timeout:  time.Duration(mgoTimeout) * time.Second,
		Database: os.Getenv(dbconsts.MongoDatabase),
		Username: os.Getenv(dbconsts.MongoUsername),
		Password: os.Getenv(dbconsts.MongoPassword),
	}

	session, err := mgo.DialWithInfo(mgoDialInfo)
	if err != nil {
		log.Panic(err)
	}
	Db = session.DB(os.Getenv(dbconsts.MongoDatabase))

	PendingUserDeleteRepository = dbPendingUserDelete{}
	PendingUserDeleteRepository.Connect()

	PublicationsRepository = dbPublications{}
	PublicationsRepository.Connect()

	PendingRequestUserDataRepository = dbRequestUserData{}
	PendingRequestUserDataRepository.connect()

	TagsRepository = dbTags{}
	TagsRepository.Connect()

	TokensRepository = dbTkn{}
	TokensRepository.Connect()

	UsersRepository = dbUser{}
	UsersRepository.connect()

	migrationRepository = dbMigrations{}
	migrationRepository.Connect()

	migrationRepository.Migrate("Likes001", migrationLikes)
	migrationRepository.Migrate("Comments001", migrationComments)
	migrationRepository.Migrate("Notifications001", migrationNotifications)

	logrus.Infoln("")
}

func migrationLikes() {
	count, err := PublicationsRepository.c.UpdateAll(bson.M{
		"likes": bson.M{
			"$exists": false,
		},
	}, []bson.M{
		{
			"$addFields": bson.M{
				"likes": []interface{}{},
			},
		},
	})

	if err != nil {
		panic(err)
	}

	logrus.Println("Matched items", count.Matched, "Updated items", count.Updated, "Removed items", count.Removed)
	logrus.Println("Migration done at", time.Now())
}

func migrationComments() {
	count, err := PublicationsRepository.c.UpdateAll(bson.M{
		"comments": bson.M{
			"$exists": false,
		},
	}, []bson.M{
		{
			"$addFields": bson.M{
				"comments": []interface{}{},
			},
		},
	})

	if err != nil {
		panic(err)
	}

	logrus.Println("Matched items", count.Matched, "Updated items", count.Updated, "Removed items", count.Removed)
	logrus.Println("Migration done at", time.Now())
}


func migrationNotifications() {
	count, err := UsersRepository.collection.UpdateAll(bson.M{
		"notifications": bson.M{
			"$exists": false,
		},
	}, []bson.M{
		{
			"$addFields": bson.M{
				"notifications": []interface{}{},
			},
		},
	})

	if err != nil {
		panic(err)
	}

	logrus.Println("Matched items", count.Matched, "Updated items", count.Updated, "Removed items", count.Removed)
	logrus.Println("Migration done at", time.Now())
}