package repository

import (
	"github.com/globalsign/mgo/bson"
)

// Generate a request that solve comments nesting with a depth of 3
func commentLookup() []bson.M {
	return []bson.M{
		{
			"$unwind": "$comment",
		},
		{
			"$unwind": "$replies",
		}, {
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "replies.author",
				"foreignField": "_id",
				"as":           "replies.author",
			},
		}, {
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "replies.likes",
			},
		}, {
			"$unwind": "$replies.author",
		}, {
			"$group": bson.M{
				"_id": "$_id",
				"author": bson.M{
					"$first": "$author",
				},
				"text": bson.M{
					"$first": "$text",
				},
				"likes": bson.M{
					"$first": "$likes",
				},
				"replies": bson.M{
					"$push": "$replies",
				},
			},
		}, {
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "likes",
				"foreignField": "_id",
				"as":           "likes",
			},
		}, {
			"$lookup": bson.M{
				"from":         "users",
				"localField":   "author",
				"foreignField": "_id",
				"as":           "author",
			},
		}, {
			"$unwind": "$author",
		},
	}
}
