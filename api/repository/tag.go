package repository

import (
	dbconsts "api/constants/database"
	"api/models"
	"fmt"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type dbTags struct {
	collection *mgo.Collection
}

var TagsRepository dbTags

func (dbTags *dbTags) Connect() {
	dbTags.collection = Db.C(dbconsts.TagsCollection)
}

func (dbTags *dbTags) CreateTag(name string) (*models.Tag, error) {
	insert := &models.Tag{
		Id:    bson.NewObjectId(),
		Name:  name,
		Score: 1,
	}

	if err := dbTags.collection.Insert(insert); err != nil {
		return nil, err
	}
	fmt.Println(insert)

	return insert, nil
}

func (dbTags *dbTags) CreateTags(names ...string) ([]*models.Tag, error) {
	var inserts []*models.Tag

	for _, name := range names {
		insert := &models.Tag{
			Id:    bson.NewObjectId(),
			Name:  name,
			Score: 1,
		}

		if err := dbTags.collection.Insert(insert); err != nil {
			return nil, err
		}
		inserts = append(inserts, insert)
	}
	return inserts, nil
}

func (dbTags *dbTags) GetAllTags(page int, size int) ([]*models.Tag, error ){
	ret := make([]*models.Tag, 0)

	err := dbTags.collection.Pipe([]bson.M{
		{
			"$sort": bson.M{
				"score": -1,
			},
		},
		{
			"$skip": (page - 1) * size,
		},
		{
			"$limit": size,
		},
	}).All(&ret)
	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return ret, nil
}

func (dbTags *dbTags) FindByID(ID string) (*models.Tag, error) {
	tag := &models.Tag{}

	err := dbTags.collection.Find(bson.M{
		"_id": bson.ObjectIdHex(ID),
	}).One(tag)
	if err != nil {
		return nil, err
	}
	return tag, nil
}

func (dbTags *dbTags) FindByName(name string) (*models.Tag, error) {
	tag := &models.Tag{}

	err := dbTags.collection.Find(bson.M{
		"name": name,
	}).One(tag)

	if err != nil {
		return nil, err
	}

	return tag, nil
}

func (dbTags *dbTags) UpdateScore(names []string) error {
	var tag *models.Tag
	var err error

	for _, name := range names {
		tag, err = dbTags.FindByName(name)

		if err == mgo.ErrNotFound{
			tag, err = dbTags.CreateTag(name)
			if err != nil {
				return err
			}
		} else if err == nil {
			tag.Score += 1

			err = dbTags.collection.Update(bson.M{
				"name": name,
			}, tag)
			if err != nil {
				return err
			}
		} else {
			return err
		}
	}
	return nil
}

func (dbTags *dbTags) DeleteObjectID(oid bson.ObjectId) error {
	return dbTags.collection.Remove(bson.M{"_id": oid})
}

func (dbTags *dbTags) GetContainingTags(partialTag string) ([]*models.Tag, error) {
	ret := make([]*models.Tag, 0)

	regex := "(?i).*" + partialTag + ".*"

	fmt.Println(regex)
	err := dbTags.collection.Find(bson.M{
			"name": bson.M{
				"$regex": bson.RegEx{regex, ""},
			},
	}).All(&ret)

	if err != nil && err == mgo.ErrNotFound {
		return nil, nil
	} else if err != nil {
		return nil, err
	}

	return ret, nil
}