package models

import (
	"github.com/mileusna/useragent"
)

type UserAgent struct {
	Name    string `bson:"name" json:"name"`
	Version string `bson:"version" json:"version"`
	Device  string `bson:"device" json:"device"`
	Os      string `bson:"os" json:"os"`
}

func CreateUserAgentFromString(userAgent string) UserAgent {
	agent := ua.Parse(userAgent)

	device := ""
	if agent.Desktop {
		device = "Desktop"
	} else if agent.Mobile {
		device = "Mobile"
	} else if agent.Tablet {
		device = "Tablet"
	} else if agent.Bot {
		device = "Bot"
	}

	return UserAgent{
		Name:    agent.Name,
		Version: agent.Version,
		Device:  device,
		Os:      agent.OS,
	}
}
