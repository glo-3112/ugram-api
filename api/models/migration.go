package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type Migration struct {
	Id     bson.ObjectId `bson:"_id"`
	Name   string        `bson:"name"`
	DoneAt time.Time     `bson:"time"`
}
