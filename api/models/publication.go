package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type Publication struct {
	Id          bson.ObjectId      `bson:"_id" json:"id,omitempty"`
	Author      bson.ObjectId      `bson:"author" json:"author"`
	Description string             `bson:"description" json:"description"`
	Tags        []string           `bson:"tags" json:"tags"`
	MentionsId  []bson.ObjectId    `bson:"mentions_id" json:"mentionsId"`
	Mentions    []UserPublic       `bson:"-" json:"mentions"`
	LikesId     []bson.ObjectId    `bson:"likes" json:"-"`
	Comments    []*InternalComment `bson:"comments" json:"comments"`
	Images      []bson.ObjectId    `bson:"images" json:"images"`
	CreatedAt   time.Time          `bson:"created_at" json:"createdAt"`
	UpdatedAt   time.Time          `bson:"updated_at" json:"updatedAt"`
}

type PublicationCreate struct {
	Description string          `json:"description"`
	Tags        []string        `json:"tags"`
	MentionsId  []bson.ObjectId `json:"mentionsId"`
	Images      []string        `json:"images"`
}

type FullPublication struct {
	Id          bson.ObjectId          `bson:"_id" json:"id,omitempty"`
	Author      *UserPublic            `bson:"author" json:"author"`
	Description string                 `bson:"description" json:"description"`
	Tags        []string               `bson:"tags" json:"tags"`
	Mentions    []*UserPublic          `bson:"mentions" json:"mentions"`
	Images      []PublicImage          `bson:"-" json:"images"`
	ImagesId    []bson.ObjectId        `bson:"images" json:"-"`
	Comments    []*ExternalComment     `bson:"comments" json:"comments"`
	Likes       []LightUserPublic      `bson:"likes" json:"likes"`
	Liked       bool                   `bson:"liked" json:"liked"`
	NbLikes     int                    `bson:"nb_likes" json:"nbLikes"`
	CreatedAt   time.Time              `bson:"created_at" json:"createdAt"`
	UpdatedAt   time.Time              `bson:"updated_at" json:"updatedAt"`
	Score       float32                `bson:"score" json:"score,omitempty"`
	DescReg     map[string]interface{} `bson:"desc_reg" json:"desc_reg,omitempty"`
}

type SearchPublication struct {
	Id          bson.ObjectId    `bson:"_id" json:"id"`
	Author      *LightUserPublic `bson:"author" json:"author"`
	Description string           `bson:"description" json:"description"`
	Match       *Match           `bson:"match" json:"match"`
}

func (p *Publication) HasLikeFrom(user *User) bool {
	if user == nil {
		return false
	}
	for _, u := range p.LikesId {
		if u == user.Id {
			return true
		}
	}
	return false
}

func (p *FullPublication) HasLikeFrom(user *User) bool {
	if user == nil {
		return false
	}
	for _, u := range p.Likes {
		if u.Id == user.Id {
			return true
		}
	}
	return false
}

func (p *Publication) LikeFrom(user *User) *Publication {
	if user == nil {
		return p
	}
	if !p.HasLikeFrom(user) {
		p.LikesId = append(p.LikesId, user.Id)
	}
	return p
}

func (p *Publication) UnlikeFrom(user *User) *Publication {
	if p.HasLikeFrom(user) {
		i := 0
		for _, u := range p.LikesId {
			if u == user.Id {
				break
			}
			i++
		}

		if i >= len(p.LikesId) {
			i = len(p.LikesId) - 1
		}
		p.LikesId = append(p.LikesId[:i], p.LikesId[i+1:]...)
	}
	return p
}
