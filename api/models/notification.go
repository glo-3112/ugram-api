package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type Notification struct {
	Id       bson.ObjectId   `bson:"_id" json:"id"`
	Type     string          `bson:"type" json:"type"`
	Target   string          `bson:"target" json:"target"`
	TargetId bson.ObjectId   `bson:"target_id" json:"targetId"`
	Author   LightUserPublic `bson:"author" json:"author"`
	Text     *string         `bson:"text" json:"text,omitempty"`
	Read     bool            `bson:"read" json:"read"`
	Date     time.Time       `bson:"date" json:"date"`
}
