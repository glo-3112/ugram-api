package models

type Match struct {
	Raw    string  `bson:"match" json:"match"`
	Index  int     `bson:"idx" json:"index"`
	Score  float64 `bson:"score" json:"score"`
	Pretty string  `bson:"pretty" json:"pretty"`
}
