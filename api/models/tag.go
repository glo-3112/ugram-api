package models

import "github.com/globalsign/mgo/bson"

type Tag struct {
	Id		bson.ObjectId 	`bson:"_id" json:"id,omitempty"`
	Name 	string			`bson:"name" json:"name"`
	Score 	int				`bson:"score" json:"score,omitempty"`
}

type AddTag struct {
	Name  string `bson:"name" json:"name"`
}