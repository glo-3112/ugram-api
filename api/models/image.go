package models

import (
	"github.com/globalsign/mgo/bson"
)

type PublicImage struct {
	Id      bson.ObjectId `json:"id"`
	MdThumb string        `json:"medium"`
	SmThumb string        `json:"small"`
	Normal  string        `json:"original"`
}
