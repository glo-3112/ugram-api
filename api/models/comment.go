package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type CreateComment struct {
	Text string `json:"text"`
}

type CreateReply struct {
	Text string `json:"text"`
}

type InternalComment struct {
	Id        bson.ObjectId    `bson:"_id" json:"id"`
	Author    bson.ObjectId    `bson:"author" json:"authorId"`
	Text      string           `bson:"text" json:"text"`
	CreatedAt time.Time        `bson:"created_at" json:"createdAt"`
	UpdatedAt *time.Time       `bson:"updated_at" json:"updatedAt"`
	Replies   []*InternalReply `bson:"replies" json:"replies"`
	LikesId   []bson.ObjectId  `bson:"likes" json:"likesId"`
}

type ExternalComment struct {
	Id        bson.ObjectId      `bson:"_id" json:"id"`
	Author    LightUserPublic    `bson:"author" json:"author"`
	Text      string             `bson:"text" json:"text"`
	CreatedAt time.Time          `bson:"created_at" json:"createdAt"`
	UpdatedAt *time.Time         `bson:"updated_at" json:"updatedAt"`
	Replies   []*ExternalReply   `bson:"replies" json:"replies"`
	NbReplies int                `bson:"nb_replies" json:"nbReplies"`
	Likes     []*LightUserPublic `bson:"likes" json:"likes"`
	Liked     bool               `bson:"liked" json:"liked"`
	NbLikes   int                `bson:"nb_likes" json:"nbLikes"`
}

type InternalReply struct {
	Id        bson.ObjectId   `bson:"_id" json:"id"`
	Author    bson.ObjectId   `bson:"author" json:"author"`
	Text      string          `bson:"text" json:"text"`
	CreatedAt time.Time       `bson:"created_at" json:"createdAt"`
	UpdatedAt *time.Time      `bson:"updated_at" json:"updatedAt"`
	Likes     []bson.ObjectId `bson:"likes" json:"likes"`
}

type ExternalReply struct {
	Id        bson.ObjectId      `bson:"_id" json:"id"`
	Author    LightUserPublic    `bson:"author" json:"author"`
	Text      string             `bson:"text" json:"text"`
	CreatedAt time.Time          `bson:"created_at" json:"createdAt"`
	UpdatedAt *time.Time         `bson:"updated_at" json:"updatedAt"`
	Likes     []*LightUserPublic `bson:"likes" json:"likes"`
	Liked     bool               `bson:"liked" json:"liked"`
	NbLikes   int                `bson:"nb_likes" json:"nbLikes"`
}
