package models

import (
	"golang.org/x/oauth2"
)

type GitHubUser struct {
	Id        int     `json:"id"`
	UserName  string  `json:"login"`
	Name      string  `json:"name"`
	Mail      string  `json:"email"`
	AvatarUrl *string `json:"avatar_url,omitempty"`
}

type GitLabUser struct {
	Id        int     `json:"id"`
	UserName  string  `json:"username"`
	Name      string  `json:"name"`
	Mail      string  `json:"email"`
	AvatarUrl *string `json:"avatar_url,omitempty"`
}

type OauthUser struct {
	Id        int           `json:"id"`
	UserName  string        `json:"username"`
	Name      string        `json:"name"`
	Mail      string        `json:"email"`
	AvatarUrl *string       `json:"avatar_url,omitempty"`
	Token     *oauth2.Token `json:"-"`
}
