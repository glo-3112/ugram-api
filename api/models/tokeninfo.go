package models

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type TokenInfo struct {
	Id         bson.ObjectId `bson:"_id" json:"id,omitempty"`
	Uuid       string        `bson:"uuid" json:"uuid"`
	Expiry     time.Time     `bson:"expiry" json:"expiry"`
	IssuedAt   time.Time     `bson:"issued_at" json:"issuedAt"`
	UserId     bson.ObjectId `bson:"user_id" json:"userId"`
	RemoteAddr string        `bson:"remote_addr" json:"remoteAddr"`
	IssuedFor  string        `bson:"issued_for" json:"issuedFor"`
	UserAgent  UserAgent     `bson:"user_agent" json:"userAgent"`
}

type RateLimit struct {
	Limit     int64     `bson:"limit" json:"limit"`
	Remaining int64     `bson:"remaining" json:"remainingCredit"`
	Reset     time.Time `bson:"reset" json:"reset"`
}

type PublicTokenInfo struct {
	Id         bson.ObjectId `bson:"_id" json:"id"`
	IssuedAt   time.Time     `bson:"issued_at" json:"issuedAt"`
	RemoteAddr string        `bson:"remote_addr" json:"remoteAddr"`
	IssuedFor  string        `bson:"issued_for" json:"issuedFor"`
	UserAgent  UserAgent     `bson:"user_agent" json:"userAgent"`
}
