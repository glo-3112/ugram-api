package models

import (
	"time"

	"golang.org/x/oauth2"
	"github.com/globalsign/mgo/bson"
)

type User struct {
	Id bson.ObjectId `bson:"_id" json:"id,omitempty"`

	// This field contain the user's roles
	Roles []string `bson:"roles" json:"roles"`

	// This field contain the user's name (username)
	Name string `bson:"name" json:"username"`

	// This field contain the user password hashed
	//     This field is for backend purpose only.
	//     It should never be serialized
	Hash string `bson:"hash" json:"-"`

	// This field contain the user first name
	Firstname string `bson:"firstname" json:"firstName"`

	// This field contain the user last name
	Lastname string `bson:"lastname" json:"lastName"`

	// This field contain the user mail address
	//     When the user sign up with OAuth the mail address
	//     provided by the OAuth provider become his mail address by default
	Mail string `bson:"mail" json:"mail"`

	// This field contain the user's phone number
	PhoneNumber *PhoneNumber `bson:"phone_number" json:"phoneNumber"`

	// This map contain all the user oauth providers
	//    This is a private field (aka for mongo only)
	//    It should never be serialized
	AuthProviders map[string]AuthProviderInformations `bson:"auth_providers,omitempty" json:"-"`

	// This boolean tells if an account is activated or not.
	//     It is used by the OAuth procedure to ask the user to complete his profile
	//     It is not serialized because it is not necessary
	Activated bool `bson:"activated" json:"-"`

	// This boolean tells if the account is public or private.
	//     If it is true, then the account is public and private otherwise.
	//     By default all accounts are public
	Public bool `bson:"public" json:"public"`

	// This field contain all the user's friends id
	//   This is a private field (aka for mongo only)
	//   It should never be serialized
	FriendsIds []bson.ObjectId `bson:"friends_ids" json:"friendsId"`

	// This field can contain both an array of ids or an array of PublicUser
	// It's the public version of FriendsId
	Friends []*User `bson:"-" json:"friends"`

	// A foreign key to Pictures image
	ProfilePicture bson.ObjectId `bson:"profile_picture" json:"profilePicture"`

	// This field contain all the publication ids of the user
	//     It's a field present in Mongo
	PublicationIds []bson.ObjectId `bson:"publication_ids" json:"publicationsId"`

	// This field contain all the publications (resolved this time) of the user
	//     It's a field not present in Mongo
	Publications []Publication `bson:"-" json:"publications"`

	// This field tells the date when the user sign up
	RegistrationDate time.Time `bson:"registration_date" json:"registrationDate"`

	// This field contain the date since the last sign in
	LastSignIn time.Time `bson:"last_sign_in" json:"lastSignIn"`

	// This field contain the security information about the current api requesting
	RateLimit *RateLimit `bson:"rate_limit" json:"rateLimit"`

	RequestedUserData []*BundleUserData `bson:"bundle_user_data" json:"bundleUserData"`
	
	Notification []*Notification `bson:"notifications" json:"-"`
}

type AuthProviderInformations struct {
	ForeignId     string         `bson:"foreign_id" json:"foreignId"`
	ForeignMail   string         `bson:"foreign_mail" json:"foreignMail"`
	ForeignName   string         `bson:"foreign_name" json:"foreignName"`
	ForeignAvatar *bson.ObjectId `bson:"foreign_avatar" json:"-"`
	ForeignToken  *oauth2.Token  `bson:"foreign_token" json:"-"`
	LinkedAt      time.Time      `bson:"linked_at" json:"linkedAt"`
}

type PartialUser struct {
	Name           *string      `json:"username,omitempty"`
	Firstname      *string      `json:"firstName,omitempty"`
	Lastname       *string      `json:"lastName,omitempty"`
	Mail           *string      `json:"mail,omitempty"`
	PhoneNumber    *PhoneNumber `json:"phoneNumber,omitempty"`
	ProfilePicture *string      `json:"profilePicture"`
}

type UserPublic struct {
	Id        bson.ObjectId `bson:"_id" json:"id"`
	Name      string        `bson:"name" json:"username"`
	Firstname string        `bson:"firstname" json:"firstName,omitempty"`
	Lastname  string        `bson:"lastname" json:"lastName,omitempty"`

	// This field contain all the user's friends id
	//   This is a private field (aka for mongo only)
	//   It should never be serialized
	FriendsIds []bson.ObjectId `bson:"friends_ids" json:"friendsId"`

	// This field contain all the publication of the user
	PublicationIds []bson.ObjectId `bson:"publication_ids" json:"publicationsId"`

	ProfilePicture   *bson.ObjectId `bson:"profile_picture" json:"profilePicture"`
	RegistrationDate time.Time      `bson:"registration_date" json:"registrationDate"`
}

type LightUserPublic struct {
	Id             bson.ObjectId  `bson:"_id" json:"id"`
	Name           string         `bson:"name" json:"username"`
	ProfilePicture *bson.ObjectId `bson:"profile_picture" json:"profilePicture"`
}

type UserMail struct {
	Mail string `json:"mail"`
}

type UserFirstname struct {
	Firstname string `json:"firstName"`
}

type UserLastname struct {
	Lastname string `json:"lastName"`
}

type UserName struct {
	UserFirstname
	UserLastname
}

type BundleUserData struct {
	Id          bson.ObjectId `bson:"_id" json:"id"`
	UserId      bson.ObjectId `bson:"user_id" json:"-"`
	RequestedAt time.Time     `bson:"requested_at" json:"requestedAt"`
	CreatedAt   *time.Time    `bson:"created_at" json:"createdAt"`
	File        *string       `bson:"file" json:"file"`
	Expiry      *time.Time    `bson:"expiry" json:"expiry"`
	Done        bool          `bson:"done" json:"done"`
}
