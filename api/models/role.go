package models

import "github.com/globalsign/mgo/bson"

type Role struct {
	Id   bson.ObjectId `bson:"_id" json:"-"`
	Name string        `bson:"name" json:"name"`
}
