package middlewares

import (
	"net/http"

	"api/req"
	"api/tools"
)

func HasPathParam(params []string) tools.MdFunc {
	return func(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
		for _, param := range params {
			if val, ok := r.Params[param]; !ok || val == "" {
				w.SendBadRequest(nil, http.StatusText(http.StatusBadRequest)+" "+param+" is required")
				return
			}
		}

		next(s, w, r)
	}
}
