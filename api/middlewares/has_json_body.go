package middlewares

import (
	"api/req"
	"api/tools"
)

func HasJsonBody(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	content := r.Header().Get("Content-Type")

	if content != "application/json" {
		w.SendNotAcceptable(nil)
		return
	}

	next(s, w, r)
	return
}
