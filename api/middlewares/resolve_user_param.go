package middlewares

import (
	"github.com/globalsign/mgo"

	"api/repository"
	"api/req"
	"api/tools"
)

func ResolveUserParam(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	userId := r.Params["userId"]

	if userId == "" {
		w.SendNotFound(nil, "Userid", userId, "not found")
		return
	}
	if userId == "me" {
		if s.User == nil {
			w.SendForbidden(nil, "You must be logged in to use /me")
			return
		}
		userId = s.User.Id.Hex()
	}

	user, err := repository.UsersRepository.FindByIdOrUsername(userId)
	if err != nil && err != mgo.ErrNotFound {
		w.SendInternalError(nil, "Unknown error occured")
		return
	} else if user == nil {
		w.SendNotFound(nil, "Userid", userId, "not found")
		return
	}

	r.SolvedParams["user"] = user
	r.SolvedParams["target_user"] = user
	next(s, w, r)
}
