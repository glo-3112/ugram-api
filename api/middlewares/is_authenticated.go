package middlewares

import (
	"net/http"
	"time"

	uuid "github.com/satori/go.uuid"

	"api/repository"
	"api/req"
	"api/security/authentication"
	"api/tools"

	securityConst "api/constants/security"
	security "api/security/jwt"
)

func MustBeAuthenticated(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	token, err := r.Cookie(securityConst.TokenCookieName)
	if err != nil || token.Value == "" {
		authentication.RemoveAuthentication(w)
		if err == http.ErrNoCookie {
			w.SendUnauthorized(nil, "You must be authenticated")
		} else {
			w.SendUnauthorized(err, "Bad token cookie")
		}
		return
	}

	_, err = r.Cookie(securityConst.AuthenticationCookieName)
	if err != nil {
		authentication.RemoveAuthentication(w)
		if err == http.ErrNoCookie {
			w.SendUnauthorized(nil, "You must be authenticated")
		} else {
			w.SendBadRequest(err, "Bad authentication cookie")
		}
		return
	}

	claims, err := security.ParseJwt(token.Value)
	if err != nil {
		authentication.RemoveAuthentication(w)
		w.SendBadRequest(err, "Bad authentication token")
		return
	}

	uid, err := uuid.FromString(claims.ID)
	if claims.Issuer != securityConst.JWTIssuer || err != nil {
		authentication.RemoveAuthentication(w)
		w.SendBadRequest(nil, "Bad authentication token 2")
		return
	}

	if claims.Expiry.Time().Unix() < time.Now().Unix() {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(nil, "Authentication expired")
		return
	}

	tokenInfo, err := repository.TokensRepository.FindByUUID(&uid)
	if err != nil {
		authentication.RemoveAuthentication(w)
		w.SendInternalError(err, "An unknown error occurred")
		return
	} else if tokenInfo == nil {
		authentication.RemoveAuthentication(w)
		w.SendInternalError(nil, "The given token has been revoked or has expired")
	}

	user, err := repository.UsersRepository.FindByObjectId(tokenInfo.UserId)
	if err != nil {
		authentication.RemoveAuthentication(w)
		next(s, w, r)
		return
	}

	if user == nil {
		authentication.RemoveAuthentication(w)
		next(s, w, r)
		return
	}

	if claims.IssuedAt.Time().Unix() != tokenInfo.IssuedAt.Unix() {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(nil, "The given token has expired")
		return
	}

	if claims.UserId != tokenInfo.UserId {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(nil, "Token error")
		return
	}

	s.Token = tokenInfo
	s.User = user
	next(s, w, r)
	return
}

func ResolveAuthentication(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	token, err := r.Cookie(securityConst.TokenCookieName)
	if err != nil || token.Value == "" {
		next(s, w, r)
		return
	}

	_, err = r.Cookie(securityConst.AuthenticationCookieName)
	if err != nil {
		next(s, w, r)
		return
	}

	claims, err := security.ParseJwt(token.Value)
	if err != nil {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(err, "Bad authentication token")
		return
	}

	uid, err := uuid.FromString(claims.ID)
	if err != nil {
		authentication.RemoveAuthentication(w)
		w.SendInternalError(err, "An unknown error occurred")
		return
	} else if claims.Issuer != securityConst.JWTIssuer {
		authentication.RemoveAuthentication(w)
		w.SendBadRequest(err, "Bad authentication token")
		return
	}

	if claims.Expiry.Time().Unix() < time.Now().Unix() {
		next(s, w, r)
		return
	}

	tokenInfo, err := repository.TokensRepository.FindByUUID(&uid)
	if err != nil {
		next(s, w, r)
		return
	}

	user, err := repository.UsersRepository.FindByObjectId(tokenInfo.UserId)
	if err != nil {
		authentication.RemoveAuthentication(w)
		next(s, w, r)
		return
	}

	if user == nil {
		authentication.RemoveAuthentication(w)
		next(s, w, r)
		return
	}

	if claims.IssuedAt.Time().Unix() != tokenInfo.IssuedAt.Unix() {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(nil, "The given token has expired")
		return
	}

	if claims.UserId != tokenInfo.UserId {
		authentication.RemoveAuthentication(w)
		w.SendUnauthorized(nil, "Token error")
		return
	}

	s.Token = tokenInfo
	s.User = user
	next(s, w, r)
	return
}
