package middlewares

import (
	"net/http"

	"api/req"
	"api/tools"
)

func CheckMethod(allowedMethods ...string) tools.MdFunc {
	return func(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
		for _, test := range allowedMethods {
			if r.Method() == test {
				next(s, w, r)
				return
			}
		}
		w.SendResponse(http.StatusMethodNotAllowed, http.StatusText(http.StatusMethodNotAllowed), nil)
		return
	}
}
