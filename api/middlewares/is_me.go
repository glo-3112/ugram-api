package middlewares

import (
	"api/req"
	"api/tools"
)

// Middleware - ResolveMe
//
// Effect: Replace the path param "userId" by the authenticated user.Id
func ResolveMe(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	if r.Params["userId"] == "me" {
		MustBeAuthenticated(s, w, r, func(s2 *req.Session, w2 *req.Writer, r2 *req.Reader) {
			r.Params["userId"] = s.User.Id.Hex()
			next(s2, w2, r2)
		})
		return
	}
	next(s, w, r)
}

func ShouldBeMe(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	if r.Params["userId"] != "me" {
		if s.User == nil {
			w.SendUnauthorized(nil, "You should be authenticated to do this")
			return
		} else if r.Params["userId"] != s.User.Id.Hex() {
			w.SendForbidden(nil, "Not allowed")
			return
		}
	}
	r.SolvedParams["user"] = s.User
	next(s, w, r)
}
