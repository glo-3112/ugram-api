package middlewares

import (
	"api/models"
	"api/req"
	"api/tools"
)

func PrivacyProtected(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {

	if r.SolvedParams["target_user"] == nil {
		w.SendUnauthorized(nil, "This profile is not public")
		return
	}
	targetUser := r.SolvedParams["target_user"].(*models.User)

	if s.User != nil && s.User.Id == targetUser.Id {
		goto friendship
	}

	if !targetUser.Public {
		if s.User == nil {
			w.SendUnauthorized(nil, "This profile is not public")
			return
		}

		for _, v := range targetUser.FriendsIds {
			if v == s.User.Id {
				goto friendship
			}
		}
		w.SendUnauthorized(nil, "This profile is not public")
		return
	}

friendship:
	next(s, w, r)
}
