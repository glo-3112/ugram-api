package middlewares

import (
	"fmt"
	"strconv"
	"time"

	constants "api/constants/security"
	"api/models"
	"api/repository"
	"api/req"
	"api/tools"
)

var rateLimiterAnonymous = make(map[string]*models.RateLimit, 0)

func makeLimit(authed bool) *models.RateLimit {
	ret := &models.RateLimit{
		Limit: constants.RateLimitAnonymousLimit,
		Reset: time.Now().Add(constants.RateLimitDelay),
	}
	if authed {
		ret.Limit = constants.RateLimitAuthedLimit
	}
	ret.Remaining = ret.Limit
	return ret
}

func RateLimiterMiddleware(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	var limit *models.RateLimit

	if s.User != nil {
		limit = s.User.RateLimit
		if limit == nil {
			limit = makeLimit(true)
			s.User.RateLimit = limit
		}
	} else {
		foreignIp := r.Req.Header.Get("X-Real-IP")
		var ok bool
		if limit, ok = rateLimiterAnonymous[foreignIp]; !ok {
			limit = makeLimit(false)
			rateLimiterAnonymous[foreignIp] = limit
		}
	}

	limit.Remaining--
	if limit.Reset.Unix() < time.Now().Unix() {
		if s.User == nil {
			limit.Remaining = constants.RateLimitAnonymousLimit
		} else {
			limit.Remaining = constants.RateLimitAuthedLimit
		}
		limit.Reset = time.Now().Add(constants.RateLimitDelay)
	}
	if limit.Remaining < 0 {
		limit.Remaining = 0
	}

	w.Header().Set("X-RateLimit", strconv.FormatInt(limit.Limit, 10))
	w.Header().Set("X-RateLimit-Remaining", strconv.FormatInt(limit.Remaining, 10))
	w.Header().Set("X-RateLimit-Reset", strconv.FormatInt(limit.Reset.Unix(), 10))

	if s.User != nil {
		repository.UsersRepository.UpdateRateLimit(s.User)
	}
	if limit.Reset.Unix() > time.Now().Unix() && limit.Remaining <= 0 {
		if s.User == nil {
			w.SendForbidden(fmt.Errorf("Anonymous request rates reached"), "You've reach the API rate limit. Authenticate or create an account, the rates will raise !")
		} else {
			w.SendForbidden(fmt.Errorf("Anonymous request rates reached"), "You've reach the API rate limit. Try again later")
		}
		return
	}
	next(s, w, r)
}
