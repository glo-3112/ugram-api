package middlewares

import (
	"api/repository"
	"api/req"
	"api/tools"
)

func ShouldOwnPublication(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	user := s.User
	if user == nil {
		w.SendForbidden(nil, "You must be authenticated to do this action")
		return
	}

	publicationId := r.Params["publicationId"]
	if publicationId == "" {
		w.SendBadRequest(nil)
		return
	}

	publication, err := repository.PublicationsRepository.FindByAuthorAndId(user.Id.Hex(), publicationId)
	if err != nil {
		w.SendInternalError(err, "An uknown error occured")
		return
	} else if publication == nil {
		w.SendNotFound(nil, "Publication", publicationId, "not found")
		return
	}

	r.SolvedParams["publication"] = publication
	next(s, w, r)
}

func ResolvePublication(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
	publicationId := r.Params["publicationId"]
	if publicationId == "" {
		w.SendBadRequest(nil)
		return
	}

	publication, err := repository.PublicationsRepository.FindById(publicationId)
	if err != nil {
		w.SendInternalError(err, "An unknown error occured")
		return
	} else if publication == nil {
		w.SendNotFound(nil, "Publication", publicationId, "not found")
		return
	}

	author, err := repository.UsersRepository.FindByObjectId(publication.Author)
	if err != nil {
		w.SendInternalError(err, "An unknown error occurred")
		return
	}

	r.SolvedParams["target_user"] = author
	r.SolvedParams["publication"] = publication
	next(s, w, r)
}
