package middlewares

import (
	"api/req"
	"api/tools"
)

func HasRole(roleNames ...string) tools.MdFunc {
	return func(s *req.Session, w *req.Writer, r *req.Reader, next tools.CtrlFunc) {
		if s.Token == nil || s.User == nil {
			w.SendForbidden(nil)
			return
		}
		userRoles := s.User.Roles

		for _, name := range roleNames {
			for _, role := range userRoles {
				if role == name {
					next(s, w, r)
					return
				}
			}
		}
		w.SendForbidden(nil, "Role not high enough")
	}
}
