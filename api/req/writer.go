package req

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"runtime/debug"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type Writer struct {
	http.ResponseWriter
	req         *http.Request
	RequestTime time.Time
}

func NewWriter(w http.ResponseWriter, r *http.Request) *Writer {
	return &Writer{
		ResponseWriter: w,
		req:            r,
		RequestTime:    time.Now(),
	}
}

// An helper that automatically build the response body with the provided arguments.
func (w *Writer) SendResponse(status int, message string, payload interface{}) *Writer {
	w.Header().Set("Content-Type", "application/json")

	w.WriteHeader(status)
	resp := &ApiResponse{Code: status, Message: message, Data: payload, ExecutionTime: fmt.Sprint(time.Since(w.RequestTime))}

	if status >= 400 {
		logrus.Error(status, message, payload)
	}

	r, err := json.Marshal(resp)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		_, _ = w.Write([]byte(err.Error()))
	}
	_, _ = w.Write(r)
	return w
}

/*********************************
 * 2XX Request response helpers
 *********************************/
// An helper that send automatically a 200 Ok response
func (w *Writer) SendOk(data interface{}, message ...string) *Writer {
	if len(message) > 0 {
		w.SendResponse(http.StatusOK, strings.Join(message, " "), data)
	} else {
		w.SendResponse(http.StatusOK, http.StatusText(http.StatusOK), data)
	}
	return w
}

// An helper that send automatically a 201 Created response
func (w *Writer) SendCreated(data interface{}, message ...string) *Writer {
	if len(message) > 0 {
		w.SendResponse(http.StatusCreated, strings.Join(message, " "), data)
	} else {
		w.SendResponse(http.StatusCreated, http.StatusText(http.StatusCreated), data)
	}
	return w
}

// An helper that send automatically a 204 No content response
func (w *Writer) SendNoContent(message ...string) *Writer {
	if len(message) > 0 {
		w.SendResponse(http.StatusOK, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusOK, http.StatusText(http.StatusNoContent), nil)
	}
	return w
}

/*********************************
 * 3XX Request response helpers
 *********************************/
func (w *Writer) RedirectPermanent(url *url.URL) *Writer {
	http.Redirect(w.ResponseWriter, w.req, url.String(), http.StatusPermanentRedirect)
	return w
}

func (w *Writer) RedirectTemporary(url *url.URL) *Writer {
	http.Redirect(w.ResponseWriter, w.req, url.String(), http.StatusTemporaryRedirect)
	return w
}

func (w *Writer) RedirectQueryPermanent(url *url.URL, queries ...string) *Writer {
	q := url.Query()
	if len(queries)%2 == 0 {
		for i := 0; i < len(queries); i += 2 {
			q.Add(queries[i], queries[i+1])
		}
	}
	url.RawQuery = q.Encode()
	http.Redirect(w.ResponseWriter, w.req, url.String(), http.StatusPermanentRedirect)
	return w
}

func (w *Writer) RedirectQueryTemporary(url *url.URL, queries ...string) *Writer {
	q := url.Query()
	if len(queries)%2 == 0 {
		for i := 0; i < len(queries); i += 2 {
			q.Add(queries[i], queries[i+1])
		}
	}
	url.RawQuery = q.Encode()
	http.Redirect(w.ResponseWriter, w.req, url.String(), http.StatusTemporaryRedirect)
	return w
}

/*********************************
 * 4XX Request response helpers
 *********************************/
// An helper that send automatically a 400 Bad Request response
func (w *Writer) SendBadRequest(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusBadRequest, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusBadRequest, http.StatusText(http.StatusBadRequest), nil)
	}
	return w
}

// An helper that send automatically a 401 Unauthorized response
func (w *Writer) SendUnauthorized(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusUnauthorized, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized), nil)
	}
	return w
}

// An helper that send automatically a 403 Forbidden response
func (w *Writer) SendForbidden(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusForbidden, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusForbidden, http.StatusText(http.StatusForbidden), nil)
	}
	return w
}

// An helper that send automatically a 404 Not Found response
func (w *Writer) SendNotFound(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotFound, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusNotFound, http.StatusText(http.StatusNotFound), nil)
	}
	return w
}

// An helper that send automatically a 406 Not Acceptable response
func (w *Writer) SendNotAcceptable(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotAcceptable, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusNotAcceptable, http.StatusText(http.StatusNotAcceptable), nil)
	}
	return w
}

// An helper that send automatically a 409 Conflict response
func (w *Writer) SendConflict(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusConflict, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusConflict, http.StatusText(http.StatusConflict), nil)
	}
	return w
}

// An helper that send automatically a 418 I'm a Teapot response
func (w *Writer) SendTeapot() *Writer {
	w.SendResponse(http.StatusTeapot, http.StatusText(http.StatusTeapot), nil)
	return w
}

/*********************************
 * 5XX Request response helpers
 *********************************/

// An helper that send automatically a 501 Not Implemented response
func (w *Writer) SendInternalError(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusInternalServerError, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError), nil)
	}
	return w
}

// An helper that send automatically a 501 Not Implemented response
func (w *Writer) SendNotImplemented(err error, message ...string) *Writer {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotImplemented, strings.Join(message, " "), nil)
	} else {
		w.SendResponse(http.StatusNotImplemented, http.StatusText(http.StatusNotImplemented), nil)
	}
	return w
}

// Cookies helper
func (w *Writer) UnsetCookie(name string) *Writer {
	ckie, err := w.req.Cookie(name)
	if err != nil {
		return w
	}

	ckie.Expires = time.Now()
	ckie.Value = ""
	ckie.MaxAge = 0

	http.SetCookie(w, ckie)
	return w
}

func (w *Writer) UnsetCookies(names ...string) *Writer {
	for _, name := range names {
		w.UnsetCookie(name)
	}
	return w
}

func (w *Writer) Static() *staticWriter {
	return newStaticWriter(w)
}
