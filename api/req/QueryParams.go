package req

import (
	"fmt"
	"net/url"
	"reflect"
	"strconv"
	"time"
)

type QueryParam struct {
	url.Values
}

var ErrNotFound = fmt.Errorf("field not found")
var ErrNumberSyntax = fmt.Errorf("wrong number syntax")
var ErrNumberRange = fmt.Errorf("number too small or too big")

func (q *QueryParam) parseInt(stringValue string, number interface{}) error {
	nb, err := strconv.ParseInt(stringValue, 10, 32)
	if err == strconv.ErrSyntax {
		return ErrNumberSyntax
	} else if err == strconv.ErrRange {
		return ErrNumberRange
	}

	value := reflect.ValueOf(number)
	value.Addr().SetInt(nb)
	return nil
}

func (q *QueryParam) GetIntIndex(field string, index int) *int {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}
	if len(fieldValues) <= index {
		return nil
	}

	ret := 0
	_ = q.parseInt(fieldValues[index], &ret)
	return &ret
}

func (q *QueryParam) GetInt(field string) *int {
	fieldValue := q.Get(field)
	if fieldValue == "" {
		return nil
	}

	ret := 0
	_ = q.parseInt(field, &ret)
	return &ret
}

func (q *QueryParam) GetAllInt(field string) []int {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}

	ret := make([]int, len(fieldValues))
	i := 0
	for _, v := range fieldValues {
		err := q.parseInt(v, &ret[i])
		if err == nil {
			i++
		}
	}
	return ret
}

func (q *QueryParam) GetString(field string) *string {
	fieldValue, ok := q.Values[field]
	if !ok {
		return nil
	}
	return &fieldValue[0]
}

func (q *QueryParam) GetStringIndex(field string, index int) *string {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}
	if len(fieldValues) <= index {
		return nil
	}
	return &fieldValues[index]
}

func (q *QueryParam) GetAllStrings(field string) []string {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}
	return fieldValues
}

func (q *QueryParam) GetTime(field string) *time.Time {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}
	t, err := time.Parse(time.RFC3339, fieldValues[0])
	if err != nil {
		return nil
	}
	return &t
}

func (q *QueryParam) GetTimeIndex(field string, index int) *time.Time {
	fieldValues, ok := q.Values[field]
	if !ok {
		return nil
	}
	if len(fieldValues) <= index {
		return nil
	}

	t, err := time.Parse(time.RFC3339, fieldValues[index])
	if err != nil {
		return nil
	}
	return &t
}

func (q *QueryParam) GetAllTime(field string) []time.Time {
	fieldValues, ok := q.Values[field]
	if !ok {
		return make([]time.Time, 0)
	}

	t := make([]time.Time, 0)
	i := 0
	var err error
	for _, v := range fieldValues {
		t[i], err = time.Parse(time.RFC3339, v)
		if err == nil {
			i++
		}
	}
	return t
}
