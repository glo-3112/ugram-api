package req

import (
	"fmt"
	"io"
	"net/http"
	"runtime/debug"
	"strings"
	"time"

	"github.com/sirupsen/logrus"
)

type FileInfos struct {
	Name string
}

type staticWriter struct {
	*Writer
	file io.Reader
}

func newStaticWriter(w *Writer) *staticWriter {
	return &staticWriter{
		Writer: w,
	}
}

func (w *staticWriter) SendResponse(status int, message string) {
	w.Header().Set("X-Server-Execution-time", fmt.Sprint(time.Since(w.RequestTime)))
	w.Header().Set("X-Server-Status-Message", message)

	if status >= 400 {
		logrus.Error(status, message)
	}
	w.WriteHeader(status)
	if w.file != nil {
		_, _ = io.Copy(w.Writer.ResponseWriter, w.file)
	}
	_, _ = w.Writer.Write(make([]byte, 0))
}

func (w *staticWriter) SetFile(file io.Reader, fileInfos *FileInfos) *staticWriter {
	if fileInfos != nil {
		if fileInfos.Name != "" {
			w.Header().Set("Content-Disposition", "attachment; " + fileInfos.Name)
		}
	}
	w.file = file
	return w
}

func (w *staticWriter) SetContentType(contentType string) *staticWriter {
	w.Writer.Header().Set("Content-Type", contentType)
	return w
}

/*********************************
 * 2XX Request response helpers
 *********************************/
// An helper that send automatically a 200 Ok response
func (w *staticWriter) SendOk(file io.Reader, message ...string) {
	w.file = file
	if len(message) > 0 {
		w.SendResponse(http.StatusOK, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusOK, http.StatusText(http.StatusOK))
	}
}

// An helper that send automatically a 201 Created response
func (w *staticWriter) SendCreated(file io.Reader, message ...string) {
	w.file = file
	if len(message) > 0 {
		w.SendResponse(http.StatusCreated, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusCreated, http.StatusText(http.StatusCreated))
	}
}

// An helper that send automatically a 204 No content response
func (w *staticWriter) SendNoContent(message ...string) {
	if len(message) > 0 {
		w.SendResponse(http.StatusOK, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusOK, http.StatusText(http.StatusNoContent))
	}
}

/*********************************
 * 4XX Request response helpers
 *********************************/
// An helper that send automatically a 400 Bad Request response
func (w *staticWriter) SendBadRequest(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusBadRequest, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusBadRequest, http.StatusText(http.StatusBadRequest))
	}
}

// An helper that send automatically a 401 Unauthorized response
func (w *staticWriter) SendUnauthorized(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusUnauthorized, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusUnauthorized, http.StatusText(http.StatusUnauthorized))
	}
}

// An helper that send automatically a 403 Forbidden response
func (w *staticWriter) SendForbidden(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusForbidden, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusForbidden, http.StatusText(http.StatusForbidden))
	}
}

// An helper that send automatically a 404 Not Found response
func (w *staticWriter) SendNotFound(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotFound, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusNotFound, http.StatusText(http.StatusNotFound))
	}
}

// An helper that send automatically a 406 Not Acceptable response
func (w *staticWriter) SendNotAcceptable(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotAcceptable, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusNotAcceptable, http.StatusText(http.StatusNotAcceptable))
	}
}

// An helper that send automatically a 409 Conflict response
func (w *staticWriter) SendConflict(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotAcceptable, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusNotAcceptable, http.StatusText(http.StatusNotAcceptable))
	}
}

// An helper that send automatically a 418 I'm a Teapot response
func (w *staticWriter) SendTeapot() {
	w.SendResponse(http.StatusTeapot, http.StatusText(http.StatusTeapot))
}

/*********************************
 * 5XX Request response helpers
 *********************************/

// An helper that send automatically a 501 Not Implemented response
func (w *staticWriter) SendInternalError(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusInternalServerError, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusInternalServerError, http.StatusText(http.StatusInternalServerError))
	}
}

// An helper that send automatically a 501 Not Implemented response
func (w *staticWriter) SendNotImplemented(err error, message ...string) {
	if err != nil {
		debug.PrintStack()
		logrus.Error(err, "|", strings.Join(message, " "))
	}
	if len(message) > 0 {
		w.SendResponse(http.StatusNotImplemented, strings.Join(message, " "))
	} else {
		w.SendResponse(http.StatusNotImplemented, http.StatusText(http.StatusNotImplemented))
	}
}
