package req

import (
	"net/http"
	"time"

	"api/models"
)

type SessionInfo struct {
	Ip              string
	Host            string
	ForwardedFor    string
	ForwardedHost   string
	ForwardedServer string
	Country         string
	City            string
}

type Session struct {
	Token        *models.TokenInfo
	User         *models.User
	RequestStart time.Time
	Infos        *SessionInfo
}

func NewSession(r *http.Request) *Session {
	return &Session{
		Infos: &SessionInfo{
			Ip:              r.Header.Get("X-Real-IP"),
			Host:            r.Header.Get("X-Host"),
			ForwardedFor:    r.Header.Get("X-Forwarded-For"),
			ForwardedHost:   r.Header.Get("X-Forwarded-Host"),
			ForwardedServer: r.Header.Get("X-Forwarded-Server"),
			Country:         r.Header.Get("X-Location-Country"),
			City:            r.Header.Get("X-Location-City"),
		},
	}
}
