package req

type ApiResponse struct {
	Code          int         `json:"code"`
	Message       string      `json:"message"`
	Data          interface{} `json:"data"`
	ExecutionTime string      `json:"executionTime,omitempty"`
}
