package validators

import (
	"errors"

	"github.com/nyaruka/phonenumbers"

	apiErr "api/constants/errors"
	"api/models"
)

var (
	ErrPhoneNumberFormat         = errors.New(apiErr.ErrBadPhoneFormat)
	ErrPhoneNumberTooShort       = errors.New(apiErr.ErrPhoneTooShort)
	ErrPhoneNumberTooLong        = errors.New(apiErr.ErrPhoneTooLong)
	ErrPhoneNumberBadCountryCode = errors.New(apiErr.ErrPhoneBadCountryCode)
	ErrPhoneNumberInvalidLength  = errors.New(apiErr.ErrPhoneInvalidLength)
)

func IsPhoneValid(phone *models.PhoneNumber) error {

	number, err := phonenumbers.Parse(phone.Number, phone.ISOCode)
	if err != nil {
		return ErrPhoneNumberFormat
	}

	international := phonenumbers.Format(number, phonenumbers.E164)

	switch phonenumbers.IsPossibleNumberWithReason(number) {
	case phonenumbers.IS_POSSIBLE:
	case phonenumbers.IS_POSSIBLE_LOCAL_ONLY:
		break
	case phonenumbers.TOO_LONG:
		return ErrPhoneNumberTooLong
	case phonenumbers.TOO_SHORT:
		return ErrPhoneNumberTooShort
	case phonenumbers.INVALID_COUNTRY_CODE:
		return ErrPhoneNumberBadCountryCode
	case phonenumbers.INVALID_LENGTH:
		return ErrPhoneNumberInvalidLength
	default:
		return ErrPhoneNumberFormat
	}

	phone.International = international
	return nil
}
