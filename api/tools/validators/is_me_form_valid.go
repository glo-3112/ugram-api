package validators

import (
	"github.com/globalsign/mgo/bson"

	"api/constants"
	"api/models"
	"api/tools/filemanager"
)

func IsMeFormValid(me *models.User, form *models.PartialUser) (error, string) {
	if form.Name != nil && *form.Name != "" {
		me.Name = *form.Name
	}
	if form.Firstname != nil && *form.Firstname != "" {
		me.Firstname = *form.Firstname
	}
	if form.Lastname != nil && *form.Lastname != "" {
		me.Lastname = *form.Lastname
	}
	if form.Mail != nil && *form.Mail != "" {
		if err := IsMailValid(*form.Mail); err != nil {
			return nil, err.Error()
		}
		me.Mail = *form.Mail
	}
	if form.ProfilePicture != nil && *form.ProfilePicture != "" {
		picture := bson.NewObjectId()
		err := filemanager.SaveBase64Picture(*form.ProfilePicture, picture.Hex())

		if err != nil {
			return err, "Can't save profile picture"
		}
		if me.ProfilePicture.Hex() != constants.DefaultProfilePicture {
			_ = filemanager.DeletePicture(me.ProfilePicture.Hex())
		}
		me.ProfilePicture = picture
	}
	if form.PhoneNumber != nil {
		if err := IsPhoneValid(form.PhoneNumber); err != nil {
			return nil, err.Error()
		}
		me.PhoneNumber = form.PhoneNumber
	}
	return nil, ""
}
