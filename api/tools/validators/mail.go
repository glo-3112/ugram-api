package validators

import (
	"errors"
	"regexp"

	apiErr "api/constants/errors"
)

func IsMailValid(mail string) error {

	emailRegexp := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

	if !emailRegexp.MatchString(mail) {
		return errors.New(apiErr.ErrBadMailFormat)
	}

	return nil
}
