package filemanager

import (
	"os"
	"path"
	"strings"

	"api/constants"
)

func CreateZipFileArchive(username string, zipPathname ...string) (*os.File, error) {
	pathname := path.Join("packed_archives", username, path.Join(zipPathname...))
	err := EnsureDirectory(path.Dir(pathname))
	if err != nil {
		return nil, err
	}
	pathname = path.Join(constants.GetStaticDir(), pathname)

	if !strings.HasSuffix(pathname, ".zip") {
		pathname += ".zip"
	}

	return os.Create(pathname)
}

func OpenZipFileArchive(username string, zipPathname ...string) (*os.File, error) {
	pathname := path.Join(constants.GetStaticDir(), "packed_archives", username, path.Join(zipPathname...))
	if !strings.HasSuffix(pathname, ".zip") {
		pathname += ".zip"
	}

	return os.Open(pathname)
}

func StatZipFileArchive(username string, zipPathname ...string) (os.FileInfo, error) {
	pathname := path.Join(constants.GetStaticDir(), "packed_archives", username, path.Join(zipPathname...))
	if !strings.HasSuffix(pathname, ".zip") {
		pathname += ".zip"
	}

	return os.Stat(pathname)
}

func RemoveZipFileArchive(username string, zipPathname ...string) error {
	pathname := path.Join(constants.GetStaticDir(), "packed_archives", username, path.Join(zipPathname...))
	if !strings.HasSuffix(pathname, ".zip") {
		pathname += ".zip"
	}

	return os.Remove(pathname)
}
