package filemanager

import (
	"os"
	"path"

	"github.com/globalsign/mgo/bson"

	"api/constants"
)

func DeletePicture(ids ...string) error {
	var err error = nil

	for _, id := range ids {
		e := os.Remove(path.Join(constants.GetStaticDir(), "small", id+".png"))
		if e != nil && err == nil {
			err = e
		}
		e = os.Remove(path.Join(constants.GetStaticDir(), "medium", id+".png"))
		if e != nil && err == nil {
			err = e
		}
		e = os.Remove(path.Join(constants.GetStaticDir(), "original", id+".png"))
		if e != nil && err == nil {
			err = e
		}
	}
	return err
}

func DeletePicturesByObjectId(oids ...bson.ObjectId) error {
	ids := make([]string, len(oids))

	for i, id := range oids {
		ids[i] = id.Hex()
	}

	return DeletePicture(ids...)
}
