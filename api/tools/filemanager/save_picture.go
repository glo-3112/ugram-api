package filemanager

import (
	"encoding/base64"
	"errors"
	"fmt"
	"image"
	_ "image/gif"
	_ "image/jpeg"
	"image/png"
	"log"
	"math"
	"mime/multipart"
	"os"
	"path"
	"strings"

	_ "golang.org/x/image/bmp"
	_ "golang.org/x/image/tiff"
	_ "golang.org/x/image/webp"

	"api/constants"
)

func init() {
	stat, _ := os.Stat(path.Join(constants.GetStaticDir(), "small"))
	if stat == nil {
		if err := os.MkdirAll(path.Join(constants.GetStaticDir(), "small"), os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}
	stat, _ = os.Stat(path.Join(constants.GetStaticDir(), "medium"))
	if stat == nil {
		if err := os.Mkdir(path.Join(constants.GetStaticDir(), "medium"), os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}
	stat, _ = os.Stat(path.Join(constants.GetStaticDir(), "original"))
	if stat == nil {
		if err := os.Mkdir(path.Join(constants.GetStaticDir(), "original"), os.ModePerm); err != nil {
			log.Fatal(err)
		}
	}
}

func saveResizedPicture(rgba image.Image, pathname string, width, height int, c chan bool, err *error) {
	rect := rgba.Bounds()

	if width == -1 {
		width = rect.Max.X
	}
	if height == -1 {
		height = rect.Max.Y
	}
	newImg := image.NewRGBA(image.Rectangle{Max: image.Point{X: width, Y: height}})
	ratioX := float64(rect.Max.X) / float64(width)
	ratioY := float64(rect.Max.Y) / float64(height)
	ratio := math.Min(ratioY, ratioX)

	originalY := float64(rect.Max.Y)/2 - (float64(height)*ratio)/2
	for y := 0; y < height; y++ {
		originalX := float64(rect.Max.X)/2 - (float64(width)*ratio)/2
		for x := 0; x < width; x++ {

			newImg.Set(x, y, rgba.At(int(originalX), int(originalY)))

			originalX = originalX + ratio
		}
		originalY = originalY + ratio
	}

	file, e := os.Create(path.Join(constants.GetStaticDir(), pathname+".png"))
	if e != nil {
		fmt.Println(e)
		*err = errors.New("Can't save image " + pathname)
		c <- false
		return
	}

	encoder := &png.Encoder{
		CompressionLevel: png.BestSpeed,
	}

	e = encoder.Encode(file, newImg)
	if e != nil {
		_ = file.Close()
		_ = os.Remove(pathname + ".png")
		*err = errors.New("Can't save image " + pathname)
		c <- false
		return
	}

	_ = file.Close()
	c <- true
	return
}

func createAndSavePicture(img *image.Image, filename string) error {
	c := make(chan bool)
	var err error = nil

	go saveResizedPicture(*img, path.Join("small", filename), constants.ImageSmallWidth, constants.ImageSmallHeight, c, &err)
	go saveResizedPicture(*img, path.Join("medium", filename), constants.ImageMediumWidth, constants.ImageMediumHeight, c, &err)
	go saveResizedPicture(*img, path.Join("original", filename), -1, -1, c, &err)

	for i := 0; i < 3; i++ {
		_ = <-c
		if err != nil {
			return err
		}
	}
	return nil
}

func SavePicture(header *multipart.FileHeader, filename string) error {
	file, err := header.Open()

	img, _, err := image.Decode(file)

	if err != nil {
		fmt.Println(err, header)
		return errors.New("Can't save image #1")
	}

	return createAndSavePicture(&img, filename)
}

func SaveBase64Picture(uploadContent string, filename string) error {

	idx := strings.Index(uploadContent, ",")
	if idx < 0 {
		log.Fatal("no comma")
	}
	dec := base64.NewDecoder(base64.StdEncoding, strings.NewReader(uploadContent[idx+1:]))

	img, _, err := image.Decode(dec)
	if err != nil {
		fmt.Println(err)
		return errors.New("Can't save image #1")
	}

	return createAndSavePicture(&img, filename)
}
