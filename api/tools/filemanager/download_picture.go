package filemanager

import (
	"image"
	"net/http"

	"github.com/globalsign/mgo/bson"
)

func DownloadPicture(uri string) (*bson.ObjectId, error) {

	response, err := http.Get(uri)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()

	img, _, err := image.Decode(response.Body)
	if err != nil {
		return nil, err
	}

	oid := bson.NewObjectId()
	err = createAndSavePicture(&img, oid.Hex())
	if err != nil {
		return nil, err
	}

	return &oid, nil
}
