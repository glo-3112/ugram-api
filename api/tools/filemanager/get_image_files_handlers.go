package filemanager

import (
	"os"
	"path"

	"github.com/globalsign/mgo/bson"

	"api/constants"
)

func GetImageFilesHandlers(file bson.ObjectId) (map[string]*os.File, error) {
	ret := map[string]*os.File{
		"original/" + file.Hex() + ".png": nil,
		"small/" + file.Hex() + ".png":    nil,
		"medium/" + file.Hex() + ".png":   nil,
	}

	for file, _ := range ret {
		var err error = nil
		ret[file], err = os.Open(path.Join(constants.GetStaticDir(), file))
		if err != nil {
			return nil, err
		}
	}
	return ret, nil
}
