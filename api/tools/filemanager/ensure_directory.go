package filemanager

import (
	"os"
	"path"

	"api/constants"
)

func EnsureDirectory(dirname ...string) error {
	stat, _ := os.Stat(path.Join(constants.GetStaticDir(), path.Join(dirname...)))
	if stat == nil {
		if err := os.MkdirAll(path.Join(constants.GetStaticDir(), path.Join(dirname...)), os.ModePerm); err != nil {
			return err
		}
	}
	return nil
}
