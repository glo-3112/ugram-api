package tools

import (
	"api/models"
)

func max(a, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Returns a string fragment starting at 'from' until 'to'
// Example:
//      substr("Lorem ipsum", 5, 5) // -> "ipsum"
func substr(source string, from, of int) string {
	srcLen := len(source)
	if from < 0 {
		from += srcLen
		of += srcLen
	}

	of += from
	if of >= srcLen {
		of = srcLen
	}
	return source[from:of]
}

func PrettyfyMatch(source string, match *models.Match, paddingLeft, paddingRight, minRight int) {
	targetSize := paddingLeft + paddingRight

	prefix := ""
	suffix := ""
	str := ""
	if len(match.Raw) > targetSize {
		str = substr(match.Raw, -targetSize, 0)
	} else {
		str = substr(match.Raw, 0, targetSize)
	}

	leftStr := substr(source, match.Index-paddingLeft, paddingLeft)
	rightStr := substr(source, match.Index+len(match.Raw), paddingRight)
	leftLen := len(leftStr)
	rightLen := len(rightStr)
	strLen := len(str)

	if leftLen+strLen+rightLen > targetSize {
		toRemove := (leftLen + strLen + rightLen) - targetSize
		if leftLen > 0 {
			prefix = "... "
			old := leftLen
			leftLen = max(0, leftLen-toRemove)
			leftStr = substr(leftStr, -leftLen, 0)
			toRemove -= old - leftLen
		}
		if rightLen > minRight {
			old := rightLen
			rightLen = max(minRight, rightLen-toRemove)
			rightStr = substr(rightStr, 0, rightLen)
			toRemove -= old - rightLen
		}
		if toRemove > 0 {
			strLen = max(10, strLen-toRemove)
			str = substr(str, -strLen, 0)
			strLen = len(str)
		}
	}

	if rightLen+match.Index+len(match.Raw) < len(source) {
		suffix = " ..."
	}

	match.Pretty = prefix + leftStr + "<q>" + str + "</q>" + rightStr + suffix
	// leftStr := substr(source, match.Index, -leftAnchor)
}

// func PrettyfyMatch(source string, match *models.Match, leftAnchor, rightAnchor, minRight int) {
// 	sourceLength := len(source)
// 	matchLen := len(match.Raw)
// 	matchStart := 0
// 	maxLength := leftAnchor + rightAnchor
//
// 	if matchLen > maxLength {
// 		matchStart += matchLen - maxLength
// 		matchStart += minRight
// 		maxLength += matchLen - maxLength
// 		match.Pretty = "...<q>" + match.Raw[matchStart:maxLength] + "</q>" + source[match.Index + maxLength: match.Index + maxLength + minRight]
// 		return
// 	} else if matchLen == maxLength {
// 		match.Pretty = match.Raw
// 		return
// 	}
//
// 	left := leftAnchor
// 	// For impair imprecision
// 	right := rightAnchor
//
// 	leftStart := match.Index - left
// 	rightStart := match.Index + matchLen
// 	rightEnds := rightStart + right
// 	if leftStart < 0 {
// 		rightEnds += leftStart * -1
// 		leftStart = 0
// 	}
// 	if rightEnds > sourceLength {
// 		if leftStart > 0 {
// 			leftStart -= rightEnds - sourceLength
// 			if leftStart < 0 {
// 				leftStart = 0
// 			}
// 		} else {
// 			rightEnds = sourceLength
// 		}
// 	}
//
// 	suffix := ""
// 	prefix := ""
// 	if matchLen > maxLength {
// 		leftStart = match.Index
// 		rightStart = 0
// 		rightEnds = 0
// 		matchStart += matchLen - maxLength
// 	} else {
// 		if leftStart > 0 {
// 			prefix = "..."
// 			leftStart += 3
// 			if leftStart > match.Index {
// 				leftStart = match.Index
// 			}
// 		}
// 		if rightEnds < sourceLength {
// 			suffix = "..."
// 			rightEnds -= 3
// 			if rightStart > rightEnds {
// 				rightStart = rightEnds
// 			}
// 		}
// 	}
//
// 	match.Pretty = prefix + source[leftStart:match.Index] + "<q>" + match.Raw[matchStart:matchLen] + "</q>"+ source[rightStart:rightEnds] + suffix
// }
