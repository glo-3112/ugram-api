MONGODB_URI="mongodb://$MONGO_INITDB_ROOT_USERNAME:$MONGO_INITDB_ROOT_PASSWORD@mongo/admin"
mongoimport --uri "$MONGODB_URI" --drop --collection users /init/users.bson
mongoimport --uri "$MONGODB_URI" --drop --collection publications /init/publications.bson
