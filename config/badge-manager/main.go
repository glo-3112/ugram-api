package main

import (
	"log"
	"net/http"
	"time"

	"github.com/gorilla/mux"

	"badge-manager/controllers"
)

func main() {
	s := &http.Server{
		Addr:         ":8082",
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	router := mux.NewRouter()
	router.HandleFunc("/badges/api.svg", controllers.BadgeApiController)
	router.HandleFunc("/badges/api/version.svg", controllers.BadgeApiVersionController)
	router.HandleFunc("/badges/api/reference.svg", controllers.BadgeApiRefController)
	router.HandleFunc("/badges/api/documentation.svg", controllers.BadgeApiDocController)
	router.HandleFunc("/badges/mongo-express.svg", controllers.BadgeMongoExpressController)

	router.HandleFunc("/redirect/api", controllers.RedirectApiController)
	router.HandleFunc("/redirect/api/version", controllers.RedirectApiVersionController)
	router.HandleFunc("/redirect/api/reference", controllers.RedirectApiRefController)
	router.HandleFunc("/redirect/api/documentation", controllers.RedirectApiDocController)
	router.HandleFunc("/redirect/mongo-express", controllers.RedirectMongoExpressController)

	s.Handler = router

	log.Fatal(s.ListenAndServe())
}
