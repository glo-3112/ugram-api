package controllers

import (
	"fmt"
	"image/color"
	"net/http"
	"os"

	"badge-manager/constants"
)

func BadgeApiController(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://" + os.Getenv(constants.ApiHost) + "/v1")
	if err != nil || res.StatusCode >= 500 {
		fmt.Println(err)
		RespondSvg(w, "Api", "Down", color.RGBA{
			R: 224,
			G: 93,
			B: 68,
			A: 0,
		}, BadgeSize{35, 30})
		return
	}

	RespondSvg(w, "Api", "http://" + os.Getenv(constants.ApiHost), color.RGBA{
		R: 64,
		G: 207,
		B: 17,
		A: 0,
	}, BadgeSize{35, 190})
	return

}

func BadgeApiVersionController(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://" + os.Getenv(constants.ApiHost) + "/v1")
	if err != nil || res.StatusCode >= 500 {
		fmt.Println(err)
		RespondSvg(w, "Api version", "Unknown", color.RGBA{
			R: 224,
			G: 93,
			B: 68,
			A: 0,
		}, BadgeSize{75, 40})
		return
	}

	RespondSvg(w, "Api version", "/v1 (1.1.5)", color.RGBA{
		R: 64,
		G: 64,
		B: 64,
		A: 0,
	}, BadgeSize{75, 80})
	return

}

func BadgeApiRefController(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://" + os.Getenv(constants.ApiRefHost))
	if err != nil || res.StatusCode >= 500 {
		fmt.Println(err)
		RespondSvg(w, "Api reference", "Down", color.RGBA{
			R: 224,
			G: 93,
			B: 68,
			A: 0,
		}, BadgeSize{90, 40})
		return
	}

	RespondSvg(w, "Api reference", "http://" + os.Getenv(constants.ApiRefHost), color.RGBA{
		R: 64,
		G: 207,
		B: 17,
		A: 0,
	}, BadgeSize{90, 165})
	return
}

func BadgeApiDocController(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://" + os.Getenv(constants.ApiDocHost))
	if err != nil || res.StatusCode >= 500 {
		fmt.Println(err)
		RespondSvg(w, "Api documentation", "Down", color.RGBA{
			R: 224,
			G: 93,
			B: 68,
			A: 0,
		}, BadgeSize{115, 40})
		return
	}

	RespondSvg(w, "Api documentation", "http://" + os.Getenv(constants.ApiDocHost), color.RGBA{
		R: 64,
		G: 207,
		B: 17,
		A: 0,
	}, BadgeSize{115, 165})
	return
}

func BadgeMongoExpressController(w http.ResponseWriter, r *http.Request) {
	res, err := http.Get("http://" + os.Getenv(constants.MongoExpressHost))
	if err != nil || res.StatusCode >= 500 {
		fmt.Println(err)
		RespondSvg(w, "Mongo express", "Down", color.RGBA{
			R: 224,
			G: 93,
			B: 68,
			A: 0,
		}, BadgeSize{100, 40})
		return
	}

	RespondSvg(w, "Mongo express", "http://" + os.Getenv(constants.MongoExpressHost), color.RGBA{
		R: 64,
		G: 207,
		B: 17,
		A: 0,
	}, BadgeSize{100, 165})
	return
}
