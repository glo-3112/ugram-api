package controllers

import (
	"net/http"
	"os"

	"badge-manager/constants"
)

func RedirectApiController(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://" + os.Getenv(constants.ApiHost), http.StatusPermanentRedirect)
}

func RedirectApiVersionController(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://" + os.Getenv(constants.ApiHost) + "/v1", http.StatusPermanentRedirect)
}

func RedirectApiRefController(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://" + os.Getenv(constants.ApiRefHost), http.StatusPermanentRedirect)
}

func RedirectApiDocController(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://" + os.Getenv(constants.ApiDocHost) + "/pkg/api", http.StatusPermanentRedirect)
}

func RedirectMongoExpressController(w http.ResponseWriter, r *http.Request) {
	http.Redirect(w, r, "https://" + os.Getenv(constants.MongoExpressHost), http.StatusPermanentRedirect)
}
