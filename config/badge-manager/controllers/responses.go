package controllers

import (
	"fmt"
	"image/color"
	"net/http"
)

type BadgeSize struct {
	// Left part width
	lW int
	// Right part width
	rW int
}

func ColorToString(c color.RGBA) string {
	r, g, b, a := c.RGBA()
	return "rgba(" + fmt.Sprint(uint8(r)) + ", " + fmt.Sprint(uint8(g)) + ", " + fmt.Sprint(uint8(b)) + ", " + fmt.Sprint(uint8(255-a)) + ")"
}

func RespondSvg(w http.ResponseWriter, label string, value string, color color.RGBA, size ...BadgeSize) {

	bs := BadgeSize{
		lW: 62,
		rW: 54,
	}
	if size != nil {
		bs = size[0]
	}

	svg := "<svg xmlns='http://www.w3.org/2000/svg' width='" + fmt.Sprint(bs.lW+bs.rW) + "' height='20'>"
	svg += "    <linearGradient id='b' x2='0' y2='100%'>"
	svg += "        <stop offset='0' stop-color='#bbb' stop-opacity='.1'/>"
	svg += "        <stop offset='1' stop-opacity='.1'/>"
	svg += "    </linearGradient>"
	svg += "    <mask id='a'>"
	svg += "        <rect width='" + fmt.Sprint(bs.lW+bs.rW) + "' height='20' rx='3' fill='#fff'/>"
	svg += "    </mask>"
	svg += "    <g mask='url(#a)'>"
	svg += "        <path fill='#555' d='M0 0 h" + fmt.Sprint(bs.lW) + " v20 H0 z'/>"
	svg += "        <path fill='" + ColorToString(color) + "' d='M" + fmt.Sprint(bs.lW) + " 0 h" + fmt.Sprint(bs.rW) + " v20 H" + fmt.Sprint(bs.lW) + " z'/>"
	svg += "        <path fill='url(#b)' d='M0 0 h" + fmt.Sprint(bs.lW+bs.rW) + " v20 H0 z'/>"
	svg += "    </g>"
	svg += "    <g fill='#fff' text-anchor='middle'>"
	svg += "        <g font-family='DejaVu Sans,Verdana,Geneva,sans-serif' font-size='11'>"
	svg += "        <text x='" + fmt.Sprint(bs.lW/2) + "' y='15' fill='#010101' fill-opacity='.3'>"
	svg += "	        " + label
	svg += "        </text>"
	svg += "        <text x='" + fmt.Sprint(bs.lW/2) + "' y='14'>"
	svg += "        	" + label
	svg += "        </text>"
	svg += "        <text x='" + fmt.Sprint(bs.lW+bs.rW/2) + "' y='15' fill='#010101' fill-opacity='.3'>"
	svg += "	        " + value
	svg += "        </text>"
	svg += "        <text x='" + fmt.Sprint(bs.lW+bs.rW/2) + "' y='14'>"
	svg += "        	" + value
	svg += "        </text>"
	svg += "        </g>"
	svg += "    </g>"
	svg += "</svg>"

	w.Header().Set("Content-Type", "image/svg+xml")
	w.WriteHeader(200)
	_, _ = w.Write([]byte(svg))
}
