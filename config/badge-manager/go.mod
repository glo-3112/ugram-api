module badge-manager

go 1.13

require (
	github.com/gorilla/mux v1.7.4
	golang.org/x/image v0.0.0-20200119044424-58c23975cae1
)
